<div class="row m-t30 m-b100">
    <div class="col-lg-9 col-md-8">
        <div class="post">
            <div class="post__slider">
                <div>
                    <img class="post__image" src="images/blog/post-1.jpg" alt=""/>
                </div>
                <div>
                    <img class="post__image" src="images/blog/post-1.jpg" alt=""/>
                </div>
                <div>
                    <img class="post__image" src="images/blog/post-1.jpg" alt=""/>
                </div>
                <div>
                    <img class="post__image" src="images/blog/post-1.jpg" alt=""/>
                </div>
                <div>
                    <img class="post__image" src="images/blog/post-1.jpg" alt=""/>
                </div>
            </div>
            <div class="post__content">
                <span class="post__content__category">Some category</span>

                <h4 class="post__content__title">Sample Fashion Product</h4>

                <span class="post__content__date">October, 2015</span>

                <p class="post__content__text">Let’s have a peek into the mists of the Spreadshirt crystal ball and see what the future holds in store in terms of designs that will do well in October. Our loyal and faithful in-house oracle reveals sound inside knowledge, and you’d be well advised to heed to the suggestions offered in this month’s design tip section. Read up on scary Halloween tips and October clues now.</p>
                <p class="post__content__text">Let’s have a peek into the mists of the Spreadshirt crystal ball and see what the future holds in store in terms of designs that will do well in October. Our loyal and faithful in-house oracle reveals sound inside knowledge, and you’d be well advised to heed to the suggestions offered in this month’s design tip section. Read up on scary Halloween tips and October clues now. Let’s have a peek into the mists of the Spreadshirt crystal ball and see what the future holds in store in terms of designs that will do well in October. Our loyal and faithful in-house oracle reveals sound inside knowledge, and you’d be well advised to heed to the suggestions offered in this month’s design tip section. Read up on scary Halloween tips and October clues now.</p>

                <div class="quote">
                    <p class="quote__text">Let’s have a peek into the mists of the Spreadshirt crystal ball and see what the future holds in store in terms of designs that will do well in October.</p>
                </div>

                <p class="post__content__text">Let’s have a peek into the mists of the Spreadshirt crystal ball and see what the future holds in store in terms of designs that will do well in October. Our loyal and faithful in-house oracle reveals sound inside knowledge, and you’d be well advised to heed to the suggestions offered in this month’s design tip section. Read up on scary Halloween tips and October clues now. Let’s have a peek into the mists of the Spreadshirt crystal ball and see what the future holds in store in terms of designs that will do well in October. Our loyal and faithful in-house oracle reveals sound inside knowledge, and you’d be well advised to heed to the suggestions offered in this month’s design tip section. Read up on scary Halloween tips and October clues now.</p>

                <div class="designs">
                    <div class="designs__item">
                        <div class="designs__img-box">
                            <img class="designs__image" src="images/blog/design.jpg" alt=""/>
                            <a class="designs__img-inner-box" href=""></a>
                        </div>

                        <a class="btn-post designs__btn" href="">Design</a>
                    </div>
                    <div class="designs__item">
                        <div class="designs__img-box">
                            <img class="designs__image" src="images/blog/design-2.jpg" alt=""/>
                            <a class="designs__img-inner-box" href=""></a>
                        </div>

                        <a class="btn-post designs__btn" href="">Design</a>
                    </div>
                    <div class="designs__item">
                        <div class="designs__img-box">
                            <img class="designs__image" src="images/blog/design-3.jpg" alt=""/>
                            <a class="designs__img-inner-box" href=""></a>
                        </div>

                        <a class="btn-post designs__btn" href="">Design</a>
                    </div>
                    <div class="designs__item">
                        <div class="designs__img-box">
                            <img class="designs__image" src="images/blog/design-4.jpg" alt=""/>
                            <a class="designs__img-inner-box" href=""></a>
                        </div>

                        <a class="btn-post designs__btn" href="">Design</a>
                    </div>
                </div>

                <p class="post__content__text">Let’s have a peek into the mists of the Spreadshirt crystal ball and see what the future holds in store in terms of designs that will do well in October. Our loyal and faithful in-house oracle reveals sound inside knowledge, and you’d be well advised to heed to the suggestions offered in this month’s design tip section. Read up on scary Halloween tips and October clues now.</p>
                <p class="post__content__text">Let’s have a peek into the mists of the Spreadshirt crystal ball and see what the future holds in store in terms of designs that will do well in October. Our loyal and faithful in-house oracle reveals sound inside knowledge, and you’d be well advised to heed to the suggestions offered in this month’s design tip section. Read up on scary Halloween tips and October clues now. Let’s have a peek into the mists of the Spreadshirt crystal ball and see what the future holds in store in terms of designs that will do well in October. Our loyal and faithful in-house oracle reveals sound inside knowledge, and you’d be well advised to heed to the suggestions offered in this month’s design tip section. Read up on scary Halloween tips and October clues now.</p>
            </div>

            <div class="post__social" data-affix>
                <span class="post__social__title">Share</span>
                <div class="post__social__item">
                    <a class="post__social__item__link" href=""><i class="fa fa-facebook"></i></a>
                </div>
                <div class="post__social__item">
                    <a class="post__social__item__link" href=""><i class="fa fa-twitter"></i></a>
                </div>
                <div class="post__social__item">
                    <a class="post__social__item__link" href=""><i class="fa fa-google-plus"></i></a>
                </div>
                <div class="post__social__item">
                    <a class="post__social__item__link" href=""><i class="fa fa-pinterest-p"></i></a>
                </div>
            </div>
        </div>

        <div class="pagination-post">
            <div class="pagination-post__item pagination-post__previous">
                <a href="" class="pagination-post__item__link"><span>Previous post</span></a>
            </div>
            <div class="pagination-post__item pagination-post__item--post">
                <a href="" class="month-posts__post">
                    <div class="month-posts__post__img-box">
                        <img src="images/blog/mini-post-1.jpg" alt=""/>
                    </div>
                    <div class="month-posts__post__content">
                        <span class="month-posts__post__title">Now that’s Fresh!</span>
                        <span class="month-posts__post__date">Let’s have a peek into the Let’s have a peek into the</span>
                    </div>
                </a>
            </div>
            <div class="pagination-post__item pagination-post__item--post">
                <a href="" class="month-posts__post">
                    <div class="month-posts__post__img-box">
                        <img src="images/blog/mini-post-2.jpg" alt=""/>
                    </div>
                    <div class="month-posts__post__content">
                        <span class="month-posts__post__title">Now that’s Fresh!</span>
                        <span class="month-posts__post__date">Let’s have a peek into the Let’s have a peek into the</span>
                    </div>
                </a>
            </div>
            <div class="pagination-post__item pagination-post__next">
                <a href="" class="pagination-post__item__link"><span>Next post</span></a>
            </div>
        </div>

        <div class="comments-post">
            <h4 class="comments-post__title">Comments (2)</h4>

            <div class="comments-post__item">
                <span class="comments-post__item__name">Samanta Grolsch</span>
                <span class="comments-post__item__date">October, 2015</span>

                <p class="comments-post__item__text">Let’s have a peek into the mists of the Spreadshirt crystal ball and see what the future holds in store in terms of designs that will do well in October. Our loyal and faithful in-house oracle reveals sound inside knowledge, and you’d be well advised to heed to the suggestions offered in this month’s design tip section. Read up on scary Halloween tips and October clues now.</p>
            </div>

            <div class="comments-post__item">
                <span class="comments-post__item__name">Samanta Grolsch</span>
                <span class="comments-post__item__date">October, 2015</span>

                <p class="comments-post__item__text">Let’s have a peek into the mists of the Spreadshirt crystal ball and see what the future holds in store in terms of designs that will do well in October. Our loyal and faithful in-house oracle reveals sound inside knowledge, and you’d be well advised to heed to the suggestions offered in this month’s design tip section. Read up on scary Halloween tips and October clues now.</p>
            </div>

            <div class="comments-post__form-box">
                <h4 class="comments-post__title">Leave reply</h4>

                <form action="" class="comments-post__form">
                    <div class="row">
                        <div class="col-sm-7">
                            <div class="form-group comments-post__form__item">
                                <label class="form-label" for="name">Name (required)</label>
                                <input type="text" class="form-control" id="name">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-7">
                            <div class="form-group comments-post__form__item">
                                <label class="form-label" for="name">Email (required)</label>
                                <input type="text" class="form-control" id="email">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-7">
                            <div class="form-group comments-post__form__item">
                                <label class="form-label" for="name">Website (required)</label>
                                <input type="text" class="form-control" id="website">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group comments-post__form__item">
                                <label class="form-label" for="name">Message  (required)</label>
                                <textarea class="form-control" name="" id="message " rows="10"></textarea>
                            </div>
                        </div>
                    </div>

                    <button class="btn__accept comments-post__form__btn">Send</button>
                </form>
            </div>
        </div>
    </div>
    <div class="col-lg-3 col-md-4 sm-hidden">
        <div class="post__banner">
            <img src="images/blog/post_banner.jpg" alt=""/>

            <div class="post__banner__text-box">
                <h4 class="post__banner__text">Video Tutorial: Applying Vector and Effects</h4>
                <a class="btn-post-reverse" href="">Read more</a>
            </div>
        </div>


        <div class="month-posts">    
            <div class="month-posts__slider">
                <div data-date="May 2015">
                    <a href="" class="month-posts__post">
                        <div class="month-posts__post__img-box">
                            <img src="images/blog/mini-post-1.jpg" alt=""/>
                        </div>
                        <div class="month-posts__post__content">
                            <span class="month-posts__post__title">Trending Colors – The Best-Selling Tones6</span>
                            <span class="month-posts__post__date">September, 2015</span>
                        </div>
                    </a>
                    <a href="" class="month-posts__post">
                        <div class="month-posts__post__img-box">
                            <img src="images/blog/mini-post-2.jpg" alt=""/>
                        </div>
                        <div class="month-posts__post__content">
                            <span class="month-posts__post__title">Now that’s Fresh!</span>
                            <span class="month-posts__post__date">September, 2015</span>
                        </div>
                    </a>
                    

                    <div class="month-posts__link-wrap">
                        <a class="month-posts__link" href="">More posts for this month</a>
                    </div>
                </div>
                <div data-date="June 2015">
                    <a href="" class="month-posts__post">
                        <div class="month-posts__post__img-box">
                            <img src="images/blog/mini-post-1.jpg" alt=""/>
                        </div>
                        <div class="month-posts__post__content">
                            <span class="month-posts__post__title">Trending Colors – The Best-Selling Tones6</span>
                            <span class="month-posts__post__date">September, 2015</span>
                        </div>
                    </a>
                    <a href="" class="month-posts__post">
                        <div class="month-posts__post__img-box">
                            <img src="images/blog/mini-post-2.jpg" alt=""/>
                        </div>
                        <div class="month-posts__post__content">
                            <span class="month-posts__post__title">Now that’s Fresh!</span>
                            <span class="month-posts__post__date">September, 2015</span>
                        </div>
                    </a>
                    <a href="" class="month-posts__post">
                        <div class="month-posts__post__img-box">
                            <img src="images/blog/mini-post-3.jpg" alt=""/>
                        </div>
                        <div class="month-posts__post__content">
                            <span class="month-posts__post__title">Trending Colors – The Best-Selling Tones6</span>
                            <span class="month-posts__post__date">September, 2015</span>
                        </div>
                    </a>
                    <a href="" class="month-posts__post">
                        <div class="month-posts__post__img-box">
                            <img src="images/blog/mini-post-4.jpg" alt=""/>
                        </div>
                        <div class="month-posts__post__content">
                            <span class="month-posts__post__title">Now that’s Fresh!</span>
                            <span class="month-posts__post__date">September, 2015</span>
                        </div>
                    </a>
                    <a href="" class="month-posts__post">
                        <div class="month-posts__post__img-box">
                            <img src="images/blog/mini-post-5.jpg" alt=""/>
                        </div>
                        <div class="month-posts__post__content">
                            <span class="month-posts__post__title">Now that’s Fresh!</span>
                            <span class="month-posts__post__date">September, 2015</span>
                        </div>
                    </a>

                    <div class="month-posts__link-wrap">
                        <a class="month-posts__link" href="">More posts for this month</a>
                    </div>
                </div>
                <div data-date="July 2015">
                    <a href="" class="month-posts__post">
                        <div class="month-posts__post__img-box">
                            <img src="images/blog/mini-post-1.jpg" alt=""/>
                        </div>
                        <div class="month-posts__post__content">
                            <span class="month-posts__post__title">Trending Colors – The Best-Selling Tones6</span>
                            <span class="month-posts__post__date">September, 2015</span>
                        </div>
                    </a>
                    <a href="" class="month-posts__post">
                        <div class="month-posts__post__img-box">
                            <img src="images/blog/mini-post-2.jpg" alt=""/>
                        </div>
                        <div class="month-posts__post__content">
                            <span class="month-posts__post__title">Now that’s Fresh!</span>
                            <span class="month-posts__post__date">September, 2015</span>
                        </div>
                    </a>
                    <a href="" class="month-posts__post">
                        <div class="month-posts__post__img-box">
                            <img src="images/blog/mini-post-3.jpg" alt=""/>
                        </div>
                        <div class="month-posts__post__content">
                            <span class="month-posts__post__title">Trending Colors – The Best-Selling Tones6</span>
                            <span class="month-posts__post__date">September, 2015</span>
                        </div>
                    </a>
                    <a href="" class="month-posts__post">
                        <div class="month-posts__post__img-box">
                            <img src="images/blog/mini-post-4.jpg" alt=""/>
                        </div>
                        <div class="month-posts__post__content">
                            <span class="month-posts__post__title">Now that’s Fresh!</span>
                            <span class="month-posts__post__date">September, 2015</span>
                        </div>
                    </a>
                    <a href="" class="month-posts__post">
                        <div class="month-posts__post__img-box">
                            <img src="images/blog/mini-post-5.jpg" alt=""/>
                        </div>
                        <div class="month-posts__post__content">
                            <span class="month-posts__post__title">Now that’s Fresh!</span>
                            <span class="month-posts__post__date">September, 2015</span>
                        </div>
                    </a>

                    <div class="month-posts__link-wrap">
                        <a class="month-posts__link" href="">More posts for this month</a>
                    </div>
                </div>
                <div data-date="September 2015">
                    <a href="" class="month-posts__post">
                        <div class="month-posts__post__img-box">
                            <img src="images/blog/mini-post-1.jpg" alt=""/>
                        </div>
                        <div class="month-posts__post__content">
                            <span class="month-posts__post__title">Trending Colors – The Best-Selling Tones6</span>
                            <span class="month-posts__post__date">September, 2015</span>
                        </div>
                    </a>
                    <a href="" class="month-posts__post">
                        <div class="month-posts__post__img-box">
                            <img src="images/blog/mini-post-2.jpg" alt=""/>
                        </div>
                        <div class="month-posts__post__content">
                            <span class="month-posts__post__title">Now that’s Fresh!</span>
                            <span class="month-posts__post__date">September, 2015</span>
                        </div>
                    </a>
                    <a href="" class="month-posts__post">
                        <div class="month-posts__post__img-box">
                            <img src="images/blog/mini-post-3.jpg" alt=""/>
                        </div>
                        <div class="month-posts__post__content">
                            <span class="month-posts__post__title">Trending Colors – The Best-Selling Tones6</span>
                            <span class="month-posts__post__date">September, 2015</span>
                        </div>
                    </a>
                    <a href="" class="month-posts__post">
                        <div class="month-posts__post__img-box">
                            <img src="images/blog/mini-post-4.jpg" alt=""/>
                        </div>
                        <div class="month-posts__post__content">
                            <span class="month-posts__post__title">Now that’s Fresh!</span>
                            <span class="month-posts__post__date">September, 2015</span>
                        </div>
                    </a>
                    <a href="" class="month-posts__post">
                        <div class="month-posts__post__img-box">
                            <img src="images/blog/mini-post-5.jpg" alt=""/>
                        </div>
                        <div class="month-posts__post__content">
                            <span class="month-posts__post__title">Now that’s Fresh!</span>
                            <span class="month-posts__post__date">September, 2015</span>
                        </div>
                    </a>

                    <div class="month-posts__link-wrap">
                        <a class="month-posts__link" href="">More posts for this month</a>
                    </div>
                </div>
                <div data-date="October 2015">
                    <a href="" class="month-posts__post">
                        <div class="month-posts__post__img-box">
                            <img src="images/blog/mini-post-1.jpg" alt=""/>
                        </div>
                        <div class="month-posts__post__content">
                            <span class="month-posts__post__title">Trending Colors – The Best-Selling Tones6</span>
                            <span class="month-posts__post__date">September, 2015</span>
                        </div>
                    </a>
                    <a href="" class="month-posts__post">
                        <div class="month-posts__post__img-box">
                            <img src="images/blog/mini-post-2.jpg" alt=""/>
                        </div>
                        <div class="month-posts__post__content">
                            <span class="month-posts__post__title">Now that’s Fresh!</span>
                            <span class="month-posts__post__date">September, 2015</span>
                        </div>
                    </a>
                    <a href="" class="month-posts__post">
                        <div class="month-posts__post__img-box">
                            <img src="images/blog/mini-post-3.jpg" alt=""/>
                        </div>
                        <div class="month-posts__post__content">
                            <span class="month-posts__post__title">Trending Colors – The Best-Selling Tones6</span>
                            <span class="month-posts__post__date">September, 2015</span>
                        </div>
                    </a>
                    <a href="" class="month-posts__post">
                        <div class="month-posts__post__img-box">
                            <img src="images/blog/mini-post-4.jpg" alt=""/>
                        </div>
                        <div class="month-posts__post__content">
                            <span class="month-posts__post__title">Now that’s Fresh!</span>
                            <span class="month-posts__post__date">September, 2015</span>
                        </div>
                    </a>
                    <a href="" class="month-posts__post">
                        <div class="month-posts__post__img-box">
                            <img src="images/blog/mini-post-5.jpg" alt=""/>
                        </div>
                        <div class="month-posts__post__content">
                            <span class="month-posts__post__title">Now that’s Fresh!</span>
                            <span class="month-posts__post__date">September, 2015</span>
                        </div>
                    </a>

                    <div class="month-posts__link-wrap">
                        <a class="month-posts__link" href="">More posts for this month</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>