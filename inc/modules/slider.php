<div class="container slider">
    <div class="row">
        <div class="col-xs-12">
            <div class="home-slider">
                <div class="home-slider__content" data-bg>
                    <img class="home-slider__picture" src="images/home/home-banner.jpg" data-img alt=""/>
                </div>
                <div class="home-slider__content" data-bg>
                    <img class="home-slider__picture" src="images/home/home-banner.jpg" data-img alt=""/>
                </div>
                <div class="home-slider__content" data-bg>
                    <img class="home-slider__picture" src="images/home/home-banner.jpg" data-img alt=""/>
                </div>
            </div>

            <ul class="category-menu">
                <li class="category-menu__item"><a href="">T-Shirts</a></li>
                <li class="category-menu__item"><a href="">T-Cap</a></li>
                <li class="category-menu__item"><a href="">Cup</a></li>
                <li class="category-menu__item"><a href="">Suspenders</a></li>
                <li class="category-menu__item"><a href="">Towel</a></li>
                <li class="category-menu__item"><a href="">Pillows</a></li>
                <li class="category-menu__item"><a href="">Belts</a></li>
            </ul>
        </div>
    </div>
</div>
<div class="container">