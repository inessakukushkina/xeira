<div class="grid-home-banners">
    <div class="row">
        <div class="col-md-9">
            <div class="row">
                <div class="col-sm-8">
                    <div class="grid-home-banners__item">
                        <a href="" class="grid-home-banners__item__link">
                            <img src="images/home/home-1.jpg" alt=""/>
                            <span class="grid-home-banners__item__link__text">Pillo</span>
                        </a>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="grid-home-banners__item">
                        <a class="grid-home-banners__item__link" href="">
                            <img src="images/home/home-4.jpg" alt=""/>
                            <span class="grid-home-banners__item__link__text">Towel</span>
                        </a>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4">
                    <div class="grid-home-banners__item">
                        <a class="grid-home-banners__item__link" href="">
                            <img src="images/home/home-5.jpg" alt=""/>
                            <span class="grid-home-banners__item__link__text">Cup</span>
                        </a>
                    </div>
                </div>
                <div class="col-sm-8">
                    <div class="grid-home-banners__item">
                        <a class="grid-home-banners__item__link" href="">
                            <img src="images/home/home-8.jpg" alt=""/>
                            <span class="grid-home-banners__item__link__text">Suspenders</span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-3 col-sm-4">
            <div class="grid-home-banners__item">
                <a class="grid-home-banners__item__link" href="">
                    <img src="images/home/home-3.jpg" alt=""/>
                    <span class="grid-home-banners__item__link__text">Woman product</span>
                </a>
            </div>
        </div>
        <div class="col-md-6 col-sm-8">
            <div class="grid-home-banners__item">
                <a class="grid-home-banners__item__link" href="">
                    <img src="images/home/home-2.jpg" alt=""/>
                    <span class="grid-home-banners__item__link__text">Popular products</span>
                </a>
            </div>
        </div>
        <div class="col-md-6">
            <div class="row">
                <div class="col-md-12 col-sm-6">
                    <div class="grid-home-banners__item">
                        <a class="grid-home-banners__item__link" href="">
                            <img src="images/home/home-6.jpg" alt=""/>
                            <span class="grid-home-banners__item__link__text">T-Shirt</span>
                        </a>
                    </div>
                </div>
                <div class="col-md-12 col-sm-6">
                    <div class="grid-home-banners__item">
                        <a class="grid-home-banners__item__link" href="">
                            <img src="images/home/home-7.jpg" alt=""/>
                            <span class="grid-home-banners__item__link__text">T-Cup</span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>    
</div>
<div class="row">
    <div class="col-xs-12">
        <div class="home__content">
            <h2 class="text-center">Print your Ideas on T-shirts</h2>
            <p>&nbsp;</p>
            <div class="clearfix"><img class="picture-right" src="http://dev.xn--hosentrger-grosshandel-64b.com/uploads/el-finder/files/justshirt-banner-px.jpg" alt="" />
            <div style="height: 1px; width: 100px; background-color: #e4436a; margin-bottom: 20px;">&nbsp;</div>
            <p>You have cool design ideas and you want them get printed on T-shirts for your next event or party?&nbsp;</p>
            <p>Then you at the right place! At JustShirt you get a huge range of T-shirts for men and women, T-shirts for kids, T-shirts with funny slogans, T-shirts with cool design - simply we will print your T-shirt.&nbsp;</p>
            <p>You can create your T-shirt with your own artwork, text or you can simply use one of the designs provided by other designers on JustShirt and combine their artwork with yours.&nbsp;</p>
            <p>With your personally designed T-shirts you will certainly at the next party stand out of the crowd.&nbsp;</p>
            <p>If you think that customized T-shirts are too mainstream, then you have exclusively on JustShirt the option to customize your own braces for trousers. All our braces are produced by us in Germany, in fact we are into production for the last 25 years.</p>
            <p>For our braces we only use high quality materials. We are sure that with your self designed braces you will be the eye-catcher at the next Oktoberfest, party or Event.</p>
            </div>
            <div style="margin-top: 20px;"><a class="btn__default" style="color: white !important;" href="http://dev.hosentr&auml;ger-grosshandel.com/configure/tshirts">Start printing now</a></div>
            <div class="text-center" style="background-color: #f9f9f9; margin-top: 30px; padding: 10px 20px 30px 20px;">
            <h2>Sell your design and create your campaigns</h2>
            <p>You want to offer your cool designs to other customers and earn money with your ideas? At JustShirt you have the option not to only customize your own t-shirts, but as our customer you are able to sell your designs easily.</p>
            <p>In your customer area you can simply upload your designs and set the amount you would like to sell the design for. Once a customer choses your design, we will print it and credit the commission onto your account.&nbsp;</p>
            <p>At JustShirt you can also create campaigns with your text or deigns.&nbsp;</p>
            <p>You think that your customized T-shirt might be interesting for other customers? Why don't you create a campaign?&nbsp;</p>
            <p>Simply start a campaign in your account with your customized design, set a sales goal and the end of your campaign. As soon your goals a reached we will start printing your design and ship it to the customer, you will receive the profit set.&nbsp;</p>
            <div style="margin-top: 20px;"><a class="btn-post" style="width: auto; padding-left: 20px; padding-right: 20px;" href="../../../selling">More Info on campaigns and selling artworks</a></div>
            </div>
            <h2 class="text-center" style="margin-bottom: 30px;">About JustShirt - Printed in Germany</h2>
            <div class="clearfix"><img class="picture-left" src="http://dev.xn--hosentrger-grosshandel-64b.com/uploads/el-finder/files/justshirt-about-px.jpg" alt="" />
            <div style="height: 1px; width: 100px; background-color: #e4436a; display: inline-block; margin-bottom: 20px;">&nbsp;</div>
            <p>25 years ago we started with the production of leather goods and have since then established ourselves in the manufacturing and wholesale industry.</p>
            <p>In 2012 we successfully entered the online retail market with our brand XEIRA with interest in leather goods, braces and other textiles.</p>
            <p>With JustShirt we would like to recreate the same success and bring closer our production facility to the consumer.</p>
            <p>We want to offer high quality prints and customized products at an affordable price.&nbsp;</p>
            <div style="margin-top: 20px;"><a class="btn__default" style="color: white !important;" href="../../../blog/my-first-travel">Get to know us</a></div>
            </div>
            <div class="text-center" style="background-color: #f9f9f9; margin-top: 30px; padding: 10px 20px 30px 20px;">
            <h2>Bulk Orders / Wholesale &nbsp;for your Event, Exhibition, Party (min 50 pcs)</h2>
            <p>You need quickly a large amount of printed T-Shirts, suspenders, hats and other fabrics for your next event, exhibition or party? JustShirt can offer your reasonable prices and a quick production. With our modern machines we able to print and dispatch your order within 24 hours.&nbsp;</p>
            <div style="margin-top: 20px;"><a class="btn-post" style="width: auto; padding-left: 20px; padding-right: 20px;">LINK</a></div>
            </div>
        </div>
    </div>
</div>