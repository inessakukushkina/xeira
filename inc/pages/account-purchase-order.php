<ul class="breadcrumbs">
	<li class="breadcrumbs__item">
		<a href="" class="breadcrumbs__item__link">
			My account
		</a>
	</li>
	<li class="breadcrumbs__item">
		<a href="" class="breadcrumbs__item__link">
			My designs
		</a>
	</li>
</ul>
<div class="row">
	<div class="col-sm-3">
		<div class="sidebar">			
			<h2 class="sidebar_title">
				My Account
			</h2>
			<ul class="sidebar__list">
				<li class="sidebar__list__item">
					<a href="" class="sidebar__list__item__link active">
						My Designs
					</a>
				</li>
				<li class="sidebar__list__item">
					<a href="" class="sidebar__list__item__link">
						My Purchases
					</a>
				</li>
				<li class="sidebar__list__item">
					<a href="" class="sidebar__list__item__link">
						My Sold Items
					</a>
				</li>
				<li class="sidebar__list__item">
					<a href="" class="sidebar__list__item__link">
						My Campaigns
					</a>
				</li>
				<li class="sidebar__list__item">
					<a href="" class="sidebar__list__item__link">
						Wishlist
					</a>
				</li>
				<li class="sidebar__list__item">
					<a href="" class="sidebar__list__item__link">
						Reviews
					</a>
				</li>
				<li class="sidebar__list__item">
					<a href="" class="sidebar__list__item__link">
						Invite & earn
					</a>
				</li>
				<li class="sidebar__list__item">
					<a href="" class="sidebar__list__item__link">
						Settings
					</a>
				</li>
			</ul>
		</div>
	</div>
	<div class="col-sm-9">
		<h2 class="head__title">
			Order details
		</h2>
		<table class="order__table responsive__table m-tb10">
			<thead class="b-None">
				<tr>
					<th>Status</th>
					<th>Shipping address</th>
					<th>Shipping method</th>
					<th>Payment Info</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td data-th="Status:">
						<span class="order__pending">
							<i class="m-r5 icon-pending"></i>
							Pending
						</span>
					</td>
					<td data-th="Shipping address:">
						<span>
							3355 S Las Vegas Blvd
						</span>
						<span>
							Las Vegas, NV
						</span>
						<span>
							(702) 414-1000
						</span>
					</td>
					<td data-th="Shipping method:">
						Fedex 1-5 days — $15.98
					</td>
					<td data-th="Payment Info:">
						<span>
							Cardholder Name: John Doe
						</span>
						<span>
							Payment type: Mastercard
						</span>
						<span>
							Card number: **** **** **** 0921
						</span>
					</td>
				</tr>
			</tbody>
		</table>

		<table class="checkout__products responsive__table b-Top">
			<thead>
				<tr>
					<th></th>
					<th>Product Name</th>
					<th>QTY</th>
					<th>Total</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td data-th="Product Image:">
						<div class="product__preview">
							<div class="product__preview__picture">
								<img src="images/products/shirt1.png" alt="" class="product__preview__avatar">
							</div>
						</div>
					</td>
					<td data-th="Product Name:">
						<h2 class="checkout__products__title">
							Product With Custom Options
						</h2>
						<ul class="checkout__products__list">
							<li class="checkout__products__list__item">
								T-Shirt Size <span>L</span>
							</li>
							<li class="checkout__products__list__item">
								T-Shirt Color <span>Pink</span>
							</li>
							<li class="checkout__products__list__item">
								Additional Options <br>
								<span>Embroidered logo to the chest</span>
							</li>
						</ul>
					</td>
					<td data-th="QTY:">
						1
					</td>
					<td data-th="Total:">
						$22.00
					</td>
				</tr>
				<tr>
					<td data-th="Product Image:">
						<div class="product__preview">
							<div class="product__preview__picture">
								<img src="images/products/shirt2.png" alt="" class="product__preview__avatar">
							</div>
						</div>
					</td>
					<td data-th="Product Name:">
						<h2 class="checkout__products__title">
							Product With Custom Options
						</h2>
						<ul class="checkout__products__list">
							<li class="checkout__products__list__item">
								T-Shirt Size <span>L</span>
							</li>
							<li class="checkout__products__list__item">
								T-Shirt Color <span>Pink</span>
							</li>
							<li class="checkout__products__list__item">
								Additional Options <br>
								<span>Embroidered logo to the chest</span>
							</li>
						</ul>
					</td>
					<td data-th="QTY:">
						1
					</td>
					<td data-th="Total:">
						$22.00
					</td>
				</tr>
				<tr>
					<td data-th="Product Image:">
						<div class="product__preview">
							<div class="product__preview__picture">
								<img src="images/products/shirt3.png" alt="" class="product__preview__avatar">
							</div>
						</div>
					</td>
					<td data-th="Product Name:">
						<h2 class="checkout__products__title">
							Product With Custom Options
						</h2>
						<ul class="checkout__products__list">
							<li class="checkout__products__list__item">
								T-Shirt Size <span>L</span>
							</li>
							<li class="checkout__products__list__item">
								T-Shirt Color <span>Pink</span>
							</li>
							<li class="checkout__products__list__item">
								Additional Options <br>
								<span>Embroidered logo to the chest</span>
							</li>
						</ul>
					</td>
					<td data-th="QTY:">
						1
					</td>
					<td data-th="Total:">
						$22.00
					</td>
				</tr>
			</tbody>
		</table>
		<div class="b-Top clearfix m-tb20 p-tb20">
			<div class="text-right">
				<p class="text-page m-b0">
					Subtotal: $88.00
				</p>
				<p class="text-page m-b0">
					Shipping price: $15.98
				</p>
				<p class="text-lighten m-b0">
					Promo code discount: $00.00
				</p>
				<p class="text-bold">
					VAT: $5.00
				</p>
			</div>
			<div class="row">
				<div class="col-xs-6">
					<a href="" class="link__page-14">
						<i class="icon-boll"></i> I have problems with this order
					</a>
				</div>
				<div class="col-xs-6 text-right">
					<p class="text-mutted text-bold">
						Total price: $108.98
					</p>
				</div>
			</div>
		</div>
	</div>
</div>