var popover = function($data_popover, $popover_class){
    var flag_lock   = false;   
    $($data_popover).each(function(){
        var $_this          = $(this);
        var $popover        = $_this.find($popover_class);
        var $popover_top    = $_this.height()/2 - $popover.height()/2 + 'px';

        if($(this).hasClass('active')){
            $(this).find($popover_class).css('display', 'block');
            $popover.css('top', $popover_top);
        }
    })   

    $(document).on('click', $data_popover, function(){
        var $_this          = $(this);
        var $popover        = $_this.find($popover_class);
        var $popover_top    = $_this.height()/2 - $popover.height()/2 + 'px';
        if(!$_this.hasClass('active')){
            $($popover_class).slideUp();
            $($data_popover).removeClass('active');
            $_this.addClass('active');
            $popover.slideDown();
            $popover.css('top', $popover_top);
        } 
    });

    $('[data-popover-color]').each(function(){
         var $popover_color = $(this).closest('[data-toggle-popover]').find('.popover__color');
        $(this).hover(function(){
            var $_count     = $popover_color.length;
            var $block_left = $_count - 1;
            $popover_color.addClass('show__color');
            $(this).closest('[data-toggle-popover]').css('left', - 45 - $block_left * $popover_color.width() + 'px');
        });
        $(this).closest('[data-toggle-popover]').mouseleave(function(){
            $popover_color.removeClass('show__color');
            $(this).removeClass('show__color').css('left', '-45px');
        })
    })
}

function colorPopover($popover_color, $popover, $side, $number){
    $($popover_color).each(function(){
        var $_this   = $(this);
        var $_color  = $_this.closest($popover).find('.popover__color');

        $_this.hover(function(){
            var $_count     = $_color.length;
            var $block_left = $_count - 1;
            $($popover_color).addClass('show__color');
            $_this.closest($popover).css($side, $number - $block_left * $_color.width() + 'px');
        });
        $(this).closest($popover).mouseleave(function(){
            $($popover_color).removeClass('show__color');
            $(this).css($side, $number + 'px');
        })
    })
}

function popoverClose($popover_color, $popover, $side, $number){
    $($popover_color).removeClass('show__color');
    $popover.css($side, $number + 'px');
}

function addActiveColor($this){
    $this.addClass('active');
}

 function removeActiveColor($this){
     $this.removeClass('active');
}

function checkBtnRemove(){
    $('[data-conf-color]').each(function(){
        var $_this          = $(this);
        var $color_empty    = $_this.find('.color__empty');
        var $set_color      = $_this.find('[data-set-color]');
        if($color_empty.length == 4){
            $set_color.first().find('[data-remove-color]').addClass('is-hidden');
        } else{
            $set_color.first().find('[data-remove-color]').removeClass('is-hidden');
        }
    });
}

function appendTemplate(_template, _block){
    $(_template).insertBefore(_block);
}

var templates = {
    'templateEmpty' :   '<div class="configurator__color color__empty" data-set-color="">' +
                            '<div class="color__block"></div>'+
                            '<div class="remove__color is-hidden" data-remove-color=""></div>'+
                        '</div>'
}
$(document).ready(function(){
    popover('[data-popover]', $('[data-toggle-popover]'));
    colorPopover('[data-popover-color].popover__left', '[data-toggle-popover]', 'left', -45);
    colorPopover('[data-popover-color].popover__right', '[data-toggle-popover]', 'right', -45);

    $('[data-close-popover]').each(function(){
       closePopover('[data-toggle-popover]', '[data-popover]');
    })

    $(document).on('click', '[data-popover-block]', function(){
        $('.popover__block').removeClass('active');
        $(this).addClass('active');
    })

    $(document).on('click', '[data-choose-color]', function(){
        var $_this          = $(this);
        var $conf__color    = $_this.closest('[data-conf-color]');
        var $_bg            = $_this.find('[style]').attr('style');
        var $set_color      = $conf__color.find('.color__empty').eq(0);
        var $_conf          = $conf__color.find('.configurator__color');
        var $color_block    = $_conf.find('.color__block');
        var $_add__color    = $conf__color.find('[data-add-color]');

        var _STYLE = [];
       
        function addColor(){
            $set_color.find('.color__block').attr('style', $_bg);
            $set_color.find('[data-remove-color]').removeClass('is-hidden');
            $set_color.removeClass('color__empty');
        }
        function checkColor(checkFunc){
            for(var i=0; i<$color_block.length - 1; i++){
                _STYLE.push($color_block.eq(i).attr('style'));
                if(_STYLE[i] === $_bg){
                   checkFunc(i);
                } 
            }
        }
        function removeAddedTemplate(i){
             $_conf.eq(i).remove();
        }

        var colorTemplates = {
            'templateBg'    :   '<div class="configurator__color" data-set-color>'+
                                    '<div class="color__block" style=' + $_bg + '></div>'+
                                    '<div class="remove__color" data-remove-color=""></div>'+
                                '</div>'
        }

        if($conf__color.find('.color__empty').length <= 0){ 
            if($_this.hasClass('active')){ 
                if($conf__color.find('.color__empty').length == 4){
                    $conf__color.find('[data-set-color]').first().find('[data-remove-color]').addClass('is-hidden');
                } else{
                    checkColor(removeAddedTemplate);
                    removeActiveColor($_this);
                }
               
                if($conf__color.find('[data-set-color]').length < 6){
                    appendTemplate(templates.templateEmpty, $_add__color);
                }
            } else{
                addActiveColor($_this);
                appendTemplate(colorTemplates.templateBg, $_add__color);
            }
        } else{
            if($_this.hasClass('active')){
                if($conf__color.find('.color__empty').length == 4){
                    $conf__color.find('[data-set-color]').first().find('[data-remove-color]').addClass('is-hidden');
                 } else{
                    checkColor(removeAddedTemplate);
                    removeActiveColor($_this);
                    appendTemplate(templates.templateEmpty); 
                }    
            } else{
                addActiveColor($_this)
                addColor();
            }
        }
        checkBtnRemove();
    });

    checkBtnRemove();

    $(document).on('click', '[data-remove-color]', function(){
        var $_this          = $(this);
        var $conf__color    = $_this.closest('[data-conf-color]');

        var $set_color      = $_this.closest('[data-set-color]'); 
        var $choose__color  = $conf__color.find('[data-choose-color]');

        var $bg__color      = $set_color.find('[style]').attr('style');

        var $conf__color    = $_this.closest('[data-conf-color]');
        var $all_set        = $conf__color.find('[data-set-color]');
        var $_add__color    = $conf__color.find('[data-add-color]');

        var _STYLE = [];

        $set_color.remove();

        if($all_set.length < 6){
            appendTemplate(templates.templateEmpty, $_add__color);
        }

        for(var i=0; i< $choose__color.length; i++){
            _STYLE.push($choose__color.eq(i).find('[style]').attr('style'));
            if(_STYLE[i] === $bg__color){  
               removeActiveColor($choose__color.eq(i));
            }
        }
        checkBtnRemove();
    })
})
