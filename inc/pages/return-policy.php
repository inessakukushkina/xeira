<div class="row m-t30 p-b100">
    <div class="col-sm-3 col-md-2">
        <div class="sidebar">
            <h4 class="sidebar__title2">About us</h4>

            <ul class="sidebar__menu">
                <li class="sidebar__menu__item">
                    <a href="">The Company</a>
                </li>
                <li class="sidebar__menu__item">
                    <a href="">Impressum</a>
                </li>
                <li class="sidebar__menu__item">
                    <a href="">Contact</a>
                </li>
                <li class="sidebar__menu__item">
                    <a href="">Shipping</a>
                </li>
                <li class="sidebar__menu__item active">
                    <a href="">Return Policy</a>
                </li>
                <li class="sidebar__menu__item">
                    <a href="">Privacy Policy</a>
                </li>
                <li class="sidebar__menu__item">
                    <a href="">Terms and Condition</a>
                </li>
                <li class="sidebar__menu__item">
                    <a href="">Order Precess</a>
                </li>
                <li class="sidebar__menu__item">
                    <a href="">Blog</a>
                </li>
            </ul>
        </div>
    </div>
    <div class="col-sm-9 col-md-10">
        <h5 class="page-title">Widerrufsbelehrung & Widerrufsformular </h5>

        <p class="page-text">Verbrauchern steht ein Widerrufsrecht nach folgender Maßgabe zu, wobei Verbraucher jede natürliche Person ist, die ein Rechtsgeschäft zu Zwecken abschließt, die überwiegend weder ihrer gewerblichen noch ihrer selbständigen beruflichen Tätigkeit zugerechnet werden können:</p>

        <div class="page-content">
            <div class="page-content__row">
                <div class="page-content__number">
                    <span class="page-content__number-label">a</span>
                </div>
                <div class="page-content__container">
                    <h6 class="page-content__title">Widerrufsbelehrung</h6>
                    <p>Sie haben das Recht, binnen 14 Tage ohne Angabe von Gründen diesen Vertrag zu widerrufen. </p>
                    <p>Die Widerrufsfrist beträgt 14 Tage ab dem Tag, an dem Sie oder ein von Ihnen benannter Dritter, der nicht der Beförderer ist, die letzte Ware in Besitz genommen haben bzw. hat. </p>
                    <p>Um Ihr Widerrufsrecht auszuüben, müssen Sie uns (Justshirt Retail Inh. Satwant Singh Pahal, Ahornstr. 75, 65933 Frankfurt am Main, Tel.: 069 38995277, Fax: 069 38030931, E-Mail: info@justshirt.de) mittels einer eindeutigen Erklärung (z. B. ein mit der Post versandter Brief, Telefax oder E-Mail) über Ihren Entschluss, diesen Vertrag zu widerrufen, informieren. Sie können dafür das beigefügte Muster-Widerrufsformular verwenden, das jedoch nicht vorgeschrieben ist. </p>
                    <p>Zur Wahrung der Widerrufsfrist reicht es aus, dass Sie die Mitteilung über die Ausübung des Widerrufsrechts vor Ablauf der Widerrufsfrist absenden.</p>

                    <div class="page-content__row">
                        <p><strong>Folgen des Widerrufs</strong></p>
                        <p>Wenn Sie diesen Vertrag widerrufen, haben wir Ihnen alle Zahlungen, die wir von Ihnen erhalten haben, einschließlich der Lieferkosten (mit Ausnahme der zusätzlichen Kosten, die sich daraus ergeben, dass Sie eine andere Art der Lieferung als die von uns angebotene, günstigste Standardlieferung gewählt haben), unverzüglich und spätestens binnen vierzehn Tagen ab dem Tag zurückzuzahlen, an dem die Mitteilung über Ihren Widerruf dieses Vertrags bei uns eingegangen ist. </p>
                        <p>Für diese Rückzahlung verwenden wir dasselbe Zahlungsmittel, das Sie bei der ursprünglichen Transaktion eingesetzt haben, es sei denn, mit Ihnen wurde ausdrücklich etwas anderes vereinbart; in keinem Fall werden Ihnen wegen dieser Rückzahlung Entgelte berechnet. </p>
                        <p>Wir können die Rückzahlung verweigern, bis wir die Waren wieder zurückerhalten haben oder bis Sie den Nachweis erbracht haben, dass Sie die Waren zurückgesandt haben, je nachdem, welches der frühere Zeitpunkt ist. </p>
                        <p>Sie haben die Waren unverzüglich und in jedem Fall spätestens binnen vierzehn Tagen ab dem Tag, an dem Sie uns über den Widerruf dieses Vertrags unterrichten, an uns zurückzusenden oder zu übergeben. </p>
                        <p>Die Frist ist gewahrt, wenn Sie die Waren vor Ablauf der Frist von vierzehn Tagen absenden. Sie tragen die unmittelbaren Kosten der Rücksendung der Waren. Sie müssen für einen etwaigen Wertverlust der Waren nur aufkommen, wenn dieser Wertverlust auf einen zur Prüfung der Beschaffenheit, Eigenschaften und Funktionsweise der Waren nicht notwendigen Umgang mit ihnen zurückzuführen ist.</p>
                    </div>
                </div>
            </div>
            <div class="page-content__row">
                <div class="page-content__number">
                    <span class="page-content__number-label">b</span>
                </div>
                <div class="page-content__container">
                    <h6 class="page-content__title">Widerrufsformular</h6>
                    <p>Muster-Widerrufsformular</p>
                    <p>Wenn Sie den Vertrag widerrufen wollen, dann füllen Sie bitte dieses Formular aus(einfach den Text kopieren) und senden Sie es zurück.</p>

                    <div class="page-content__row">
                        <div class="row">
                            <div class="col-sm-4">
                                <p class="address-list">
                                    <span>Justshirt Retail Inh. Satwant Singh Pahal </span>
                                    <span>Ahornstraße 75</span>
                                    <span>65933 Frankfurt am Main </span>
                                    <span>Fax: 069-38030931</span>
                                    <span>E-Mail: info@justshirt.de</span>
                                </p>
                            </div>
                            <div class="col-sm-8">
                                <p>Hiermit widerrufe(n) ich/wir ( Ihr Name) den von mir/uns abgeschlossenen Vertrag über den Kauf der folgenden Waren (Bitte nachfolgend die Rechnungsnummer oder Bestellnummer angeben)</p>

                                <ul class="page-content__list page-content__list--space">
                                    <li>- Bestellt am .................../erhalten am .................................</li>
                                    <li>- Rechnungsnummer : ........................................................</li>
                                    <li>- Bestellnummer : ................................................................</li>
                                    <li>- Name des/der Verbraucher(s) : .....................................................................</li>
                                    <li>- Anschrift des/der Verbraucher(s) : ..................................................................................................................</li>
                                    <li>- Datum .................................................................</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>