<div class="row m-t30 p-b100">
    <div class="col-sm-3 col-md-2">
        <div class="sidebar">
            <h4 class="sidebar__title2">About us</h4>

            <ul class="sidebar__menu">
                <li class="sidebar__menu__item">
                    <a href="">The Company</a>
                </li>
                <li class="sidebar__menu__item active">
                    <a href="">Impressum</a>
                </li>
                <li class="sidebar__menu__item">
                    <a href="">Contact</a>
                </li>
                <li class="sidebar__menu__item">
                    <a href="">Shipping</a>
                </li>
                <li class="sidebar__menu__item">
                    <a href="">Return Policy</a>
                </li>
                <li class="sidebar__menu__item">
                    <a href="">Privacy Policy</a>
                </li>
                <li class="sidebar__menu__item">
                    <a href="">Terms and Condition</a>
                </li>
                <li class="sidebar__menu__item">
                    <a href="">Order Precess</a>
                </li>
                <li class="sidebar__menu__item">
                    <a href="">Blog</a>
                </li>
            </ul>
        </div>
    </div>
    <div class="col-sm-9 col-md-10">
        <div class="page-content page-content--m0">
            <div class="row">
                <div class="col-md-6">
                    <div class="page-content__map">
                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2559.4571154338078!2d8.595977122278857!3d50.09645035607829!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47bd0a3a0d9b8163%3A0xe6aeea8f44baa78a!2zTMOkcmNoZW5zdHJhw59lLCA2NTkzMyBGcmFua2Z1cnQgYW0gTWFpbiwg0JPQtdGA0LzQsNC90LjRjw!5e0!3m2!1sru!2sru!4v1445507497476" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
                    </div>
                </div>
                <div class="col-md-6 margin-top-md-28">
                    <div class="page-content__row page-content__row--bp0">
                        <h4 class="page-title">Impressum JUSTSHIRT</h4>
                        <p><a href="">JUSTSHIRT RETAIL</a></p>
                        <p>
                            <span class="page-content__block">Inh. Satwant Singh Pahal</span>
                            <span class="page-content__block">Ahornstraße 75</span>
                            <span class="page-content__block">65933 Frankfurt am Main</span>
                            <span class="page-content__block">Deutschland</span>
                        </p>
                        <p>USt-Id: DE291769554 </p>
                        <p>Kontakt</p>
                        <p>
                            <span class="page-content__block">Montag bis Freitag - von 9 Uhr bis 18 Uhr</span>
                            <span class="page-content__block">Tel: 069 – 93997583</span>
                            <span class="page-content__block">Fax: 069 – 38030931</span>
                            <span class="page-content__block">E-Mail: info@justshirt.de</span>
                        </p>
                        <p>Wir wünschen Ihnen noch Viel Spaß beim Einkaufen auf JUSTSHIRT.de</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>