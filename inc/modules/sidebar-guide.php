<ul class="sidebar__guide">
	<li class="sidebar__guide__item">
		<a href="#grid" class="sidebar__guide__item__link active">
			Grid
		</a>
	</li>
	<li class="sidebar__guide__item">
		<a href="#typography" class="sidebar__guide__item__link">
			Typography
		</a>
	</li>
	<li class="sidebar__guide__item">
		<a href="#forms" class="sidebar__guide__item__link">
			Forms
		</a>
	</li>
	<li class="sidebar__guide__item">
		<a href="#checkbo" class="sidebar__guide__item__link">
			Checkbox & Radio
		</a>
	</li>
	<li class="sidebar__guide__item">
		<a href="#dropdown" class="sidebar__guide__item__link">
			Dropdown
		</a>
	</li>
	<li class="sidebar__guide__item">
		<a href="#quantity" class="sidebar__guide__item__link">
			Quantity
		</a>
	</li>
	<li class="sidebar__guide__item">
		<a href="#upload" class="sidebar__guide__item__link">
			Upload Form
		</a>
	</li>
	<li class="sidebar__guide__item">
		<a href="#head" class="sidebar__guide__item__link">
			Head Menu
		</a>
	</li>
	<li class="sidebar__guide__item">
		<a href="#alert" class="sidebar__guide__item__link">
			Alert
		</a>
	</li>
	<li class="sidebar__guide__item">
		<a href="#color" class="sidebar__guide__item__link">
			Choose color
		</a>
	</li>
</ul>