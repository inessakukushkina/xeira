<div class="row m-t30 p-b100">
    <div class="col-sm-3 col-md-2">
        <div class="sidebar">
            <h4 class="sidebar__title2">About us</h4>

            <ul class="sidebar__menu">
                <li class="sidebar__menu__item">
                    <a href="">The Company</a>
                </li>
                <li class="sidebar__menu__item">
                    <a href="">Impressum</a>
                </li>
                <li class="sidebar__menu__item">
                    <a href="">Contact</a>
                </li>
                <li class="sidebar__menu__item">
                    <a href="">Shipping</a>
                </li>
                <li class="sidebar__menu__item">
                    <a href="">Return Policy</a>
                </li>
                <li class="sidebar__menu__item">
                    <a href="">Privacy Policy</a>
                </li>
                <li class="sidebar__menu__item">
                    <a href="">Terms and Condition</a>
                </li>
                <li class="sidebar__menu__item active">
                    <a href="">Order Precess</a>
                </li>
                <li class="sidebar__menu__item">
                    <a href="">Blog</a>
                </li>
            </ul>
        </div>
    </div>
    <div class="col-sm-9 col-md-10">
        <h5 class="page-title">Hilfe zum Bestellvorgang</h5>

        <div class="page-content">
            <div class="page-content__row">
                <h6 class="page-content__title">Warenborb</h6>
                <p>Im Warenkorb legen Sie bitte die Artikelmenge, Zahlungsart und Versandweg fest. Durch einen klick auf den Button „zur Kasse gehen" beginnen Sie den Bestellvorgang.</p>
            </div>

            <div class="page-content__row">
                <h6 class="page-content__title page-content__title--has-dot">Login</h6>
                <p>Wenn Sie noch nicht eingeloggt sind, müssen Sie sich nun einloggen. Dazu geben Sie bitte ihre E-Mail Adresse und das von ihnen vergebene Passwort an.</p>

                <p>
                    <strong class="page-content__block">Ist dies ihre erste Bestellung in unserem Webshop?</strong>
                    Geben Sie dann bitte nur ihre E-Mail Adresse an und wählen Sie „Ich bin ein neuer Kunde".
                </p>
            </div>

            <div class="page-content__row">
                <h6 class="page-content__title page-content__title--has-dot">Rechnungsanschrift</h6>
                <p>Bitte geben Sie ihre vollständige Rechnungsanschrift an. In diesem Schritt müssen Sie die Kenntnis unserer AGB bestätigen. Bitte lesen Sie unsere AGB und markieren darauf diesen Punkt.</p>

                <p>Nur wenn Sie ein neuer Kunde sind, muss ein Passwort hinterlegt werden.</p>
            </div>

            <div class="page-content__row">
                <h6 class="page-content__title page-content__title--has-dot">Lieferanschrift</h6>
                <p>Wählen Sie in diesem Bereich bitte die Lieferanschrift, an welche ihre Bestellung geliefert werden soll. Ist die korrekte Adresse nicht vorhanden, dann legen Sie bitte eine neue Lieferanschrift an.</p>
            </div>

            <div class="page-content__row">
                <h6 class="page-content__title page-content__title--has-dot">Latschrift</h6>
                <p>Nur bei Zahlungsart Lastschrift erscheint nun die Abfrage der Kontodaten. Bitte achten Sie auf die korrekte Schreibweise ihrer Kontodaten.</p>
            </div>

            <div class="page-content__row">
                <h6 class="page-content__title page-content__title--has-dot">Bestellubersicht</h6>
                <p>Im letzten Schritt erhalten Sie eine Übersicht über alle angegebenen Daten und alle Artikel inkl. Gesamtbetrag.</p>
                <p>Durch einen klick auf den Button "Bestellung abschicken" wird die Bestellung rechtsverbindlich Abgesendet.</p>
                <p>Der Kaufvertrag wird durch die Bestätigung des Anbieters per E-Mail rechtskräftig abgeschlossen.</p>
            </div>
        </div>
    </div>
</div>