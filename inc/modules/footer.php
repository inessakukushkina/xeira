		<footer class="footer">
			<div class="container">
				<div class="row">
					<div class="col-sm-3">
						<h6 class="footer__title">
							About Justshirt
						</h6>
						<p class="footer__text">
							In our company, the customer is the focus and that is why our creative team is always in mind to offer the best products and the best service interested in them.
						</p>
					</div>
					<div class="col-sm-3">
						<h6 class="footer__title">
							Theme Features
						</h6>
						<ul class="footer__list">
							<li class="footer__list__item">
								<a href="" class="footer__list__item__link">Shop</a>
							</li>
							<li class="footer__list__item">
								<a href="" class="footer__list__item__link">Sell</a>
							</li>
							<li class="footer__list__item">
								<a href="" class="footer__list__item__link">Create</a>
							</li>
							<li class="footer__list__item">
								<a href="" class="footer__list__item__link">Promote</a>
							</li>
						</ul>
					</div>
					<div class="col-sm-3">
						<h6 class="footer__title">
							Key Features
						</h6>
						<ul class="footer__list">
							<li class="footer__list__item">
								<a href="" class="footer__list__item__link">About Us</a>
							</li>
							<li class="footer__list__item">
								<a href="" class="footer__list__item__link">Data protection</a>
							</li>
							<li class="footer__list__item">
								<a href="" class="footer__list__item__link">Condition</a>
							</li>
							<li class="footer__list__item">
								<a href="" class="footer__list__item__link">Withdrawal</a>
							</li>
							<li class="footer__list__item">
								<a href="" class="footer__list__item__link">Shipping and Payment</a>
							</li>
							<li class="footer__list__item">
								<a href="" class="footer__list__item__link">Bestellvorgang</a>
							</li>
							<li class="footer__list__item">
								<a href="" class="footer__list__item__link">Fair appearances</a>
							</li>
						</ul>
					</div>
					<div class="col-sm-3">
						<h6 class="footer__title">
							Company Info
						</h6>
						<ul class="footer__list">
							<li class="footer__list__item">
								<span>Telefon (+49) 069 93997583</span>
								<span>Fax (+49)069 38030931</span>
							</li>
							<li class="footer__list__item">
								<span>info@justshirt.de</span>
								<span>support@justshirt.de</span>
							</li>
							<li class="footer__list__item">
								Skype: justshirt.contact
							</li>
						</ul>
					</div>
				</div>
			</div>
		</footer>
		<div class="copyright">
			<div class="container">
				<p class="copyright__text pull-lg-left">
					Copyright © 2012-2015 JUSTSHIRT All Rights Reserved.
				</p>
				<div class="pull-lg-right">
					<a href="">
						<img src="images/shipping/dhl.png" class="copyright__picture" alt="">
					</a>
					<a href="">
						<img src="images/shipping/deutsche-post.png" class="copyright__picture" alt="">
					</a>
					<a href="">
						<img src="images/shipping/dpd.png" class="copyright__picture" alt="">
					</a>
					<a href="">
						<img src="images/shipping/pay-pal.png" class="copyright__picture" alt="">
					</a>
					<a href="">
						<img src="images/shipping/amazon.png" class="copyright__picture" alt="">
					</a>
					<a href="">
						<img src="images/shipping/nachnahme.png" class="copyright__picture" alt="">
					</a>
					<a href="">
						<img src="images/shipping/vorkasse.png" class="copyright__picture" alt="">
					</a>
				</div>
			</div>
		</div>
		<script src="js/bootstrap.min.js"></script>
		<script src="js/bootstrap-colorpicker.min.js"></script>
		<script src="js/bootstrap-select.js"></script>
		<script src="tinymce/tinymce.min.js"></script>
		<script src="js/highcharts.js"></script>
		<script src="js/exporting.js"></script>
		<script src="js/slick.min.js"></script>
		<script src="js/chosen.jquery.js"></script>
		<script src="js/bootstrap-tagsinput.min.js"></script>
		<script src="js/jquery.zoom.min.js"></script>
		<script src="js/checkBo.min.js"></script>
		<script src="js/dropzone.min.js"></script>
		<script src="js/dropzone_settings.js"></script>
		<script src="js/select2.min.js"></script>
		<script src="js/chart.js"></script>
		<script src="js/function.js"></script>
		<script src="js/scripts.js"></script>
	</body>
</html>