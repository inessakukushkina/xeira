<?php
	$_GET['id'] = isset($_GET['id']) ? $_GET['id'] : 'catalog';
	$id = $_GET['id'];
?>
<?php require_once 'inc/config/all.php'; ?>
<?php require_once 'inc/modules/header.php';?>

<header class="nav navbar-fixed-top header p-l40">
	<p class="header__title">
		Xeira guidline
	</p>
</header>

<?php require_once 'inc/modules/sidebar-guide.php';?>

<div class="content__guide">
	<h2 id="grid" class="content__guide__title">Grid</h2>
	<div class="table-responsive">
        <table class="table table-bordered table-striped">
            <thead>
            <tr>
                <th></th>
                <th>
                    Extra small devices
                    <small>Phones (< 768px)</small>
                </th>
                <th>
                    Small devices
                    <small>Tablets (≥ 768px)</small>
                </th>
                <th>
                    Medium devices
                    <small>Desktops (>992px)</small>
                </th>
                <th>
                    Large devices
                    <small>Desktops (≥ 1200px)</small>
                </th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <th class="text-nowrap">Grid behavior</th>
                <td>Horizontal at all times</td>
                <td colspan="3">Collapsed to start, horizontal above breakpoints</td>
            </tr>
            <tr>
                <th class="text-nowrap">Container width</th>
                <td>None (auto)</td>
                <td>750px</td>
                <td>970px</td>
                <td>1170px</td>
            </tr>
            <tr>
                <th class="text-nowrap">Class prefix</th>
                <td><code>.col-xs-*</code></td>
                <td><code>.col-sm-*</code></td>
                <td><code>.col-md-*</code></td>
                <td><code>.col-lg-*</code></td>
            </tr>
            <tr>
                <th class="text-nowrap"># of columns</th>
                <td colspan="4">12</td>
            </tr>
            <tr>
                <th class="text-nowrap">Gutter width</th>
                <td colspan="4">30px (15px on each side of a column)</td>
            </tr>
            <tr>
                <th class="text-nowrap">Nestable</th>
                <td colspan="4">Yes</td>
            </tr>
            <tr>
            </tbody>
        </table>
    </div>
    <div class="container-fluid p-0">
        <div class="row m-b15">
            <div class="col-sm-12">
                <div class="bscol">col-sm-12</div>
            </div>
        </div>
        <div class="row m-b15">
            <div class="col-sm-6"><div class="bscol">col-sm-6</div></div>
            <div class="col-sm-6"><div class="bscol">col-sm-6</div></div>
        </div>
        <div class="row m-b15">
            <div class="col-sm-4"><div class="bscol">col-sm-4</div></div>
            <div class="col-sm-4"><div class="bscol">col-sm-4</div></div>
            <div class="col-sm-4"><div class="bscol">col-sm-4</div></div>
        </div>
        <div class="row m-b15">
            <div class="col-sm-3"><div class="bscol">col-sm-3</div></div>
            <div class="col-sm-3"><div class="bscol">col-sm-3</div></div>
            <div class="col-sm-3"><div class="bscol">col-sm-3</div></div>
            <div class="col-sm-3"><div class="bscol">col-sm-3</div></div>
        </div>
        <div class="row m-b15">
            <div class="col-sm-2"><div class="bscol">col-sm-2</div></div>
            <div class="col-sm-2"><div class="bscol">col-sm-2</div></div>
            <div class="col-sm-2"><div class="bscol">col-sm-2</div></div>
            <div class="col-sm-2"><div class="bscol">col-sm-2</div></div>
            <div class="col-sm-2"><div class="bscol">col-sm-2</div></div>
            <div class="col-sm-2"><div class="bscol">col-sm-2</div></div>
        </div>
        <div class="row m-b15">
            <div class="col-sm-4"><div class="bscol">col-sm-4</div></div>
            <div class="col-sm-8"><div class="bscol">col-sm-8</div></div>
        </div>
    </div>

    <h2 id="typography" class="content__guide__title">Typography</h2>
    <p>HTML headings, <code>&lt;h1&gt;</code> through <code>&lt;h6&gt;</code></p>
    <div class="demo">
      	<table class="table">
            <thead>
                <tr>
                    <th>Tag</th>
                    <th>Font size</th>
                </tr>
            </thead>
            <tbody>
				<tr>
					<td><h1>h1. heading</h1></td>
					<td class="type-info">32px</td>
				</tr>
				<tr>
					<td><h2>h2. heading</h2></td>
					<td class="type-info">28px</td>
				</tr>
				<tr>
					<td><h3>h3. heading</h3></td>
					<td class="type-info">24px</td>
				</tr>
				<tr>
					<td><h4>h4. heading</h4></td>
					<td class="type-info">20px</td>
				</tr>
				<tr>
					<td><h5>h5. heading</h5></td>
					<td class="type-info">16px</td>
				</tr>
				<tr>
					<td><h6>h6. heading</h6></td>
					<td class="type-info">12px</td>
				</tr>
          	</tbody>
      </table>
    </div>

    <h2 id="forms" class="content__guide__title">Forms</h2>
    <div class="row">
        <div class="col-sm-6">
            <h3 class="content__guide__subtitle">Input</h3>
            <div class="row">
                <div class="col-sm-8">
                    <div class="form-group">
                        <input type="text" class="form-control" placeholder="Placeholder's text is here">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-8">
                    <div class="form-group has-error">
                        <input type="text" class="form-control">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-8">
                    <div class="form-group">
                        <input type="text" class="form-control" disabled="disabled" value="email@email.com">
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-6">
            <h3 class="content__guide__subtitle">Textarea</h3>
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                       <textarea rows="5" class="form-control"></textarea>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group has-error">
                       <textarea rows="5" class="form-control"></textarea>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <h2 id="checkbo" class="content__guide__title">Checkbox & Radio</h2>
    <div class="row custom-form">
        <div class="col-sm-6">
            <h3 class="content__guide__subtitle">Checkbox</h3>
            <label class="cb-checkbox w-100">
                <input type="checkbox">
                Check Me
            </label>
            <label class="cb-checkbox w-100">
                <input type="checkbox">
                Check This
            </label>
        </div>
        <div class="col-sm-6">
            <h3 class="content__guide__subtitle">Radio</h3>
            <label class="cb-radio w-100">
                <input type="radio" name="group-name">
                Check Me
            </label>
            <label class="cb-radio w-100">
                <input type="radio" name="group-name">
                Check This
            </label>
        </div>
    </div>

    <h2 id="dropdown" class="content__guide__title">Dropdown</h2>
    <div class="row">
        <div class="col-sm-6">
            <select class="chosen-select">
                <option value="">New York</option>
                <option value="">Minsk</option>
                <option value="">Moscow</option>
                <option value="">Kiev</option>
                <option value="">Prague</option>
            </select>
        </div>
    </div>

    <h2 id="pagination" class="content__guide__title">Pagination</h2>
    <ul class="pagination">
        <li class="pagination__item">
            <a href="" class="pagination__item__link disabled">
                «
            </a>
        </li>
        <li class="pagination__item">
            <a href="" class="pagination__item__link active">
                1
            </a>
        </li>
        <li class="pagination__item">
            <a href="" class="pagination__item__link">
                2
            </a>
        </li>
        <li class="pagination__item">
            <a href="" class="pagination__item__link">
                3
            </a>
        </li>
        <li class="pagination__item">
            <a href="" class="pagination__item__link">
              »
            </a>
        </li>
    </ul>

    <ul class="pagination">
        <li class="pagination__item">
            <a href="" class="pagination__item__link">
                «
            </a>
        </li>
        <li class="pagination__item">
            <a href="" class="pagination__item__link">
                1
            </a>
        </li>
        <li class="pagination__item">
            <a href="" class="pagination__item__link active">
                2
            </a>
        </li>
        <li class="pagination__item">
            <a href="" class="pagination__item__link">
                3
            </a>
        </li>
        <li class="pagination__item">
            <a href="" class="pagination__item__link">
              »
            </a>
        </li>
    </ul>

    <h2 id="quantity" class="content__guide__title">Quantity</h2>
    <div class="clearfix">
        <div class="quantity">
            <p class="quantity__text">Qty:</p> 
            <div class="quantity__block input-group">
                <button class="quantity__decrement input-group-btn" disabled="disabled">-</button>
                <input class="quantity__control form-control" type="text" value="1">    
                <button class="quantity__increment input-group-btn">+</button>          
            </div>
        </div>
    </div>    

    <h2 id="upload" class="content__guide__title">Upload Form</h2>
    <a href="http://www.dropzonejs.com/" class="link__page-20">Documentation</a>
    <form action="/" id="dropFiles" class="dropzone">
        <div class="dropzone__content">
            <h3 class="dropzone__content__title">
                Drag your design here
            </h3>
            <div class="dropzone__content__or">
                <span class="dropzone__content__or__text">or</span>
            </div>
            <button class="btn__accept">
                Brows Design
            </button>
            <ul class="dropzone__content__list">
                <li class="dropzone__content__list__item">
                    JPG
                </li>
                <li class="dropzone__content__list__item">
                    AI
                </li>
                <li class="dropzone__content__list__item">
                    GIF
                </li>
                <li class="dropzone__content__list__item">
                    SDR
                </li>
                <li class="dropzone__content__list__item">
                    EPS
                </li>
                <li class="dropzone__content__list__item">
                    PNG
                </li>
                <li class="dropzone__content__list__item">
                    SVG
                </li>
            </ul>
            <p class="dropzone__content__text">
                Image size 40 to 4000 px, file size 10 MB max.
            </p>
            <p class="dropzone__content__text">
                Vector file 3 colors max.
            </p>
        </div>          
    </form>

    <h2 id="head" class="content__guide__title">Head menu</h2>
    <h4 class="content__guide__title">Login</h4>
    <?php require_once('inc/modules/head_login.php'); ?>    
    </div>
    <h4 class="content__guide__title">Not login</h4>
    <?php require_once('inc/modules/head_notLogin.php'); ?>
    </div>
    <h4 class="content__guide__title">Deutsch</h4>
     <?php require_once('inc/modules/head_de.php'); ?>
    </div>
    <h4 class="content__guide__title">Alert</h4> 
    <h6 class="content__guide__title">Good alert</h6>
    <header class="nav header">
        <div class="container">
            <p class="header__title">
                Invite & earn
            </p>        
        </div> 
        <div class="alert alert-good">
            <div class="container">
                <p class="alert__text">
                    <i class="icon-good-alert"></i>
                    Some good alert text
                    <i class="icon-good-remove" data-remove=".alert"></i>
                </p>
            </div>
        </div> 
    </header> 
    <div class="alert alert-good">
        <div class="container">
            <p class="alert__text">
                <i class="icon-good-alert"></i>
                Some good alert text
                <i class="icon-good-remove" data-remove=".alert"></i>
            </p>
        </div>
    </div> 
    <h6 class="content__guide__title">Bad alert</h6>
    <header class="nav header">
        <div class="container">
            <p class="header__title">
                Invite & earn
            </p>        
        </div> 
        <div class="alert alert-bad">
            <div class="container">
                <p class="alert__text">
                    <i class="icon-bad-alert"></i>
                    Sample Fashion Product has been deleted. <a href="" class="link__page-16"><i class="fa fa-refresh"></i> Restore</a>
                    <i class="icon-bad-remove" data-remove=".alert"></i>
                </p>
            </div>
        </div> 
    </header>  
    <div class="alert alert-bad">
        <div class="container">
            <p class="alert__text">
                <i class="icon-bad-alert"></i>
                Sample Fashion Product has been deleted. <a href="" class="link__page-16"><i class="fa fa-refresh"></i> Restore</a>
                <i class="icon-bad-remove" data-remove=".alert"></i>
            </p>
        </div>
    </div>

    <h6 class="content__guide__title">Configurator popover</h6>
    <div class="row">
        <div class="col-sm-3 col-sm-offset-3">
             <div class="configurator__settings__content">
                <h3 class="configurator__settings__title">
                    Product
                </h3>
                <p class="configurator__settings__lighten__text">Select style</p>
                <div class="configurator__create__settings">
                    <div class="configurator__create__settings__item active clearfix" data-popover="">
                        <div class="configurator__create__settings__item__content">
                            <div class="shirt__preview">
                                <img src="images/icons/long-shirt.png" alt="">
                            </div>
                            <div class="configurator__create__settings__item__inner">
                                <h3 class="configurator__create__settings__item__title">
                                    Teespring Women’s
                                </h3>
                                <p class="configurator__create__settings__item__text">
                                    Premium materials XS - 4XL
                                </p>
                                <a href="" class="link__page-12">
                                    Change
                                </a>
                            </div>
                        </div>
                        <div class="popover popover__left" data-toggle-popover="" style="display: block; top: -87.5px; left: -45px;">
                            <div class="popover__color">
                                <div class="popover__block">
                                    <div class="popover__block__color" style="background-color:red;"></div>
                                </div>
                                <div class="popover__block">
                                    <div class="popover__block__color" style="background-color:pink;"></div>
                                </div>
                                <div class="popover__block">
                                    <div class="popover__block__color" style="background-color:orange;"></div>
                                </div>
                                <div class="popover__block">
                                    <div class="popover__block__color" style="background-color:green;"></div>
                                </div>
                                <div class="popover__block">
                                    <div class="popover__block__color" style="background-color:red;"></div>
                                </div>
                                <div class="popover__block">
                                    <div class="popover__block__color" style="background-color:pink;"></div>
                                </div>
                                <div class="popover__block">
                                    <div class="popover__block__color" style="background-color:orange;"></div>
                                </div>
                                <div class="popover__block last__block__color" data-popover-color="">
                                    <div class="popover__block__color"></div>
                                </div>
                            </div>
                            <div class="popover__color">                
                                <div class="popover__block">
                                    <div class="popover__block__color" style="background-color:red;"></div>
                                </div>
                                <div class="popover__block">
                                    <div class="popover__block__color" style="background-color:pink;"></div>
                                </div>
                                <div class="popover__block">
                                    <div class="popover__block__color" style="background-color:orange;"></div>
                                </div>
                                <div class="popover__block">
                                    <div class="popover__block__color" style="background-color:green;"></div>
                                </div>
                                <div class="popover__block">
                                    <div class="popover__block__color" style="background-color:red;"></div>
                                </div>
                                <div class="popover__block">
                                    <div class="popover__block__color" style="background-color:pink;"></div>
                                </div>
                                <div class="popover__block">
                                    <div class="popover__block__color" style="background-color:orange;"></div>
                                </div>
                                <div class="popover__block">
                                    <div class="popover__block__color" style="background-color:red;"></div>
                                </div>
                            </div>                      
                        </div>
                    </div>
                    <div class="configurator__create__settings__item clearfix" data-popover="">
                        <div class="configurator__create__settings__item__content">
                            <div class="shirt__preview">
                                <img src="images/icons/long-shirt.png" alt="">
                            </div>
                            <div class="configurator__create__settings__item__inner">
                                <h3 class="configurator__create__settings__item__title">
                                    Teespring Women’s
                                </h3>
                                <p class="configurator__create__settings__item__text">
                                    Premium materials XS - 4XL
                                </p>
                            </div>
                        </div>
                        <div class="popover popover__left" data-toggle-popover="">
                            <div class="popover__color">
                                <div class="popover__block">
                                    <div class="popover__block__color" style="background-color:red;"></div>
                                </div>
                                <div class="popover__block">
                                    <div class="popover__block__color" style="background-color:pink;"></div>
                                </div>
                                <div class="popover__block">
                                    <div class="popover__block__color" style="background-color:orange;"></div>
                                </div>
                                <div class="popover__block">
                                    <div class="popover__block__color" style="background-color:green;"></div>
                                </div>
                                <div class="popover__block">
                                    <div class="popover__block__color" style="background-color:red;"></div>
                                </div>
                                <div class="popover__block">
                                    <div class="popover__block__color" style="background-color:pink;"></div>
                                </div>
                                <div class="popover__block">
                                    <div class="popover__block__color" style="background-color:orange;"></div>
                                </div>
                                <div class="popover__block last__block__color" data-popover-color="">
                                    <div class="popover__block__color"></div>
                                </div>
                            </div>
                            <div class="popover__color">                
                                <div class="popover__block">
                                    <div class="popover__block__color" style="background-color:red;"></div>
                                </div>
                                <div class="popover__block">
                                    <div class="popover__block__color" style="background-color:pink;"></div>
                                </div>
                                <div class="popover__block">
                                    <div class="popover__block__color" style="background-color:orange;"></div>
                                </div>
                                <div class="popover__block">
                                    <div class="popover__block__color" style="background-color:green;"></div>
                                </div>
                                <div class="popover__block">
                                    <div class="popover__block__color" style="background-color:red;"></div>
                                </div>
                                <div class="popover__block">
                                    <div class="popover__block__color" style="background-color:pink;"></div>
                                </div>
                                <div class="popover__block">
                                    <div class="popover__block__color" style="background-color:orange;"></div>
                                </div>
                                <div class="popover__block">
                                    <div class="popover__block__color" style="background-color:red;"></div>
                                </div>
                            </div>
                            <div class="popover__color">                
                                <div class="popover__block">
                                    <div class="popover__block__color" style="background-color:red;"></div>
                                </div>
                                <div class="popover__block">
                                    <div class="popover__block__color" style="background-color:pink;"></div>
                                </div>
                                <div class="popover__block">
                                    <div class="popover__block__color" style="background-color:orange;"></div>
                                </div>
                                <div class="popover__block">
                                    <div class="popover__block__color" style="background-color:green;"></div>
                                </div>
                                <div class="popover__block">
                                    <div class="popover__block__color" style="background-color:red;"></div>
                                </div>
                                <div class="popover__block">
                                    <div class="popover__block__color" style="background-color:pink;"></div>
                                </div>
                                <div class="popover__block">
                                    <div class="popover__block__color" style="background-color:orange;"></div>
                                </div>
                                <div class="popover__block">
                                    <div class="popover__block__color" style="background-color:red;"></div>
                                </div>
                            </div>              
                        </div>
                    </div>
                </div>
                <p class="configurator__settings__lighten__text">Select Print Type and Color</p>
                <div class="row">
                    <div class="col-xs-12">
                        <div class="form-group">
                            <select class="chosen-select">
                                <option value="">All sizes</option>
                                <option value="">S</option>
                                <option value="">M</option>
                                <option value="">L</option>
                            </select>
                        </div>      
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <a href="" class="link__page-12">
                            More about Print Type
                        </a>
                    </div>
                </div>
                <p class="configurator__settings__lighten__text p-t15">Size and Qty</p>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <select class="chosen-select">
                                <option value="">All sizes</option>
                                <option value="">S</option>
                                <option value="">M</option>
                                <option value="">L</option>
                            </select>
                        </div>                  
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <div class="quantity configurate__quantity">                   
                                <div class="quantity__block input-group">
                                    <button class="quantity__decrement input-group-btn">-</button>
                                    <input class="quantity__control form-control" type="text" value="8">    
                                    <button class="quantity__increment input-group-btn">+</button>          
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <a href="" class="link__page-12">
                            + Add
                        </a>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <p class="cost p-t20">$7.14</p>
                    </div>
                    <div class="col-sm-6">
                        <button class="btn__accept" data-forward="">
                            Continue
                        </button>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <p class="cost m-b0">$7.14</p>
                        <p class="text-muted">
                            plus delivery
                        </p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <button class="btn__accept" data-forward="">
                            Add to Cart
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <h6 class="content__guide__title">Choode color</h6>
    
    <?php require_once 'inc/modules/footer.php';?>
</div>