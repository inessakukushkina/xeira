var accordeon = function($link){
	$link.on('click', function(){
		var $this = $(this);
    	if($this.next('ul').length > 0){
    		var $item 		= $this.closest('li'),
    			$parentItem = $item.parent(),
    			$_click 	= $item.find('ul:first').is(':visible');
    		if(!$_click) {
				$parentItem.find('> li ul:visible').slideToggle();
				$item.find('a').removeClass('active');	
				$item.removeClass('active');			
				$parentItem.find('li').removeClass('active');	
				$parentItem.find('a').removeClass('active');
			}				
			$item.find('ul:first').slideToggle();
			$item.find('a').toggleClass('active');
			$item.toggleClass('active');
			return false;
    	}
	})
}

CountGoods = {
	numericValidationString: /^[0-9]+$/
};

function ValidateNumeric(input){
	return CountGoods.numericValidationString.test(input);
}
$.fn.quantityInput = function(){
    $(this).each(function(){
        var $decrement = $(this).find('.quantity__decrement'),
            $quantity = $(this).find('.quantity__control'),
            $incriment = $(this).find('.quantity__increment');

  		$decrement.on('click', function () {
            if (ValidateNumeric($quantity.val())) {
                var number = parseInt($quantity.val());
                number--;
                if (number < 1) {
                	number = 1;
                	console.log('111111111');
                } else {
                	$quantity.val(number);
                }                
            }
            else {
            	$quantity.val(1);
            	console.log('111111111');
            }
            return false;

        });
     	$incriment.on('click', function () {
            if (ValidateNumeric($quantity.val())) {
                var number = parseInt($quantity.val());
                number++;
                if (number < 1) {
                	number = 1;
                } else{
                	$quantity.val(number);
                }                
            }
            else {
            	$quantity.val(1);
            	console.log('adsavs');
            }
            return false;
        });
    });
};

$(document).ready(function(){
	clickActive($('.sidebar__guide__item__link'), $('.sidebar__guide'));
	
	accordeon($('.shop__list__item__link'));

	$('.custom-form').checkBo();
	$('.chosen-select').chosen();
	$('.chosen-search').remove();

	$('.quantity__block').quantityInput();

	$(document).on('click', '.btn-menu', function(){
		$('.shop__list').slideToggle();
	})

	$(document).on('click', '.btn-search', function(){
		$('.form-search').slideToggle();
		$(this).toggleClass('btn-remove');
	});

	$(document).on('click', '.filter__block .sidebar_title', function(){
		$('.filter__block__title').slideToggle();
		$(this).toggleClass('active');
		$('.filter__block .row').slideUp();
	})

	$(document).on('click', '.filter__block__title', function(){
		$('.filter__block .row').slideUp();
		$('.filter__block__title').removeClass('active');

		if($(this).hasClass('active')){
			$(this).next('.row').slideUp();
			$(this).removeClass('active');	
		} else{
			$(this).next('.row').slideDown();
			$(this).addClass('active');	
		}				
	})
})