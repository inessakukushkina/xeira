<ul class="create__menu">
	<li class="create__menu__item">
		<a href="" class="create__menu__item__link">
			Women
		</a>
	</li>
	<li class="create__menu__item">
		<a href="" class="create__menu__item__link">
			Men
		</a>
	</li>
	<li class="create__menu__item">
		<a href="" class="create__menu__item__link">
			Kids
		</a>
	</li>
</ul>
<div class="is-relative block__upload">
	<ul class="configurator__aside clearfix">
		<li class="configurator__aside__item">
			<a href="" class="configurator__aside__item__link active">
				<span>T-shirts</span>
				<i class="zmdi zmdi-texture"></i>
			</a>
		</li>
		<li class="configurator__aside__item">
			<a href="" class="configurator__aside__item__link">
				<span>Hoddies</span>
				<i class="zmdi zmdi-texture"></i>
			</a>
		</li>
		<li class="configurator__aside__item is-border">
			<a href="" class="configurator__aside__item__link">
				<span>Tank top</span>
				<i class="zmdi zmdi-texture"></i>
			</a>
		</li>
		<li class="configurator__aside__item">
			<a href="" class="configurator__aside__item__link">
				<span>Cups</span>
				<i class="zmdi zmdi-texture"></i>
			</a>
		</li>
		<li class="configurator__aside__item">
			<a href="" class="configurator__aside__item__link">
				<span>Suspenders</span>
				<i class="zmdi zmdi-texture"></i>
			</a>
		</li>
		<li class="configurator__aside__item">
			<a href="" class="configurator__aside__item__link">
				<span>Belts</span>
				<i class="zmdi zmdi-texture"></i>
			</a>
		</li>
		<li class="configurator__aside__item">
			<a href="" class="configurator__aside__item__link">
				<span>Scarfs</span>
				<i class="zmdi zmdi-texture"></i>
			</a>
		</li>
		<li class="configurator__aside__item is-border">
			<a href="" class="configurator__aside__item__link">
				<span>Cooking Apron</span>
				<i class="zmdi zmdi-texture"></i>
			</a>
		</li>
		<li class="configurator__aside__item">
			<a href="" class="configurator__aside__item__link">
				<span>Caps</span>
				<i class="zmdi zmdi-texture"></i>
			</a>
		</li>
		<li class="configurator__aside__item">
			<a href="" class="configurator__aside__item__link">
				<span>Towel</span>
				<i class="zmdi zmdi-texture"></i>
			</a>
		</li>
		<li class="configurator__aside__item">
			<a href="" class="configurator__aside__item__link">
				<span>Pillows</span>
				<i class="zmdi zmdi-texture"></i>
			</a>
		</li>
		<li class="configurator__aside__item">
			<a href="" class="configurator__aside__item__link">
				<span>Bags</span>
				<i class="zmdi zmdi-texture"></i>
			</a>
		</li>		
	</ul>
	<div class="clearfix">
		<div class="configurator__aside__xs">
			<select class="chosen-select">
				<option>T-shirts</option>
				<option>Hoddies</option>
				<option>Tank Tops</option>
				<option>Caps</option>
				<option>Suspenders</option>
				<option>Belts</option>
				<option>Scarfs</option>
				<option>Cooking Apron</option>
				<option>Cups</option>
				<option>Towels</option>
				<option>Pillows</option>
				<option>Bags</option>
			</select>
		</div>

		<div class="configurator__settings">
			<div class="configurator__settings__content">
				<h3 class="configurator__settings__title">
					Add Text or Artwork
				</h3>
				<button class="btn btn-conf btn-conf-indent" data-show=".add-text" data-hide=".add-artwork" data-toggle>
					<i class="fa fa-font"></i> Add text
					<i class="zmdi zmdi-upload"></i>
				</button>
				<button class="btn btn-conf" data-show=".add-artwork" data-hide=".add-text" data-toggle>
					<i class="fa fa-image"></i> Upload Artwork
					<i class="zmdi zmdi-upload"></i>
				</button>
			</div>

			<div class="configurator__settings__content add-text is-hide">
				<h3 class="configurator__settings__title">
					Print Type
				</h3>
				<p class="configurator__settings__lighten__text">
					Select type
				</p>
				<div class="form-group">
					<select class="chosen-select form-control">
						<option value="">Flock</option>
						<option value="">Flock 2</option>
						<option value="">Flock 3</option>
						<option value="">Flock 4</option>
					</select>
				</div>
				<div class="xs-min-hidden">
					<h3 class="configurator__settings__title">
						Text
					</h3>
					<p class="configurator__settings__lighten__text">
						Enter text below
					</p>
					<div class="form-group">
						<input type="text" class="form-control">
					</div>
					<p class="text-lighten">
						Choose a font
					</p>
					<div class="row">
						<div class="col-xs-7">
							<div class="form-group">
								<select class="chosen-select form-control">
									<option value="">Comic Sans</option>
									<option value="">Ubuntu</option>
									<option value="">Lato</option>
									<option value="">Open Sans</option>
								</select>
							</div>
						</div>
						<div class="col-xs-5">
							<select class="selectpicker color-select">
		                        <option data-content="<div class='color__block'><div class='color__mark color__red'></div></div>" title="<div class='color__block'><div class='color__mark color__red'></div></div>"></option>
		                        <option data-content="<div class='color__block'><div class='color__mark color__pink'></div></div>" title="<div class='color__block'><div class='color__mark color__pink'></div></div>"></option>
		                        <option data-content="<div class='color__block'><div class='color__mark color__green'></div></div>" title="<div class='color__block'><div class='color__mark color__green'></div></div>"></option>
		                        <option data-content="<div class='color__block'><div class='color__mark color__red'></div></div>" title="<div class='color__block'><div class='color__mark color__red'></div></div>"></option>
		                        <option data-content="<div class='color__block'><div class='color__mark color__pink'></div></div>" title="<div class='color__block'><div class='color__mark color__pink'></div></div>"></option>
		                        <option data-content="<div class='color__block'><div class='color__mark color__green'></div></div>" title="<div class='color__block'><div class='color__mark color__green'></div></div></div>"></option>
							</select>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-7">
							<div class="form-group">
								<select class="chosen-select form-control">
									<option value="">12px</option>
									<option value="">14px</option>
									<option value="">16px</option>
									<option value="">18px</option>
								</select>
							</div>
						</div>
						<div class="col-xs-5">
							<select class="selectpicker">
		                      <option data-content="<i class='fa fa-align-center'></i>" title="<i class='fa fa-align-center'></i>"></option>
		                      <option data-content="<i class='fa fa-align-justify'></i>" title="<i class='fa fa-align-justify'></i>"></option>
		                      <option data-content="<i class='fa fa-align-left'></i>" title="<i class='fa fa-align-left'></i>"></option>
		                      <option data-content="<i class='fa fa-align-right'></i>" title="<i class='fa fa-align-right'></i>"></option>
		                    </select>
						</div>		
					</div>
					<div class="row">
						<div class="col-xs-12">
							<button class="btn btn-conf btn-conf-indent">
								<i class="fa fa-refresh"></i>
								Rotate Artwork
							</button>
						</div>
						<div class="col-xs-12">
							<button class="btn btn-conf">
								<i class="fa fa-trash"></i>
								Remove Artwork
							</button>
						</div>
					</div>
				</div>

				<a href="" class="link__page-14">More about Print Type</a>
				<hr class="hr"/>
			</div>

			<div class="configurator__settings__content add-artwork is-hide">
				<h3 class="configurator__settings__title">
					Atrwork
				</h3>
				<div data-popover>
					<button class="btn btn-conf" >
						<i class="fa fa-upload"></i>
						Browse Artwork
					</button>
					<div class="popover__modal popover__left" data-toggle-popover>	
						<div class="row">
							<div class="col-md-4 p-t10">
								<div class="form-group form-search">
									<input type="text" class="form-control" placeholder="Search entire store here...">
								</div>
							</div>
							<div class="col-md-2 md-right p-t10">
								<p class="text-page p-t5 m-b0">Sort By:</p>				
							</div>
							<div class="col-md-3 p-t10">
								<div class="form-group">
									<select class="form-control">
										<option value="">All categories</option>
										<option value="">Pig</option>
										<option value="">Raccoon</option>
									</select>
								</div>
							</div>
							<div class="col-md-3">
								<button class="btn__accept m-0">
									Upload design
								</button>
							</div>
						</div>	
						<div class="popover__modal__content">
							<div class="row p-r15">
								<div class="col-xs-12">
									<div class="b-Top">
										<div class="row">
											<div class="col-sm-6 col-md-4">
												<a href="" data-toggle="modal" data-target="#modal-design" class="profile__design__link">
													<span class="profile__design">
														<button class="profile__design__remove">
															<i class="fa fa-pencil"></i>
														</button>
														<button class="profile__design__trash">
															<i class="fa fa-trash"></i>
														</button>
														<img src="images/profile/profile1.png" class="profile__design__picture">
														<span class="profile__design__price">
															$5.00
														</span>
													</span>
												</a>				
											</div>
											<div class="col-sm-6 col-md-4">
												<a href="" class="profile__design__link">
													<span class="profile__design">
														<button class="profile__design__remove">
															<i class="fa fa-pencil"></i>
														</button>
														<button class="profile__design__trash">
															<i class="fa fa-trash"></i>
														</button>
														<img src="images/profile/profile2.png" class="profile__design__picture">
														<span class="profile__design__price">
															$5.00
														</span>
													</span>
												</a>				
											</div>
											<div class="col-sm-6 col-md-4">
												<a href="" class="profile__design__link">
													<span class="profile__design">
														<button class="profile__design__remove">
															<i class="fa fa-pencil"></i>
														</button>
														<button class="profile__design__trash">
															<i class="fa fa-trash"></i>
														</button>
														<img src="images/profile/profile3.png" class="profile__design__picture">
														<span class="profile__design__price">
															Free
														</span>
													</span>
												</a>				
											</div>
											<div class="col-sm-6 col-md-4">
												<a href="" class="profile__design__link">
													<span class="profile__design">
														<button class="profile__design__remove">
															<i class="fa fa-pencil"></i>
														</button>
														<button class="profile__design__trash">
															<i class="fa fa-trash"></i>
														</button>
														<img src="images/profile/profile1.png" class="profile__design__picture">
														<span class="profile__design__price">
															$5.00
														</span>
													</span>
												</a>				
											</div>
											<div class="col-sm-6 col-md-4">
												<a href="" class="profile__design__link">
													<span class="profile__design">
														<button class="profile__design__remove">
															<i class="fa fa-pencil"></i>
														</button>
														<button class="profile__design__trash">
															<i class="fa fa-trash"></i>
														</button>
														<img src="images/profile/profile2.png" class="profile__design__picture">
														<span class="profile__design__price">
															$5.00
														</span>
													</span>
												</a>				
											</div>
											<div class="col-sm-6 col-md-4">
												<a href="" class="profile__design__link">
													<span class="profile__design">
														<button class="profile__design__remove">
															<i class="fa fa-pencil"></i>
														</button>
														<button class="profile__design__trash">
															<i class="fa fa-trash"></i>
														</button>
														<img src="images/profile/profile3.png" class="profile__design__picture">
														<span class="profile__design__price">
															Free
														</span>
													</span>
												</a>				
											</div>
											<div class="col-sm-6 col-md-4">
												<a href="" class="profile__design__link">
													<span class="profile__design">
														<button class="profile__design__remove">
															<i class="fa fa-pencil"></i>
														</button>
														<button class="profile__design__trash">
															<i class="fa fa-trash"></i>
														</button>
														<img src="images/profile/profile1.png" class="profile__design__picture">
														<span class="profile__design__price">
															$5.00
														</span>
													</span>
												</a>				
											</div>
											<div class="col-sm-6 col-md-4">
												<a href="" class="profile__design__link">
													<span class="profile__design">
														<button class="profile__design__remove">
															<i class="fa fa-pencil"></i>
														</button>
														<button class="profile__design__trash">
															<i class="fa fa-trash"></i>
														</button>
														<img src="images/profile/profile2.png" class="profile__design__picture">
														<span class="profile__design__price">
															$5.00
														</span>
													</span>
												</a>				
											</div>
											<div class="col-sm-6 col-md-4">
												<a href="" class="profile__design__link">
													<span class="profile__design">
														<button class="profile__design__remove">
															<i class="fa fa-pencil"></i>
														</button>
														<button class="profile__design__trash">
															<i class="fa fa-trash"></i>
														</button>
														<img src="images/profile/profile3.png" class="profile__design__picture">
														<span class="profile__design__price">
															Free
														</span>
													</span>
												</a>				
											</div>
											<div class="col-sm-6 col-md-4">
												<a href="" class="profile__design__link">
													<span class="profile__design">
														<button class="profile__design__remove">
															<i class="fa fa-pencil"></i>
														</button>
														<button class="profile__design__trash">
															<i class="fa fa-trash"></i>
														</button>
														<img src="images/profile/profile1.png" class="profile__design__picture">
														<span class="profile__design__price">
															$5.00
														</span>
													</span>
												</a>				
											</div>
											<div class="col-sm-6 col-md-4">
												<a href="" class="profile__design__link">
													<span class="profile__design">
														<button class="profile__design__remove">
															<i class="fa fa-pencil"></i>
														</button>
														<button class="profile__design__trash">
															<i class="fa fa-trash"></i>
														</button>
														<img src="images/profile/profile2.png" class="profile__design__picture">
														<span class="profile__design__price">
															$5.00
														</span>
													</span>
												</a>				
											</div>
											<div class="col-sm-6 col-md-4">
												<a href="" class="profile__design__link">
													<span class="profile__design">
														<button class="profile__design__remove">
															<i class="fa fa-pencil"></i>
														</button>
														<button class="profile__design__trash">
															<i class="fa fa-trash"></i>
														</button>
														<img src="images/profile/profile3.png" class="profile__design__picture">
														<span class="profile__design__price">
															Free
														</span>
													</span>
												</a>				
											</div>
											<div class="col-sm-6 col-md-4">
												<a href="" class="profile__design__link">
													<span class="profile__design">
														<button class="profile__design__remove">
															<i class="fa fa-pencil"></i>
														</button>
														<button class="profile__design__trash">
															<i class="fa fa-trash"></i>
														</button>
														<img src="images/profile/profile2.png" class="profile__design__picture">
														<span class="profile__design__price">
															$5.00
														</span>
													</span>
												</a>				
											</div>
											<div class="col-sm-6 col-md-4">
												<a href="" class="profile__design__link">
													<span class="profile__design">
														<button class="profile__design__remove">
															<i class="fa fa-pencil"></i>
														</button>
														<button class="profile__design__trash">
															<i class="fa fa-trash"></i>
														</button>
														<img src="images/profile/profile3.png" class="profile__design__picture">
														<span class="profile__design__price">
															Free
														</span>
													</span>
												</a>				
											</div>
											<div class="col-sm-6 col-md-4">
												<a href="" class="profile__design__link">
													<span class="profile__design">
														<button class="profile__design__remove">
															<i class="fa fa-pencil"></i>
														</button>
														<button class="profile__design__trash">
															<i class="fa fa-trash"></i>
														</button>
														<img src="images/profile/profile2.png" class="profile__design__picture">
														<span class="profile__design__price">
															$5.00
														</span>
													</span>
												</a>				
											</div>
											<div class="col-sm-6 col-md-4">
												<a href="" class="profile__design__link">
													<span class="profile__design">
														<button class="profile__design__remove">
															<i class="fa fa-pencil"></i>
														</button>
														<button class="profile__design__trash">
															<i class="fa fa-trash"></i>
														</button>
														<img src="images/profile/profile3.png" class="profile__design__picture">
														<span class="profile__design__price">
															Free
														</span>
													</span>
												</a>				
											</div>
											<div class="col-sm-6 col-md-4">
												<a href="" class="profile__design__link">
													<span class="profile__design">
														<button class="profile__design__remove">
															<i class="fa fa-pencil"></i>
														</button>
														<button class="profile__design__trash">
															<i class="fa fa-trash"></i>
														</button>
														<img src="images/profile/profile2.png" class="profile__design__picture">
														<span class="profile__design__price">
															$5.00
														</span>
													</span>
												</a>				
											</div>
											<div class="col-sm-6 col-md-4">
												<a href="" class="profile__design__link">
													<span class="profile__design">
														<button class="profile__design__remove">
															<i class="fa fa-pencil"></i>
														</button>
														<button class="profile__design__trash">
															<i class="fa fa-trash"></i>
														</button>
														<img src="images/profile/profile3.png" class="profile__design__picture">
														<span class="profile__design__price">
															Free
														</span>
													</span>
												</a>				
											</div>
											<div class="col-sm-6 col-md-4">
												<a href="" class="profile__design__link">
													<span class="profile__design">
														<button class="profile__design__remove">
															<i class="fa fa-pencil"></i>
														</button>
														<button class="profile__design__trash">
															<i class="fa fa-trash"></i>
														</button>
														<img src="images/profile/profile2.png" class="profile__design__picture">
														<span class="profile__design__price">
															$5.00
														</span>
													</span>
												</a>				
											</div>
											<div class="col-sm-6 col-md-4">
												<a href="" class="profile__design__link">
													<span class="profile__design">
														<button class="profile__design__remove">
															<i class="fa fa-pencil"></i>
														</button>
														<button class="profile__design__trash">
															<i class="fa fa-trash"></i>
														</button>
														<img src="images/profile/profile3.png" class="profile__design__picture">
														<span class="profile__design__price">
															Free
														</span>
													</span>
												</a>				
											</div>
											<div class="col-sm-6 col-md-4">
												<a href="" class="profile__design__link">
													<span class="profile__design">
														<button class="profile__design__remove">
															<i class="fa fa-pencil"></i>
														</button>
														<button class="profile__design__trash">
															<i class="fa fa-trash"></i>
														</button>
														<img src="images/profile/profile2.png" class="profile__design__picture">
														<span class="profile__design__price">
															$5.00
														</span>
													</span>
												</a>				
											</div>
											<div class="col-sm-6 col-md-4">
												<a href="" class="profile__design__link">
													<span class="profile__design">
														<button class="profile__design__remove">
															<i class="fa fa-pencil"></i>
														</button>
														<button class="profile__design__trash">
															<i class="fa fa-trash"></i>
														</button>
														<img src="images/profile/profile3.png" class="profile__design__picture">
														<span class="profile__design__price">
															Free
														</span>
													</span>
												</a>				
											</div>
										</div>
									</div>
								</div>
							</div>	
						</div>
					</div>
				</div>	
				<div class="block__or">
					<span class="block__or__text">or</span>
				</div>
				<button class="btn btn-conf fileinput-button">
					<i class="fa fa-files-o"></i>
					<span>Upload your own</span>
					<input id="upload-image" type="file" name="files[]" multiple="">
				</button>
			</div>

			<div class="configurator__settings__content">
				<h3 class="configurator__settings__title">
					Product
				</h3>
				<p class="configurator__settings__lighten__text">Select style</p>
				<div class="configurator__create__settings">
					<div class="configurator__create__settings__item clearfix">
						<div class="configurator__create__settings__item__content">
							<div class="shirt__preview">
								<img src="images/icons/long-shirt.png"  alt="">
							</div>
							<div class="configurator__create__settings__item__inner">
								<h3 class="configurator__create__settings__item__title">
									Teespring Women’s
								</h3>
								<a href="" class="link__page-14">
									Change
								</a>
							</div>
						</div>
					</div>
					<div class="configurator__create__settings__colors colors-box clearfix">
						<div class="colors-box__item active">
							<button class="colors-box__item__btn" style="background-color: #9C7A7B;"></button>
						</div>
						<div class="colors-box__item">
							<button class="colors-box__item__btn" style="background-color: #3B3A3A;"></button>
						</div>
						<div class="colors-box__item">
							<button class="colors-box__item__btn" style="background-color: #C6CCD2;"></button>
						</div>
						<div class="colors-box__item">
							<button class="colors-box__item__btn" style="background-color: #F2F2F2;"></button>
						</div>
						<div class="colors-box__item">
							<button class="colors-box__item__btn" style="background-color: #C8D6E4;"></button>
						</div>
						<div class="colors-box__item">
							<button class="colors-box__item__btn" style="background-color: #9DBBD9;"></button>
						</div>
						<div class="colors-box__item">
							<button class="colors-box__item__btn" style="background-color: #62A9DF;"></button>
						</div>
						<div class="colors-box__item">
							<button class="colors-box__item__btn" style="background-color: #4C8FC4;"></button>
						</div>
						<div class="colors-box__item">
							<button class="colors-box__item__btn" style="background-color: #94C345;"></button>
						</div>
						<div class="colors-box__item">
							<button class="colors-box__item__btn" style="background-color: #B6DC74;"></button>
						</div>
						<div class="colors-box__item">
							<button class="colors-box__item__btn" style="background-color: #E8B839;"></button>
						</div>
						<div class="colors-box__item">
							<button class="colors-box__item__btn" style="background-color: #EEC556;"></button>
						</div>
						<div class="colors-box__item">
							<button class="colors-box__item__btn" style="background-color: #F1D484;"></button>
						</div>
						<div class="colors-box__item">
							<button class="colors-box__item__btn" style="background-color: #DB4444;"></button>
						</div>
					</div>
				</div>
				<p class="configurator__settings__lighten__text">Size & Qty</p>
				<div class="row">
					<div class="col-sm-5">
						<div class="form-group">
							<select class="chosen-select form-control">
								<option value="">All sizes</option>
								<option value="">S</option>
								<option value="">M</option>
								<option value="">L</option>
							</select>
						</div>					
					</div>
					<div class="col-sm-7">
						<div class="form-group clearfix">
							<div class="quantity configurate__quantity pull-sm-right">
					            <div class="quantity__block input-group">
					                <button class="quantity__decrement input-group-btn">-</button>
					                <input class="quantity__control form-control" type="text" value="8">    
					                <button class="quantity__increment input-group-btn">+</button>
					            </div>
					        </div>
					    </div>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12">
						<a href="" class="link__page-14">
							+ Add more
						</a>
					</div>
				</div>
				<hr class="hr"/>
				<div class="row">
					<div class="col-xs-12">
						<p class="cost m-b0">Total: €19.99</p>
						<p class="configurator__settings__lighten__text">
							include 19% VAT + plus delivery
						</p>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12">
						<button class="btn__accept" data-forward="">
							Add to Cart
						</button>
					</div>
				</div>
			</div>
		</div>

		<div class="configurator__content">
			<div class="canvas-container">
				<img class="configurator__content__picture" width="530" height="530" src="http://rocker-store.com/upload/content/27/2/28/99/19892_478_1.jpg" alt="">
				<div class="configurator__content__media" id="media">
					<div id="text">
						<img src="images/about/text.png" alt="">
					</div>
					<div id="editor" class="configurator__content__media__editor">
						<select class="selectpicker editor__item editor__item__text">
	                        <option data-content="Comic Sans" title="Comic Sans"></option>
	                        <option data-content="Ubuntu" title="Ubuntu"></option>
	                    </select>
	                    <select class="selectpicker editor__item editor__item__block">
	                        <option data-content="12px" title="12px"></option>
	                        <option data-content="14px" title="14px"></option>
	                        <option data-content="16px" title="16px"></option>
	                        <option data-content="18px" title="18px"></option>
	                    </select>
	                    <select class="selectpicker editor__item editor__item__block">
	                      <option data-content="<i class='fa fa-align-center'></i>" title="<i class='fa fa-align-center'></i>"></option>
	                      <option data-content="<i class='fa fa-align-justify'></i>" title="<i class='fa fa-align-justify'></i>"></option>
	                      <option data-content="<i class='fa fa-align-left'></i>" title="<i class='fa fa-align-left'></i>"></option>
	                      <option data-content="<i class='fa fa-align-right'></i>" title="<i class='fa fa-align-right'></i>"></option>
	                    </select>
	                   <select class="selectpicker editor__item editor__item__block edit__color">
	                        <option data-content="<div class='color__block'><div class='color__mark color__red'></div></div>" title="<div class='color__block'><div class='color__mark color__red'></div></div>"></option>
	                        <option data-content="<div class='color__block'><div class='color__mark color__pink'></div></div>" title="<div class='color__block'><div class='color__mark color__pink'></div></div>"></option>
	                        <option data-content="<div class='color__block'><div class='color__mark color__green'></div></div>" title="<div class='color__block'><div class='color__mark color__green'></div></div>"></option>
	                        <option data-content="<div class='color__block'><div class='color__mark color__red'></div></div>" title="<div class='color__block'><div class='color__mark color__red'></div></div>"></option>
	                        <option data-content="<div class='color__block'><div class='color__mark color__pink'></div></div>" title="<div class='color__block'><div class='color__mark color__pink'></div></div>"></option>
	                        <option data-content="<div class='color__block'><div class='color__mark color__green'></div></div>" title="<div class='color__block'><div class='color__mark color__green'></div></div>"></option>
	                        <option data-content="<div class='color__block'><div class='color__mark color__red'></div></div>" title="<div class='color__block'><div class='color__mark color__red'></div></div>"></option>
	                        <option data-content="<div class='color__block'><div class='color__mark color__pink'></div></div>" title="<div class='color__block'><div class='color__mark color__pink'></div></div>"></option>
	                        <option data-content="<div class='color__block'><div class='color__mark color__green'></div></div>" title="<div class='color__block'><div class='color__mark color__green'></div></div>"></option>
	                        <option data-content="<div class='color__block'><div class='color__mark color__red'></div></div>" title="<div class='color__block'><div class='color__mark color__red'></div></div>"></option>
	                        <option data-content="<div class='color__block'><div class='color__mark color__pink'></div></div>" title="<div class='color__block'><div class='color__mark color__pink'></div></div>"></option>
	                        <option data-content="<div class='color__block'><div class='color__mark color__green'></div></div>" title="<div class='color__block'><div class='color__mark color__green'></div></div>"></option>
	                        <option data-content="<div class='color__block'><div class='color__mark color__red'></div></div>" title="<div class='color__block'><div class='color__mark color__red'></div></div>"></option>
	                        <option data-content="<div class='color__block'><div class='color__mark color__pink'></div></div>" title="<div class='color__block'><div class='color__mark color__pink'></div></div>"></option>
	                        <option data-content="<div class='color__block'><div class='color__mark color__green'></div></div>" title="<div class='color__block'><div class='color__mark color__green'></div></div>"></option>
	                        <option data-content="<div class='color__block'><div class='color__mark color__red'></div></div>" title="<div class='color__block'><div class='color__mark color__red'></div></div>"></option>
	                        <option data-content="<div class='color__block'><div class='color__mark color__pink'></div></div>" title="<div class='color__block'><div class='color__mark color__pink'></div></div>"></option>
	                        <option data-content="<div class='color__block'><div class='color__mark color__green'></div></div>" title="<div class='color__block'><div class='color__mark color__green'></div></div>"></option>
	                        <option data-content="<div class='color__block'><div class='color__mark' style='background-color:black'></div></div>" title="<div class='color__block'><div class='color__mark' style='background-color:black'></div></div>"></option>
						</select>
						<button class="btn btn-editor">
							<i class="icon-double"></i>
						</button>
						<button class="btn btn-editor">
							<i class="icon-center"></i>
						</button>
						<button class="btn btn-editor">
							<i class="icon-front"></i>
						</button>
						<button class="btn btn-editor">
							<i class="icon-horizontal"></i>
						</button>
						<button class="btn btn-editor">
							<i class="icon-vertical"></i>
						</button>
						<button class="btn btn-editor">
							<i class="fa fa-refresh"></i>
						</button>
						<button class="btn btn-editor">
							<i class="fa fa-trash"></i>
						</button>
					</div>					
				</div>
			</div>			
			<ul class="list-inline m-t40">
				<li>
					<a href="" class="btn__primary">
						<i class="zmdi zmdi-rotate-right"></i>
						<span>Back</span>
					</a>
				</li>
			</ul>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12">
			<div class="configurator__tab-container">
				<ul class="product__tabs clearfix">
					<li class="product__tabs__item active">
						<a class="product__tabs__item__link" href="#description" data-toggle="tab">Configurator Help</a>
					</li>
					<li class="product__tabs__item">
						<a class="product__tabs__item__link" href="#review" data-toggle="tab">Product Sizes & Fitting</a>
					</li>
					<li class="product__tabs__item">
						<a class="product__tabs__item__link" href="#keyword" data-toggle="tab">Our Printing Types</a>
					</li>
				</ul>
				<div class="tab-content">
					<div class="tab-pane product__tab__content active" id="description">
						<p class="product__text">Configurator details EN 1</p>
					</div>
					<div class="tab-pane product__tab__content" id="review">
						<p class="product__text">Configurator details EN 2</p>
					</div>
					<div class="tab-pane product__tab__content" id="keyword">
						<p class="product__text">Configurator details EN 3</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="block__upload is-hidden">
	<div class="row">
		<div class="col-sm-6">
			<div class="configurator__body">
				<h3 class="configurator__body__title">
					Sales goal
				</h3>
				<div class="row">
					<div class="col-xs-12">
						<div id="slider" class="configurator__body__slider m-tb20">
							<div class="configurator__body__slider__content" data-slider></div>		
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-4 col-sm-6">
						<div class="input-group slider__value">
							<span class="input-group-addon"># of shirts</span>
							<input id="slider-count" type="text" class="form-control text-center">	
						</div>
					</div>
					<div class="col-lg-8 col-sm-6">
						<p class="title_text">
							Your goal is the number of shirts you’re aiming to sell, but we’ll print your campaign as long as you sell enough to generate a profit
						</p>
					</div>
				</div>
				<h3 class="configurator__body__title">
					Estimated profit
				</h3>
				<h1 class="pink_text text-bold">$801+</h1>
				<h3 class="configurator__body__title">
					Style & design
				</h3>
				<div class="configurator__body__product clearfix" data-conf-color>
					<div class="clearfix">
						<div class="configurator__body__product__picture">
							<img src="images/products/shirt1.png" alt="">
						</div>
						<div class="configurator__body__product__content">
							<h6 class="configurator__body__product__content__title">
								Teespring Premium Tee
							</h6>
							<p class="configurator__body__product__content__text">
								$13.36 profit/sale
							</p>						
							<p class="configurator__body__product__price">
								22.00
							</p>
						</div>	
					</div>
					<div class="configurator__choose__color clearfix " data-popover>
						<div class="configurator__color" data-set-color>
							<div class="color__block" style="background-color:pink;"></div>
							<div class="remove__color is-hidden" data-remove-color></div>
						</div>
						<div class="configurator__color color__empty" data-set-color>
							<div class="color__block"></div>
							<div class="remove__color is-hidden" data-remove-color></div>
						</div>
						<div class="configurator__color color__empty" data-set-color>
							<div class="color__block"></div>
							<div class="remove__color is-hidden" data-remove-color></div>
						</div>
						<div class="configurator__color color__empty" data-set-color>
							<div class="color__block"></div>
							<div class="remove__color is-hidden" data-remove-color></div>
						</div>
						<div class="configurator__color color__empty" data-set-color>
							<div class="color__block"></div>
							<div class="remove__color is-hidden" data-remove-color></div>
						</div>	
						<div class="configurator__color" data-add-color>
							<div class="add__color">
								<div class="color__block"></div>
							</div>
							<div class="popover popover__top" data-toggle-popover data-close-popover>
                                <div class="popover__block pull-left" data-choose-color>
                                    <div class="popover__block__color" style="background-color:red;"></div>
                                </div>
                                <div class="popover__block pull-left active" data-choose-color>
                                    <div class="popover__block__color" style="background-color:pink;"></div>
                                </div>
                                <div class="popover__block pull-left" data-choose-color>
                                    <div class="popover__block__color" style="background-color:orange;"></div>
                                </div>
                                <div class="popover__block pull-left" data-choose-color>
                                    <div class="popover__block__color" style="background-color:green;"></div>
                                </div>
                                <div class="popover__block pull-left" data-choose-color>
                                    <div class="popover__block__color" style="background-color:purple;"></div>
                                </div>
                                <div class="popover__block pull-left" data-choose-color>
                                    <div class="popover__block__color" style="background-color:blue;"></div>
                                </div>
                                <div class="popover__block pull-left" data-choose-color>
                                    <div class="popover__block__color" style="background-color:yellow;"></div>
                                </div>                
                                <div class="popover__block pull-left" data-choose-color>
                                    <div class="popover__block__color" style="background-color:red;"></div>
                                </div>
                                <div class="popover__block pull-left" data-choose-color>
                                    <div class="popover__block__color" style="background-color:pink;"></div>
                                </div>
                                <div class="popover__block pull-left" data-choose-color>
                                    <div class="popover__block__color" style="background-color:orange;"></div>
                                </div>
                                <div class="popover__block pull-left" data-choose-color>
                                    <div class="popover__block__color" style="background-color:green;"></div>
                                </div>
                                <div class="popover__block pull-left" data-choose-color>
                                    <div class="popover__block__color" style="background-color:red;"></div>
                                </div>
                                <div class="popover__block pull-left" data-choose-color>
                                    <div class="popover__block__color" style="background-color:red;"></div>
                                </div>
                                <div class="popover__block pull-left" data-choose-color>
                                    <div class="popover__block__color" style="background-color:pink;"></div>
                                </div>
                                <div class="popover__block pull-left" data-choose-color>
                                    <div class="popover__block__color" style="background-color:orange;"></div>
                                </div>                
                                <div class="popover__block pull-left" data-choose-color>
                                    <div class="popover__block__color" style="background-color:red;"></div>
                                </div>
                                <div class="popover__block pull-left" data-choose-color>
                                    <div class="popover__block__color" style="background-color:pink;"></div>
                                </div>
                                <div class="popover__block pull-left" data-choose-color>
                                    <div class="popover__block__color" style="background-color:orange;"></div>
	                            </div>
	                            <div class="popover__block pull-left" data-choose-color>
	                                <div class="popover__block__color" style="background-color:green;"></div>
	                            </div>
	                            <div class="popover__block pull-left" data-choose-color>
	                                <div class="popover__block__color" style="background-color:red;"></div>
	                            </div>                
	                        </div>
						</div>
					</div>
				</div>
				<div class="configurator__body__product clearfix" data-conf-color>
					<div class="clearfix">
						<div class="configurator__body__product__picture">
							<img src="images/products/shirt1.png" alt="">
						</div>
						<div class="configurator__body__product__content">
							<h6 class="configurator__body__product__content__title">
								Teespring Premium Tee
							</h6>
							<p class="configurator__body__product__content__text">
								$13.36 profit/sale
							</p>						
							<p class="configurator__body__product__price">
								22.00
							</p>
						</div>	
					</div>
					<div class="configurator__choose__color clearfix " data-popover>
						<div class="configurator__color" data-set-color>
							<div class="color__block" style="background-color:pink;"></div>
							<div class="remove__color is-hidden" data-remove-color></div>
						</div>
						<div class="configurator__color color__empty" data-set-color>
							<div class="color__block"></div>
							<div class="remove__color is-hidden" data-remove-color></div>
						</div>
						<div class="configurator__color color__empty" data-set-color>
							<div class="color__block"></div>
							<div class="remove__color is-hidden" data-remove-color></div>
						</div>
						<div class="configurator__color color__empty" data-set-color>
							<div class="color__block"></div>
							<div class="remove__color is-hidden" data-remove-color></div>
						</div>
						<div class="configurator__color color__empty" data-set-color>
							<div class="color__block"></div>
							<div class="remove__color is-hidden" data-remove-color></div>
						</div>	
						<div class="configurator__color" data-add-color>
							<div class="add__color">
								<div class="color__block"></div>
							</div>
							<div class="popover popover__top" data-toggle-popover data-close-popover>
                                <div class="popover__block pull-left" data-choose-color>
                                    <div class="popover__block__color" style="background-color:red;"></div>
                                </div>
                                <div class="popover__block pull-left active" data-choose-color>
                                    <div class="popover__block__color" style="background-color:pink;"></div>
                                </div>
                                <div class="popover__block pull-left" data-choose-color>
                                    <div class="popover__block__color" style="background-color:orange;"></div>
                                </div>
                                <div class="popover__block pull-left" data-choose-color>
                                    <div class="popover__block__color" style="background-color:green;"></div>
                                </div>
                                <div class="popover__block pull-left" data-choose-color>
                                    <div class="popover__block__color" style="background-color:purple;"></div>
                                </div>
                                <div class="popover__block pull-left" data-choose-color>
                                    <div class="popover__block__color" style="background-color:blue;"></div>
                                </div>
                                <div class="popover__block pull-left" data-choose-color>
                                    <div class="popover__block__color" style="background-color:yellow;"></div>
                                </div>                
                                <div class="popover__block pull-left" data-choose-color>
                                    <div class="popover__block__color" style="background-color:red;"></div>
                                </div>
                                <div class="popover__block pull-left" data-choose-color>
                                    <div class="popover__block__color" style="background-color:pink;"></div>
                                </div>
                                <div class="popover__block pull-left" data-choose-color>
                                    <div class="popover__block__color" style="background-color:orange;"></div>
                                </div>
                                <div class="popover__block pull-left" data-choose-color>
                                    <div class="popover__block__color" style="background-color:green;"></div>
                                </div>
                                <div class="popover__block pull-left" data-choose-color>
                                    <div class="popover__block__color" style="background-color:red;"></div>
                                </div>
                                <div class="popover__block pull-left" data-choose-color>
                                    <div class="popover__block__color" style="background-color:red;"></div>
                                </div>
                                <div class="popover__block pull-left" data-choose-color>
                                    <div class="popover__block__color" style="background-color:pink;"></div>
                                </div>
                                <div class="popover__block pull-left" data-choose-color>
                                    <div class="popover__block__color" style="background-color:orange;"></div>
                                </div>                
                                <div class="popover__block pull-left" data-choose-color>
                                    <div class="popover__block__color" style="background-color:red;"></div>
                                </div>
                                <div class="popover__block pull-left" data-choose-color>
                                    <div class="popover__block__color" style="background-color:pink;"></div>
                                </div>
                                <div class="popover__block pull-left" data-choose-color>
	                                <div class="popover__block__color" style="background-color:orange;"></div>
	                            </div>
	                            <div class="popover__block pull-left" data-choose-color>
	                                <div class="popover__block__color" style="background-color:green;"></div>
	                            </div>
	                            <div class="popover__block pull-left" data-choose-color>
	                                <div class="popover__block__color" style="background-color:red;"></div>
	                            </div>
	                        </div>
                        </div>
					</div>
				</div>
				<div class="configurator__body__product clearfix" data-conf-color>
					<div class="clearfix">
						<div class="configurator__body__product__picture">
							<img src="images/products/shirt1.png" alt="">
						</div>
						<div class="configurator__body__product__content">
							<h6 class="configurator__body__product__content__title">
								Teespring Premium Tee
							</h6>
							<p class="configurator__body__product__content__text">
								$13.36 profit/sale
							</p>						
							<p class="configurator__body__product__price">
								22.00
							</p>
						</div>	
					</div>
					<div class="configurator__choose__color clearfix " data-popover>
						<div class="configurator__color" data-set-color>
							<div class="color__block" style="background-color:pink;"></div>
							<div class="remove__color is-hidden" data-remove-color></div>
						</div>
						<div class="configurator__color color__empty" data-set-color>
							<div class="color__block"></div>
							<div class="remove__color is-hidden" data-remove-color></div>
						</div>
						<div class="configurator__color color__empty" data-set-color>
							<div class="color__block"></div>
							<div class="remove__color is-hidden" data-remove-color></div>
						</div>
						<div class="configurator__color color__empty" data-set-color>
							<div class="color__block"></div>
							<div class="remove__color is-hidden" data-remove-color></div>
						</div>
						<div class="configurator__color color__empty" data-set-color>
							<div class="color__block"></div>
							<div class="remove__color is-hidden" data-remove-color></div>
						</div>	
						<div class="configurator__color" data-add-color>
							<div class="add__color">
								<div class="color__block"></div>
							</div>
							<div class="popover popover__top" data-toggle-popover data-close-popover>
                                <div class="popover__block pull-left" data-choose-color>
                                    <div class="popover__block__color" style="background-color:red;"></div>
                                </div>
                                <div class="popover__block pull-left active" data-choose-color>
                                    <div class="popover__block__color" style="background-color:pink;"></div>
                                </div>
                                <div class="popover__block pull-left" data-choose-color>
                                    <div class="popover__block__color" style="background-color:orange;"></div>
                                </div>
                                <div class="popover__block pull-left" data-choose-color>
                                    <div class="popover__block__color" style="background-color:green;"></div>
                                </div>
                                <div class="popover__block pull-left" data-choose-color>
                                    <div class="popover__block__color" style="background-color:purple;"></div>
                                </div>
                                <div class="popover__block pull-left" data-choose-color>
                                    <div class="popover__block__color" style="background-color:blue;"></div>
                                </div>
                                <div class="popover__block pull-left" data-choose-color>
                                    <div class="popover__block__color" style="background-color:yellow;"></div>
                                </div>                
                                <div class="popover__block pull-left" data-choose-color>
                                    <div class="popover__block__color" style="background-color:red;"></div>
                                </div>
                                <div class="popover__block pull-left" data-choose-color>
                                    <div class="popover__block__color" style="background-color:pink;"></div>
                                </div>
                                <div class="popover__block pull-left" data-choose-color>
                                    <div class="popover__block__color" style="background-color:orange;"></div>
                                </div>
                                <div class="popover__block pull-left" data-choose-color>
                                    <div class="popover__block__color" style="background-color:green;"></div>
                                </div>
                                <div class="popover__block pull-left" data-choose-color>
                                    <div class="popover__block__color" style="background-color:red;"></div>
                                </div>
                                <div class="popover__block pull-left" data-choose-color>
                                    <div class="popover__block__color" style="background-color:red;"></div>
                                </div>
                                <div class="popover__block pull-left" data-choose-color>
                                    <div class="popover__block__color" style="background-color:pink;"></div>
                                </div>
                                <div class="popover__block pull-left" data-choose-color>
                                    <div class="popover__block__color" style="background-color:orange;"></div>
                                </div>                
                                <div class="popover__block pull-left" data-choose-color>
                                    <div class="popover__block__color" style="background-color:red;"></div>
                                </div>
                                <div class="popover__block pull-left" data-choose-color>
                                    <div class="popover__block__color" style="background-color:pink;"></div>
                                </div>
                                <div class="popover__block pull-left" data-choose-color>
	                                <div class="popover__block__color" style="background-color:orange;"></div>
	                            </div>
	                            <div class="popover__block pull-left" data-choose-color>
	                                <div class="popover__block__color" style="background-color:green;"></div>
	                            </div>
	                            <div class="popover__block pull-left" data-choose-color>
	                                <div class="popover__block__color" style="background-color:red;"></div>
	                            </div>
	                        </div>
                        </div>
					</div>
				</div>
				<div class="configurator__body__product clearfix" data-conf-color>
					<div class="clearfix">
						<div class="configurator__body__product__picture">
							<img src="images/products/shirt1.png" alt="">
						</div>
						<div class="configurator__body__product__content">
							<h6 class="configurator__body__product__content__title">
								Teespring Premium Tee
							</h6>
							<p class="configurator__body__product__content__text">
								$13.36 profit/sale
							</p>						
							<p class="configurator__body__product__price">
								22.00
							</p>
						</div>	
					</div>
					<div class="configurator__choose__color clearfix " data-popover>
						<div class="configurator__color" data-set-color>
							<div class="color__block" style="background-color:pink;"></div>
							<div class="remove__color is-hidden" data-remove-color></div>
						</div>
						<div class="configurator__color color__empty" data-set-color>
							<div class="color__block"></div>
							<div class="remove__color is-hidden" data-remove-color></div>
						</div>
						<div class="configurator__color color__empty" data-set-color>
							<div class="color__block"></div>
							<div class="remove__color is-hidden" data-remove-color></div>
						</div>
						<div class="configurator__color color__empty" data-set-color>
							<div class="color__block"></div>
							<div class="remove__color is-hidden" data-remove-color></div>
						</div>
						<div class="configurator__color color__empty" data-set-color>
							<div class="color__block"></div>
							<div class="remove__color is-hidden" data-remove-color></div>
						</div>	
						<div class="configurator__color" data-add-color>
							<div class="add__color">
								<div class="color__block"></div>
							</div>
							<div class="popover popover__top" data-toggle-popover data-close-popover>
                                <div class="popover__block pull-left" data-choose-color>
                                    <div class="popover__block__color" style="background-color:red;"></div>
                                </div>
                                <div class="popover__block pull-left active" data-choose-color>
                                    <div class="popover__block__color" style="background-color:pink;"></div>
                                </div>
                                <div class="popover__block pull-left" data-choose-color>
                                    <div class="popover__block__color" style="background-color:orange;"></div>
                                </div>
                                <div class="popover__block pull-left" data-choose-color>
                                    <div class="popover__block__color" style="background-color:green;"></div>
                                </div>
                                <div class="popover__block pull-left" data-choose-color>
                                    <div class="popover__block__color" style="background-color:purple;"></div>
                                </div>
                                <div class="popover__block pull-left" data-choose-color>
                                    <div class="popover__block__color" style="background-color:blue;"></div>
                                </div>
                                <div class="popover__block pull-left" data-choose-color>
                                    <div class="popover__block__color" style="background-color:yellow;"></div>
                                </div>                
                                <div class="popover__block pull-left" data-choose-color>
                                    <div class="popover__block__color" style="background-color:red;"></div>
                                </div>
                                <div class="popover__block pull-left" data-choose-color>
                                    <div class="popover__block__color" style="background-color:pink;"></div>
                                </div>
                                <div class="popover__block pull-left" data-choose-color>
                                    <div class="popover__block__color" style="background-color:orange;"></div>
                                </div>
                                <div class="popover__block pull-left" data-choose-color>
                                    <div class="popover__block__color" style="background-color:green;"></div>
                                </div>
                                <div class="popover__block pull-left" data-choose-color>
                                    <div class="popover__block__color" style="background-color:red;"></div>
                                </div>
                                <div class="popover__block pull-left" data-choose-color>
                                    <div class="popover__block__color" style="background-color:red;"></div>
                                </div>
                                <div class="popover__block pull-left" data-choose-color>
                                    <div class="popover__block__color" style="background-color:pink;"></div>
                                </div>
                                <div class="popover__block pull-left" data-choose-color>
                                    <div class="popover__block__color" style="background-color:orange;"></div>
                                </div>                
                                <div class="popover__block pull-left" data-choose-color>
                                    <div class="popover__block__color" style="background-color:red;"></div>
                                </div>
                                <div class="popover__block pull-left" data-choose-color>
                                    <div class="popover__block__color" style="background-color:pink;"></div>
                                </div>
                                <div class="popover__block pull-left" data-choose-color>
	                                <div class="popover__block__color" style="background-color:orange;"></div>
	                            </div>
	                            <div class="popover__block pull-left" data-choose-color>
	                                <div class="popover__block__color" style="background-color:green;"></div>
	                            </div>
	                            <div class="popover__block pull-left" data-choose-color>
	                                <div class="popover__block__color" style="background-color:red;"></div>
	                            </div>
	                        </div>
                        </div>
					</div>
				</div>
				<p class="text-muted segoe__family m-t20 p-b10 b-Bottom">
					You can add 10 more product options.
				</p>
				<div class="row is-hide add-style">
					<div class="col-sm-3">
						<div class="form-group">
							<select type="text" class="form-control">
								<option value="">V-Neck Tees</option>
								<option value="">Tees</option>
							</select>
						</div>
					</div>
					<div class="col-sm-6">
						<div class="form-group">
							<select type="text" class="form-control">
								<option value="">Canvas Ringspun V-Neck</option>
								<option value="">V-Neck</option>
							</select>
						</div>
					</div>
					<div class="col-sm-3">
						<button class="btn__default btn-small w-100">
							Add
						</button>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12">
						<a href="" class="link__page-16" data-toggle-block=".add-style">
							<i class="icon-circle-plus m-r5"></i> Add style
						</a>
					</div>
				</div>
				<p class="title_text segoe__family m-t20">
					Optimize your campaign by adding an additional style
				</p>
			</div>
			<div class="row">
				<div class="col-xs-12">
					<button class="btn__primary m-r20" data-back="">
						Back
					</button>
					<button class="btn__default" data-forward="">
						Continue
					</button>
				</div>
			</div>			
		</div>
		<div class="col-sm-6 text-center p-t20">
			<img src="images/configurator/shirt.png" alt="">
			<button class="btn__primary">
				Front
			</button>
			<button class="btn__primary">
				Back
			</button>
		</div>
	</div>	
</div>

<div class="block__upload is-hidden">
	<div class="row">
		<div class="col-sm-6">
			<div class="configurator__body">
				<h3 class="configurator__body__title">
					Details
				</h3>
				<p class="form-label">
					Campaign title
				</p>
				<div class="form-group">
					<input type="text" class="form-control">
				</div>
				<p class="form-label">
					Description
				</p>
				<div class="form-group">
					<textarea id="description" class="form-control" rows="10"></textarea>
				</div>
				<div class="row">
					<div class="col-xs-12">
						<a href="" class="link__page-16" >
							<i class="icon-circle-plus m-r5"></i> Tag your company
						</a>
					</div>
				</div>
				<p class="form-label m-t20">
					Duration
				</p>
				<div class="form-group">
					<select type="text" class="form-control">
						<option value="">7 Days (Ending Wednesday, Augest 5, 2015)</option>
						<option value="">Ending Wednesday</option>
					</select>
					<p class="title_text p-t5">
						<small>
							US orders will arrive 10-14 days after the end of the campaign. 
							<a href="" class="link__page-12" >
								Learn more
							</a>
						</small>
					</p>
				</div>				
				<p class="form-label">
					URL
				</p>
				<div class="input-group">
			      	<span class="input-group-addon">justshirt.com/</span>
			      	<input type="text" class="form-control" placeholder="Username">
			    </div>
			    <h3 class="configurator__body__title">
					Shipping options
				</h3>
				<label class="cb-checkbox w-100">
					<input type="checkbox">
					Allow buyers to pick up their orders from you
				</label>
				<p class="title_text">
					<small>
						Please note pick-up is currently only available to US Addresses.
					</small>
				</p>
				<p class="form-label m-t20">
					Where/when will buyers be able to pick up their shirts?
				</p>
				<div class="form-group">
					<textarea rows="5" class="form-control"></textarea>
					<p class="title_text p-t5">
						<small>
							Buyers will see these instructions when choosing between shipping/pick-up
						</small>
					</p>
				</div>
				<div class="row">
					<div class="col-sm-6">
						<p class="form-label">
							First name
						</p>
						<div class="form-group">
							<input type="text" class="form-control">
						</div>
					</div>
					<div class="col-sm-6">
						<p class="form-label">
							Last name
						</p>
						<div class="form-group">
							<input type="text" class="form-control">
						</div>
					</div>
				</div>	
				<div class="row">
					<div class="col-sm-8">
						<p class="form-label">
							Contry
						</p>
						<div class="form-group">
							<select type="text" class="form-control">
								<option value="">USA</option>
								<option value="">Belarus</option>
							</select>
						</div>
					</div>
					<div class="col-sm-4">
						<p class="form-label">
							Zipcode
						</p>
						<div class="form-group">
							<input type="text" class="form-control">
						</div>					
					</div>
				</div>
				<div class="row">
					<div class="col-sm-6">
						<p class="form-label">
							State
						</p>
						<div class="form-group">
							<select type="text" class="form-control">
								<option value="">USA</option>
								<option value="">Belarus</option>
							</select>
						</div>
					</div>
					<div class="col-sm-6">
						<p class="form-label">
							City
						</p>
						<div class="form-group">
							<input type="text" class="form-control">
						</div>					
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12">
						<p class="form-label">
							Address
						</p>
						<div class="form-group">
							<input type="text" class="form-control">
						</div>
					</div>
				</div>
				 <h3 class="configurator__body__title">
					Display Options
				</h3>
				<label class="cb-checkbox w-100">
					<input type="checkbox">
					Show the back side by default
				</label>
			</div>
			<div class="row">
				<div class="col-xs-12">
					<button class="btn__primary m-r20" data-back="">
						Back
					</button>
					<button class="btn__default">
						Launch
					</button>
				</div>
			</div>
		</div>
		<div class="col-sm-6 text-center p-t20">
			<img src="images/configurator/shirt.png" alt="">
			<button class="btn__primary">
				Front
			</button>
			<button class="btn__primary">
				Back
			</button>
		</div>
	</div>
</div>

<div id="modal-design" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-body">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
				<div class="row">
					<div class="col-sm-6">
						<div class="artwork">
							<img src="http://www.tattoobite.com/wp-content/uploads/2013/04/butterfly-tattoo-designs-picture.jpg" alt="" class="artwork__img" data-artwork>
						</div>
					</div>
					<div class="col-sm-6">					
						<h3 class="artwork__title">
							Butterlies Tripple
						</h3>
						<h4 class="artwork__subtitle">
							Designed by EDDArts
						</h4>
						<div class="clearfix">
							<a href="" class="artwork__gallery active" data-gallery>
								<img class="artwork__gallery__picture" src="http://www.tattoobite.com/wp-content/uploads/2013/04/butterfly-tattoo-designs-picture.jpg">
							</a>
							<a href="" class="artwork__gallery" data-gallery>
								<img class="artwork__gallery__picture" src="http://beautycarehub.com/wp-content/uploads/2015/07/Butterfly-Tattoo-Design-1.jpg" >
							</a>
							<a href="" class="artwork__gallery" data-gallery>
								<img class="artwork__gallery__picture" src="http://www.tattooshunt.com/images/33/beautiful-siimple-butterfly-design.jpg" >
							</a>
							<a href="" class="artwork__gallery" data-gallery>
								<img class="artwork__gallery__picture" src="http://www.tattoobite.com/wp-content/uploads/2013/04/butterfly-tattoo-designs-picture.jpg" >
							</a>
							<a href="" class="artwork__gallery" data-gallery>
								<img class="artwork__gallery__picture" src="http://beautycarehub.com/wp-content/uploads/2015/07/Butterfly-Tattoo-Design-1.jpg" >
							</a>
							<a href="" class="artwork__gallery" data-gallery>
								<img class="artwork__gallery__picture" src="http://www.tattooshunt.com/images/33/beautiful-siimple-butterfly-design.jpg" >
							</a>
							<a href="" class="artwork__gallery" data-gallery>
								<img class="artwork__gallery__picture" src="http://www.tattoobite.com/wp-content/uploads/2013/04/butterfly-tattoo-designs-picture.jpg" >
							</a>
							<a href="" class="artwork__gallery" data-gallery>
								<img class="artwork__gallery__picture" src="http://beautycarehub.com/wp-content/uploads/2015/07/Butterfly-Tattoo-Design-1.jpg" >
							</a>
							<a href="" class="artwork__gallery" data-gallery>
								<img class="artwork__gallery__picture" src="http://www.tattooshunt.com/images/33/beautiful-siimple-butterfly-design.jpg" >
							</a>
							<a href="" class="artwork__gallery" data-gallery>
								<img class="artwork__gallery__picture" src="http://www.tattoobite.com/wp-content/uploads/2013/04/butterfly-tattoo-designs-picture.jpg" >
							</a>						
						</div>
						<div class="row m-t10">
							<div class="col-sm-6">
								<p class="cost__text p-t20"><strong>From $ 7.50</strong></p>
							</div>
							<div class="col-sm-6">
								<button class="btn__accept" data-forward="">
									Add Design
								</button>
							</div>
						</div>
					</div>
				</div>				
			</div>
		</div>
	</div>
</div>

<style>
	.configurator__content__media {
	    position: absolute;
	    width: 200px;
	    height: 300px;
	    border: 2px solid #dfdede;
	    top: 0;
	    left: 0;
	    right: 0;
	    bottom: 0;
	    margin: auto;
	}
	.configurator__content__media__editor {
	    background-color: #000;
	    padding: 8px;
	    position: absolute;
	    top: 30px;
	   	left: -176px;
	    z-index: 5;
	    width: 550px;
	    height: 41px;
	    -moz-border-radius: 4px;
	    -webkit-border-radius: 4px;
	    border-radius: 4px;
	}

	.configurator__content__media:after {
	   height: 20px; 
	   background-color: #dfdede;
	   content: 'Printable area';
	   font-size: 12px;
	   color: #fff; 
	   font-family: "Lato",sans-serif;
	   bottom: 0; 
	   left: 0;
	   right: 0; 
	   position: absolute;
	   text-align: left; 
	   padding-left: 10px;
	}

</style>

<script src="js/popover.js"></script>