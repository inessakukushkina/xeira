<ul class="breadcrumbs">
	<li class="breadcrumbs__item">
		<a href="" class="breadcrumbs__item__link">
			Shop
		</a>
	</li>
	<li class="breadcrumbs__item">
		<a href="" class="breadcrumbs__item__link">
			T-Shirts
		</a>
	</li>
	<li class="breadcrumbs__item">
		<a href="" class="breadcrumbs__item__link">
			Women's
		</a>
	</li>
	<li class="breadcrumbs__item">
		<a href="" class="breadcrumbs__item__link">
			Sample Fashion Product
		</a>
	</li>
</ul>
<div class="row">
	<div class="col-md-9">
		<div class="row product__block">
			<div class="col-md-5">
				<div class="product__gallery">
					<div class="carous">
						<div class="carous-slides clearfix">
							<div class="item product__picture">
								<a href="" class="product__picture__link">
									<i class="fa fa-pencil"></i> Edit Design
								</a>
								<img class="product__picture__image" src="images/products/shirt1.png" data-zoom-image="" alt="" />
							</div>
						</div>
						<div class="carous__block">
							<div class="carous-controls">
								<button type="button" class="carous-btn carous-prev"><i class="fa fa-angle-left"></i></button>
								<button type="button" class="carous-btn carous-next"><i class="fa fa-angle-right "></i></button>
							</div>
							<div class="carous-thumbnails">
								<div class="thumbs-container">
									<ul class="carous__list">
										<li class="carous__list__item">
											<img class="carous__list__item__picture" src="images/products/shirt1.png" alt="" />
										</li>
										<li class="carous__list__item">
											<img class="carous__list__item__picture" src="images/products/shirt2.png" alt="" />
										</li>
										<li class="carous__list__item">
											<img class="carous__list__item__picture" src="images/products/shirt3.png" alt="" />
										</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-7">
				<h1 class="product__title">
					Sample Fashion Product
				</h1>
				<div class="rating">
					<div class="pull-left">
						<input type="radio" class="rating__control" value="1" id="rating__control1" name="rating">
					    <label for="rating__control1" class="rating__star"></label>

					    <input type="radio" class="rating__control" value="2" id="rating__control2" name="rating">
					    <label for="rating__control2" class="rating__star"></label>

					    <input type="radio" class="rating__control" value="3" id="rating__control3" name="rating">
					    <label for="rating__control3" class="rating__star"></label>

					    <input type="radio" class="rating__control" value="4" id="rating__control4" name="rating">
					    <label for="rating__control4" class="rating__star"></label>
					    
					    <input type="radio" class="rating__control" value="5" id="rating__control5" name="rating">
					    <label for="rating__control5" class="rating__star"></label>
					</div>					
				    <span class="rating__text">
				    	Be the first to review this product
				    </span>
				</div>
				<div class="product__author clearfix">
					<img src="images/avatar/product_author.png" class="product__author__avatar">
					<p class="product__author__text">
						Brunette Best Friend
					</p>
					<p class="product__author__text">
						by <a href="" class="product__author__link">Jorge Chambers</a>
					</p>
				</div>
				<div class="clearfix">
					<p class="product__price pull-left">
						$ 50.00
					</p>
					<div class="pull-sm-right text-right">
						<p class="product__availability">
							Availability: In Stock
						</p>
						<p class="product__required">
							* Required Fields
						</p>
					</div>
				</div>
				<p class="product__subtitle">
					<span class="product__required">*</span> <strong>Color:</strong> gray
				</p>
				<div class="clearfix">
					<div class="color__block active">
						<div class="color__block__item">
							<div class="color__mark color__pink"></div>
						</div>
					</div>
					<div class="color__block">
						<div class="color__block__item">
							<div class="color__mark color__red"></div>
						</div>
					</div>
					<div class="color__block">
						<div class="color__block__item">
							<div class="color__mark color__green"></div>
						</div>
					</div>
					<div class="color__block">
						<div class="color__block__item">
							<div class="color__mark" style="background-color:white"></div>
						</div>
					</div>
				</div>
				<p class="product__subtitle clearfix">
					<span class="product__required">*</span> <strong>Size:</strong> S					
				</p>
				<div class="row">
					<div class="col-xs-12">
						<div class="size__block active">
							<div class="size__block__item">S</div>
						</div>
						<div class="size__block">
							<div class="size__block__item">L</div>
						</div>
						<div class="size__block">
							<div class="size__block__item">M</div>
						</div>
						<div class="size__block">
							<div class="size__block__item">XL</div>
						</div>
					</div>
				</div>
				<h5 class="product__shipping__title">
					<strong>Shipping Info</strong>
				</h5>
				<p class="product__shipping__text">
					Orders are printed and shipped when the time expires (February 24th). You can expect your package to arrive around 5 business days after the campaign finishes printing.
				</p>
				<p class="product__shipping__text">
					Orders are mailed via first-class or priority mail.
					Shipping costs €3.99 for the first apparel item and €2.00 for each additional apparel item.
				</p>				
				<div class="row">
					<div class="col-xs-12">
						<div class="quantity clearfix">
							<p class="quantity__text">Qty:</p> 
							<div class="quantity__block input-group">
								<button class="quantity__decrement input-group-btn" disabled="disabled">-</button>
								<input class="quantity__control form-control" type="text" value="1">	
								<button class="quantity__increment input-group-btn">+</button>
							</div>
						</div>
						<a href="" class="product__add_to_cart add-to-cart">
							Add to Cart
						</a>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12">
						<div class="product__share">
							<h5 class="product__shipping__title">
								<strong>3108 sold, available until <span class="product__available__date">Feb 24!</span></strong>
							</h5>
							<p class="product__shipping__text">
								We reached our goal! You can keep buying until the campaign ends!
							</p>
							<div class="row">
								<div class="col-xs-6 col-sm-3">
									<div class="countown">
										<p class="countown__header">
											07
										</p>
										<p class="countown__text">
											Days
										</p>
									</div>
								</div>
								<div class="col-xs-6 col-sm-3">
									<div class="countown">
										<p class="countown__header">
											15
										</p>
										<p class="countown__text">
											Hours
										</p>
									</div>
								</div>
								<div class="col-xs-6 col-sm-3">
									<div class="countown">
										<p class="countown__header">
											15
										</p>
										<p class="countown__text">
											Minutes
										</p>
									</div>
								</div>
								<div class="col-xs-6 col-sm-3">
									<div class="countown">
										<p class="countown__header">
											30
										</p>
										<p class="countown__text">
											Seconds
										</p>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-lg-6">
									<p class="product__share__text social__text">
										Share: 
									</p>
									<ul class="share__list">
										<li class="share__list__item">
											<a class="share__list__item__link" href=""><i class="icon-facebook-gray"></i></a>
										</li>
										<li class="share__list__item">
											<a class="share__list__item__link" href=""><i class="icon-twitter-gray"></i></a>
										</li>
										<li class="share__list__item">
											<a class="share__list__item__link" href=""><i class="icon-google-gray"></i></a>	
										</li>
									</ul>	
								</div>
								<div class="col-lg-6">
									<p class="product__share__text lg-right">
										<a href="" class="product__share__link">
											<i class="fa fa-heart"></i> Add to wishlist
										</a>							
									</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<ul class="product__tabs">
					<li class="product__tabs__item active">
						<a class="product__tabs__item__link" href="#description" data-toggle="tab">Item Description</a>
					</li>
					<li class="product__tabs__item">
						<a class="product__tabs__item__link" href="#add_inform" data-toggle="tab">Additional Information</a>
					</li>
					<li class="product__tabs__item">
						<a class="product__tabs__item__link" href="#review" data-toggle="tab">Reviews</a>
					</li>
					<li class="product__tabs__item">
						<a class="product__tabs__item__link" href="#keyword" data-toggle="tab">Keywords</a>
					</li>
					<li class="product__tabs__item">
						<a class="product__tabs__item__link" href="#ship_inform" data-toggle="tab">Shipping Information</a>
					</li>
					<li class="product__tabs__item">
						<a class="product__tabs__item__link" href="#size_chart" data-toggle="tab">Size Chart</a>
					</li>
				</ul>
				<div class="tab-content">
					<div class="tab-pane product__tab__content active" id="description">
						<p class="product__text">
							Configurable Products let your customers select the variant they desire by choosing options. For example, you sell t-shirts in two colors and three sizes. You’d create the six variants as individual products (with their own skus) and then add these six to a configurable product from where customers can choose size and color, then add to cart. If desired you could also have customers search for “red medium t-shirt” and land on the specific page for this variant.
						</p>
						<p class="product__text">
							Complex products in Magento are a way to consolidate product variants onto a single product info page in the front-end. The variants themselves are actually simple products and have their own SKUs and stock management. This is very powerful - it allows you to let customers search for the individual variants, but browse only to the consolidated product pages. Here are some selected options of Magento catalog management:
						</p>
						<div class="row">
							<div class="col-sm-6">
								<ul class="product__list">
									<li class="product__list__item">Create Store-specific attributes on the fly</li>
									<li class="product__list__item">Simple, Configurable (e.g. size, color, etc.), Bundled and Grouped Products</li>
									<li class="product__list__item">Downloadable/Digital Products, Virtual Products</li>
									<li class="product__list__item">Customer Personalized Products - upload text for embroidery, monogramming, etc.</li>
									<li class="product__list__item">Inventory Management with Backordered items</li>
									<li class="product__list__item">Tax Rates per location, customer group and product type</li>
									<li class="product__list__item">Advanced Pricing Rules and support for Special Prices</li>
								</ul>
							</div>
							<div class="col-sm-6">
								<ul class="product__list">
									<li class="product__list__item">Attribute Sets for quick product creation of different item types</li>
									<li class="product__list__item">Approve, Edit and Delete Product Reviews and Tags</li>
									<li class="product__list__item">Media Manager with automatic image resizing and watermarking</li>
									<li class="product__list__item">Search Results rewrites and redirects</li>
									<li class="product__list__item">Batch Import and Export of catalog</li>
									<li class="product__list__item">Batch Updates to products in admin panel</li>
									<li class="product__list__item">RSS feed for Low Inventory Alerts </li>
								</ul>
							</div>
						</div>						
					</div>
					<div class="tab-pane product__tab__content" id="add_inform">
						<table class="product__table">
							<tbody>
								<tr>
									<td>
										SKU
									</td>
									<td>
										HEU4CXLPCT-V-GR4104-KF
									</td>
								</tr>
								<tr>
									<td>
										EAN
									</td>
									<td>
										No
									</td>
								</tr>
								<tr>
									<td>
										Country of Manufacture
									</td>
									<td>
										Germany
									</td>
								</tr>
								<tr>
									<td>
										Delivery Time
									</td>
									<td>
										2-3 Tage
									</td>
								</tr>
							</tbody>
						</table>
					</div>
					<div class="tab-pane product__tab__content" id="review">
						<h2 class="product__content__title">
							5 Comments
						</h2>
						<div class="product__review clearfix">
							<div class="product__review__settings">
								<p class="product__review__name">
									Neil Kennedy
								</p>
								<i class="icon-star-gold"></i>	
								<i class="icon-star-gold"></i>
								<i class="icon-star-gold"></i>
								<i class="icon-star-gold"></i>
								<i class="icon-star-gray"></i>							
							</div>
							<div class="product__review__content">
								<p class="product__review__text">
									Lorem ipsum dolor sit amet, 
									consectetur adipisicing elit. 
									Laudantium aliquam ipsa cupiditate, 
									nihil dicta placeat minus, veritatis, 
									ipsam mollitia nisi, ducimus laborum 
									libero expedita error vero eveniet 
									veniam commodi consequatur.
								</p>
							</div>
							<div class="product__review__settings">
								<p class="product__review__name">
									June 27, 2015
								</p>
							</div>
						</div>
						<div class="product__review clearfix">
							<div class="product__review__settings">
								<p class="product__review__name">
									Neil Kennedy
								</p>
								<i class="icon-star-gold"></i>	
								<i class="icon-star-gold"></i>
								<i class="icon-star-gold"></i>
								<i class="icon-star-gold"></i>
								<i class="icon-star-gray"></i>							
							</div>
							<div class="product__review__content">
								<p class="product__review__text">
									Lorem ipsum dolor sit amet, 
									consectetur adipisicing elit. 
									Laudantium aliquam ipsa cupiditate, 
									nihil dicta placeat minus, veritatis, 
									ipsam mollitia nisi, ducimus laborum 
									libero expedita error vero eveniet 
									veniam commodi consequatur.
								</p>
							</div>
							<div class="product__review__settings">
								<p class="product__review__name">
									June 27, 2015
								</p>
							</div>
						</div>
						<div class="product__review clearfix">
							<div class="product__review__settings">
								<p class="product__review__name">
									Neil Kennedy
								</p>
								<i class="icon-star-gold"></i>	
								<i class="icon-star-gold"></i>
								<i class="icon-star-gold"></i>
								<i class="icon-star-gold"></i>
								<i class="icon-star-gray"></i>							
							</div>
							<div class="product__review__content">
								<p class="product__review__text">
									Lorem ipsum dolor sit amet, 
									consectetur adipisicing elit. 
									Laudantium aliquam ipsa cupiditate, 
									nihil dicta placeat minus, veritatis, 
									ipsam mollitia nisi, ducimus laborum 
									libero expedita error vero eveniet 
									veniam commodi consequatur.
								</p>
							</div>
							<div class="product__review__settings">
								<p class="product__review__name">
									June 27, 2015
								</p>
							</div>
						</div>
						<div class="product__review clearfix">
							<div class="product__review__settings">
								<p class="product__review__name">
									Neil Kennedy
								</p>
								<i class="icon-star-gold"></i>	
								<i class="icon-star-gold"></i>
								<i class="icon-star-gold"></i>
								<i class="icon-star-gold"></i>
								<i class="icon-star-gray"></i>
							</div>
							<div class="product__review__content">
								<p class="product__review__text">
									Lorem ipsum dolor sit amet, 
									consectetur adipisicing elit. 
									Laudantium aliquam ipsa cupiditate, 
									nihil dicta placeat minus, veritatis, 
									ipsam mollitia nisi, ducimus laborum 
									libero expedita error vero eveniet 
									veniam commodi consequatur.
								</p>
							</div>
							<div class="product__review__settings">
								<p class="product__review__name">
									June 27, 2015
								</p>
							</div>
						</div>
						<h2 class="product__content__title">
							Leave your comment
						</h2>
						<div class="row">
							<div class="col-sm-9">
								<div class="rating__box">
									<div class="rating">
									 	<span class="rating__label form-label m-r10">
									    	Quality rate
									    </span>
									    <div class="rating__content">
									    	<div class="pull-left">
										    	<input type="radio" class="rating__control" value="6" id="rating__control6" name="rating">
											    <label for="rating__control6" class="rating__star"></label>

											    <input type="radio" class="rating__control" value="7" id="rating__control7" name="rating">
											    <label for="rating__control7" class="rating__star"></label>

											    <input type="radio" class="rating__control" value="8" id="rating__control8" name="rating">
											    <label for="rating__control8" class="rating__star"></label>

											    <input type="radio" class="rating__control" value="9" id="rating__control9" name="rating">
											    <label for="rating__control9" class="rating__star"></label>
											    
											    <input type="radio" class="rating__control" value="10" id="rating__control10" name="rating">
											    <label for="rating__control10" class="rating__star"></label>
											</div>
									    </div>
									</div>
									<div class="rating">
									 	<span class="rating__label form-label m-r10">
									    	Price rate
									    </span>
									    <div class="rating__content">
										    <div class="pull-left">
										    	<input type="radio" class="rating__control" value="6" id="rating__control16" name="rating1">
											    <label for="rating__control16" class="rating__star"></label>

											    <input type="radio" class="rating__control" value="7" id="rating__control17" name="rating1">
											    <label for="rating__control17" class="rating__star"></label>

											    <input type="radio" class="rating__control" value="8" id="rating__control18" name="rating1">
											    <label for="rating__control18" class="rating__star"></label>

											    <input type="radio" class="rating__control" value="9" id="rating__control19" name="rating1">
											    <label for="rating__control19" class="rating__star"></label>
											    
											    <input type="radio" class="rating__control" value="10" id="rating__control20" name="rating1">
											    <label for="rating__control20" class="rating__star"></label>
										    </div>
									    </div>
									</div>
									<div class="rating">
									 	<span class="rating__label form-label m-r10">
									    	Design rate
									    </span>
									    <div class="rating__content">
									    	<div class="pull-left">
										    	<input type="radio" class="rating__control" value="6" id="rating__control26" name="rating2">
											    <label for="rating__control26" class="rating__star"></label>

											    <input type="radio" class="rating__control" value="7" id="rating__control27" name="rating2">
											    <label for="rating__control27" class="rating__star"></label>

											    <input type="radio" class="rating__control" value="8" id="rating__control28" name="rating2">
											    <label for="rating__control28" class="rating__star"></label>

											    <input type="radio" class="rating__control" value="9" id="rating__control29" name="rating2">
											    <label for="rating__control29" class="rating__star"></label>
											    
											    <input type="radio" class="rating__control" value="10" id="rating__control30" name="rating2">
											    <label for="rating__control30" class="rating__star"></label>
											</div>
									    </div>
									</div>
								</div>
								<p class="form-label">
									Comment
								</p>
								<div class="form-group">
									<textarea rows="10" class="form-control"></textarea>
								</div>
								<button class="btn__accept m-t10">
									Submit
								</button>	
							</div>
						</div>						
					</div>
					<div class="tab-pane product__tab__content" id="keyword">
						<h2 class="product__content__title">
							Product tags
						</h2>
						<ul class="product__tags">
							<li class="product__tags__item">
								<a href="" class="product__tags__item__link">Shirt</a>
							</li>
							<li class="product__tags__item">
								<a href="" class="product__tags__item__link">Pink</a>
							</li>
							<li class="product__tags__item">
								<a href="" class="product__tags__item__link">Sport</a>
							</li>
						</ul>
					</div>
					<div class="tab-pane product__tab__content" id="ship_inform">
						<p class="product__text">
							Lorem ipsum dolor sit amet, consectetur adipisicing elit. 
							Enim asperiores ut quo aperiam optio repellat numquam ab quibusdam voluptas dolore, 
							id adipisci? A eligendi, unde maxime velit quod aliquid est.
						</p>
						<p class="product__text">
							Lorem ipsum dolor sit amet, consectetur adipisicing elit. 
							Enim asperiores ut quo aperiam optio repellat numquam ab quibusdam voluptas dolore, 
							id adipisci? A eligendi, unde maxime velit quod aliquid est.
						</p>
						<p class="product__text">
							Lorem ipsum dolor sit amet, consectetur adipisicing elit. 
							Enim asperiores ut quo aperiam optio repellat numquam ab quibusdam voluptas dolore, 
							id adipisci? A eligendi, unde maxime velit quod aliquid est.
						</p>
						<p class="product__text">
							Lorem ipsum dolor sit amet, consectetur adipisicing elit. 
							Enim asperiores ut quo aperiam optio repellat numquam ab quibusdam voluptas dolore, 
							id adipisci? A eligendi, unde maxime velit quod aliquid est.
						</p>
					</div>
					<div class="tab-pane product__tab__content" id="size_chart">
						<table class="product__size responsive__table">
							<thead>
								<tr>
									<th>Italian Size</th>
									<th>French Size</th>
									<th>Spanish Size</th>
									<th>German Size</th>
									<th>UK Size</th>
									<th>US Size</th>
									<th>Japanese Size</th>
									<th>Chinese size</th>
								</tr>
							</thead>
							<tbody>								
								<tr>
									<td data-th="Italian Size">34</td>
									<td data-th="French Size">30</td>
									<td data-th="Spanish Size">30</td>
									<td data-th="German Size">30</td>
									<td data-th="UK Size">4</td>
									<td data-th="US Size">00</td>
									<td data-th="Japanese Size">34</td>
									<td data-th="Chinese Size">155/76A</td>
								</tr>
								<tr>
									<td data-th="Italian Size">36</td>
									<td data-th="French Size">32</td>
									<td data-th="Spanish Size">32</td>
									<td data-th="German Size">32</td>
									<td data-th="UK Size">6</td>
									<td data-th="US Size">0</td>
									<td data-th="Japanese Size">36</td>
									<td data-th="Chinese Size">155/80A</td>
								</tr>
								<tr>
									<td data-th="Italian Size">38</td>
									<td data-th="French Size">34</td>
									<td data-th="Spanish Size">34</td>
									<td data-th="German Size">34</td>
									<td data-th="UK Size">8</td>
									<td data-th="US Size">2</td>
									<td data-th="Japanese Size">38</td>
									<td data-th="Chinese Size">160/84A</td>
								</tr>
								<tr>
									<td data-th="Italian Size">40</td>
									<td data-th="French Size">36</td>
									<td data-th="Spanish Size">36</td>
									<td data-th="German Size">36</td>
									<td data-th="UK Size">10</td>
									<td data-th="US Size">4</td>
									<td data-th="Japanese Size">40</td>
									<td data-th="Chinese Size">160/88A</td>
								</tr>
								<tr>
									<td data-th="Italian Size">42</td>
									<td data-th="French Size">38</td>
									<td data-th="Spanish Size">38</td>
									<td data-th="German Size">38</td>
									<td data-th="UK Size">12</td>
									<td data-th="US Size">6</td>
									<td data-th="Japanese Size">42</td>
									<td data-th="Chinese Size">165/92A</td>
								</tr>
								<tr>
									<td data-th="Italian Size">44</td>
									<td data-th="French Size">40</td>
									<td data-th="Spanish Size">40</td>
									<td data-th="German Size">40</td>
									<td data-th="UK Size">14</td>
									<td data-th="US Size">8</td>
									<td data-th="Japanese Size">44</td>
									<td data-th="Chinese Size">170/96A</td>
								</tr>
								<tr>
									<td data-th="Italian Size">46</td>
									<td data-th="French Size">42</td>
									<td data-th="Spanish Size">42</td>
									<td data-th="German Size">42</td>
									<td data-th="UK Size">16</td>
									<td data-th="US Size">10</td>
									<td data-th="Japanese Size">46</td>
									<td data-th="Chinese Size">170/98A</td>
								</tr>
								<tr>
									<td data-th="Italian Size">48</td>
									<td data-th="French Size">44</td>
									<td data-th="Spanish Size">44</td>
									<td data-th="German Size">44</td>
									<td data-th="UK Size">18</td>
									<td data-th="US Size">12</td>
									<td data-th="Japanese Size">48</td>
									<td data-th="Chinese Size">175/102A</td>
								</tr>
								<tr>
									<td data-th="Italian Size">50</td>
									<td data-th="French Size">46</td>
									<td data-th="Spanish Size">46</td>
									<td data-th="German Size">46</td>
									<td data-th="UK Size">20</td>
									<td data-th="US Size">14</td>
									<td data-th="Japanese Size">50</td>
									<td data-th="Chinese Size">175/106A</td>
								</tr>
							</tbody>
						</table>
					</div>
			    </div>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<h2 class="product__content__title p-r60">
					You may also be interested in the following product(s)
				</h2>
			</div>
		</div>
		<div class="row following_products">
			<div class="col-sm-4">
				<a href="" class="product__preview">
					<div class="product__preview__picture">
						<img src="images/products/shirt1.png" alt="" class="product__preview__avatar">
					</div>
					<div class="product__preview__content">
						<div class="product__preview__label label__sale">
							Sale
						</div>
						<h5 class="product__preview__title">
							Sample Fashion Product
						</h5>						
						<ul class="product__preview__color_list">
							<li class="product__preview__color_list__item">
								<div class="color__block color__black"></div>
							</li>
							<li class="product__preview__color_list__item">
								<div class="color__block color__orange"></div>
							</li>
							<li class="product__preview__color_list__item">
								<div class="color__block color__green"></div>
							</li>
						</ul>
						<div class="product__preview__price">
							<p class="product__preview__price__active">
								$ 25.00
							</p>
						</div>
					</div>
				</a>
			</div>
			<div class="col-sm-4">
				<a href="" class="product__preview">
					<div class="product__preview__picture">
						<img src="images/products/shirt2.png" alt="" class="product__preview__avatar">
					</div>					
					<div class="product__preview__content">
						<div class="product__preview__label label__new">
							New
						</div>
						<h5 class="product__preview__title">
							Sample Fashion Product
						</h5>
						<ul class="product__preview__color_list">
							<li class="product__preview__color_list__item">
								<div class="color__block color__black"></div>
							</li>
							<li class="product__preview__color_list__item">
								<div class="color__block color__orange"></div>
							</li>
							<li class="product__preview__color_list__item">
								<div class="color__block color__green"></div>
							</li>
						</ul>
						<div class="product__preview__price">
							<p class="product__preview__price__active">
								$ 15.00
								<span class="product__preview__price__old">$ 25.00</span>
							</p>
						</div>
					</div>
				</a>
			</div>
			<div class="col-sm-4">
				<a href="" class="product__preview">
					<div class="product__preview__picture">
						<img src="images/products/shirt3.png" alt="" class="product__preview__avatar">
					</div>
					<div class="product__preview__content">
						<h5 class="product__preview__title">
							Sample Fashion Product
						</h5>
						<ul class="product__preview__color_list">
							<li class="product__preview__color_list__item">
								<div class="color__block color__black"></div>
							</li>
							<li class="product__preview__color_list__item">
								<div class="color__block color__orange"></div>
							</li>
							<li class="product__preview__color_list__item">
								<div class="color__block color__green"></div>
							</li>
						</ul>
						<div class="product__preview__price">
							<p class="product__preview__price__active">
								$ 30.00
							</p>
						</div>
					</div>					
				</a>
			</div>
			<div class="col-sm-4">
				<a href="" class="product__preview">
					<div class="product__preview__picture">
						<img src="images/products/shirt1.png" alt="" class="product__preview__avatar">
					</div>
					<div class="product__preview__content">
						<div class="product__preview__label label__sale">
							Sale
						</div>
						<h5 class="product__preview__title">
							Sample Fashion Product
						</h5>						
						<ul class="product__preview__color_list">
							<li class="product__preview__color_list__item">
								<div class="color__block color__black"></div>
							</li>
							<li class="product__preview__color_list__item">
								<div class="color__block color__orange"></div>
							</li>
							<li class="product__preview__color_list__item">
								<div class="color__block color__green"></div>
							</li>
						</ul>
						<div class="product__preview__price">
							<p class="product__preview__price__active">
								$ 25.00
							</p>
						</div>
					</div>
				</a>
			</div>
			<div class="col-sm-4">
				<a href="" class="product__preview">
					<div class="product__preview__picture">
						<img src="images/products/shirt2.png" alt="" class="product__preview__avatar">
					</div>					
					<div class="product__preview__content">
						<div class="product__preview__label label__new">
							New
						</div>
						<h5 class="product__preview__title">
							Sample Fashion Product
						</h5>
						<ul class="product__preview__color_list">
							<li class="product__preview__color_list__item">
								<div class="color__block color__black"></div>
							</li>
							<li class="product__preview__color_list__item">
								<div class="color__block color__orange"></div>
							</li>
							<li class="product__preview__color_list__item">
								<div class="color__block color__green"></div>
							</li>
						</ul>
						<div class="product__preview__price">
							<p class="product__preview__price__active">
								$ 15.00
								<span class="product__preview__price__old">$ 25.00</span>
							</p>
						</div>
					</div>
				</a>
			</div>
			<div class="col-sm-4">
				<a href="" class="product__preview">
					<div class="product__preview__picture">
						<img src="images/products/shirt3.png" alt="" class="product__preview__avatar">
					</div>
					<div class="product__preview__content">
						<h5 class="product__preview__title">
							Sample Fashion Product
						</h5>
						<ul class="product__preview__color_list">
							<li class="product__preview__color_list__item">
								<div class="color__block color__black"></div>
							</li>
							<li class="product__preview__color_list__item">
								<div class="color__block color__orange"></div>
							</li>
							<li class="product__preview__color_list__item">
								<div class="color__block color__green"></div>
							</li>
						</ul>
						<div class="product__preview__price">
							<p class="product__preview__price__active">
								$ 30.00
							</p>
						</div>
					</div>					
				</a>
			</div>
		</div>
	</div>	
	<div class="col-md-3">
		<div class="product__condition clearfix">
			<i class="icon-dark-plane"></i>
			<p class="product__condition__text">
				We will send this product in 2 days
			</p>
		</div>
		<div class="product__condition clearfix">
			<i class="icon-dark-phone"></i>
			<p class="product__condition__text">
				Call us now for more info about our products
			</p>
		</div>
		<div class="product__condition clearfix">
			<i class="icon-dark-reload"></i>
			<p class="product__condition__text">
				Return purchased items and get all your money back
			</p>
		</div>
		<div class="product__condition clearfix">
			<i class="icon-dark-star"></i>
			<p class="product__condition__text">
				Buy this product and earn 10 special loyalty points!
			</p>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<h2 class="product__content__title p-r60">
					Related
				</h2>
			</div>
		</div>
		<div class="row relating_products">
			<div class="col-sm-4">
				<a href="" class="product__preview">
					<div class="product__preview__picture">
						<img src="images/products/shirt1.png" alt="" class="product__preview__avatar">
					</div>
					<div class="product__preview__content">
						<div class="product__preview__label label__sale">
							Sale
						</div>
						<h5 class="product__preview__title">
							Sample Fashion Product
						</h5>						
						<ul class="product__preview__color_list">
							<li class="product__preview__color_list__item">
								<div class="color__block color__black"></div>
							</li>
							<li class="product__preview__color_list__item">
								<div class="color__block color__orange"></div>
							</li>
							<li class="product__preview__color_list__item">
								<div class="color__block color__green"></div>
							</li>
						</ul>
						<div class="product__preview__price">
							<p class="product__preview__price__active">
								$ 25.00
							</p>
						</div>
					</div>
				</a>
			</div>
			<div class="col-sm-4">
				<a href="" class="product__preview">
					<div class="product__preview__picture">
						<img src="images/products/shirt2.png" alt="" class="product__preview__avatar">
					</div>					
					<div class="product__preview__content">
						<div class="product__preview__label label__new">
							New
						</div>
						<h5 class="product__preview__title">
							Sample Fashion Product
						</h5>
						<ul class="product__preview__color_list">
							<li class="product__preview__color_list__item">
								<div class="color__block color__black"></div>
							</li>
							<li class="product__preview__color_list__item">
								<div class="color__block color__orange"></div>
							</li>
							<li class="product__preview__color_list__item">
								<div class="color__block color__green"></div>
							</li>
						</ul>
						<div class="product__preview__price">
							<p class="product__preview__price__active">
								$ 15.00
								<span class="product__preview__price__old">$ 25.00</span>
							</p>
						</div>
					</div>
				</a>
			</div>
			<div class="col-sm-4">
				<a href="" class="product__preview">
					<div class="product__preview__picture">
						<img src="images/products/shirt3.png" alt="" class="product__preview__avatar">
					</div>
					<div class="product__preview__content">
						<h5 class="product__preview__title">
							Sample Fashion Product
						</h5>
						<ul class="product__preview__color_list">
							<li class="product__preview__color_list__item">
								<div class="color__block color__black"></div>
							</li>
							<li class="product__preview__color_list__item">
								<div class="color__block color__orange"></div>
							</li>
							<li class="product__preview__color_list__item">
								<div class="color__block color__green"></div>
							</li>
						</ul>
						<div class="product__preview__price">
							<p class="product__preview__price__active">
								$ 30.00
							</p>
						</div>
					</div>					
				</a>
			</div>
			<div class="col-sm-4">
				<a href="" class="product__preview">
					<div class="product__preview__picture">
						<img src="images/products/shirt1.png" alt="" class="product__preview__avatar">
					</div>
					<div class="product__preview__content">
						<div class="product__preview__label label__sale">
							Sale
						</div>
						<h5 class="product__preview__title">
							Sample Fashion Product
						</h5>						
						<ul class="product__preview__color_list">
							<li class="product__preview__color_list__item">
								<div class="color__block color__black"></div>
							</li>
							<li class="product__preview__color_list__item">
								<div class="color__block color__orange"></div>
							</li>
							<li class="product__preview__color_list__item">
								<div class="color__block color__green"></div>
							</li>
						</ul>
						<div class="product__preview__price">
							<p class="product__preview__price__active">
								$ 25.00
							</p>
						</div>
					</div>
				</a>
			</div>
			<div class="col-sm-4">
				<a href="" class="product__preview">
					<div class="product__preview__picture">
						<img src="images/products/shirt2.png" alt="" class="product__preview__avatar">
					</div>					
					<div class="product__preview__content">
						<div class="product__preview__label label__new">
							New
						</div>
						<h5 class="product__preview__title">
							Sample Fashion Product
						</h5>
						<ul class="product__preview__color_list">
							<li class="product__preview__color_list__item">
								<div class="color__block color__black"></div>
							</li>
							<li class="product__preview__color_list__item">
								<div class="color__block color__orange"></div>
							</li>
							<li class="product__preview__color_list__item">
								<div class="color__block color__green"></div>
							</li>
						</ul>
						<div class="product__preview__price">
							<p class="product__preview__price__active">
								$ 15.00
								<span class="product__preview__price__old">$ 25.00</span>
							</p>
						</div>
					</div>
				</a>
			</div>
			<div class="col-sm-4">
				<a href="" class="product__preview">
					<div class="product__preview__picture">
						<img src="images/products/shirt3.png" alt="" class="product__preview__avatar">
					</div>
					<div class="product__preview__content">
						<h5 class="product__preview__title">
							Sample Fashion Product
						</h5>
						<ul class="product__preview__color_list">
							<li class="product__preview__color_list__item">
								<div class="color__block color__black"></div>
							</li>
							<li class="product__preview__color_list__item">
								<div class="color__block color__orange"></div>
							</li>
							<li class="product__preview__color_list__item">
								<div class="color__block color__green"></div>
							</li>
						</ul>
						<div class="product__preview__price">
							<p class="product__preview__price__active">
								$ 30.00
							</p>
						</div>
					</div>					
				</a>
			</div>
		</div>
	</div>
</div>