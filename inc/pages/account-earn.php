<ul class="breadcrumbs">
	<li class="breadcrumbs__item">
		<a href="" class="breadcrumbs__item__link">
			My account
		</a>
	</li>
	<li class="breadcrumbs__item">
		<a href="" class="breadcrumbs__item__link">
			Invite & Earn
		</a>
	</li>
</ul>
<div class="row">
	<div class="col-sm-3">
		<div class="sidebar">			
			<h2 class="sidebar_title">
				My Account
			</h2>
			<ul class="sidebar__list">
				<li class="sidebar__list__item">
					<a href="" class="sidebar__list__item__link active">
						My Designs
					</a>
				</li>
				<li class="sidebar__list__item">
					<a href="" class="sidebar__list__item__link">
						My Purchases
					</a>
				</li>
				<li class="sidebar__list__item">
					<a href="" class="sidebar__list__item__link">
						My Sold Items
					</a>
				</li>
				<li class="sidebar__list__item">
					<a href="" class="sidebar__list__item__link">
						My Campaigns
					</a>
				</li>
				<li class="sidebar__list__item">
					<a href="" class="sidebar__list__item__link">
						Wishlist
					</a>
				</li>
				<li class="sidebar__list__item">
					<a href="" class="sidebar__list__item__link">
						Reviews
					</a>
				</li>
				<li class="sidebar__list__item">
					<a href="" class="sidebar__list__item__link">
						Invite & earn
					</a>
				</li>
				<li class="sidebar__list__item">
					<a href="" class="sidebar__list__item__link">
						Settings
					</a>
				</li>
			</ul>
		</div>
	</div>
	<div class="col-sm-9">
		<h2 class="head__title">
			Invite & Earn
		</h2>
		<div class="inform clearfix">
			<div class="inform__block">
				<h3 class="inform__block__title">
					$ 00,00
				</h3>
				<p class="inform__block__text">
					Revenue
				</p>
			</div>
			<div class="inform__block ">
				<h3 class="inform__block__title">
					20
				</h3>
				<p class="inform__block__text">
					Invites
				</p>
			</div>
			<div class="inform__block">
				<h3 class="inform__block__title">
					23
				</h3>
				<p class="inform__block__text">
					Conversions
				</p>
			</div>
			<div class="inform__block">
				<h3 class="inform__block__title">
					5
				</h3>
				<p class="inform__block__text">
					Refferals
				</p>
			</div>
		</div>
		<h3 class="head__subtitle">
			Statistics
		</h3>
		<ul class="chart__list">
			<li class="chart__list__item active">
				<a href="" class="chart__list__item__link">day</a>
			</li>
			<li class="chart__list__item">
				<a href="" class="chart__list__item__link">week</a>
			</li>
			<li class="chart__list__item">
				<a href="" class="chart__list__item__link">month</a>
			</li>
		</ul>
		<div id="chart" class="m-tb20"></div>
	</div>
</div>