<ul class="breadcrumbs">
	<li class="breadcrumbs__item">
		<a href="" class="breadcrumbs__item__link">
			My account
		</a>
	</li>
	<li class="breadcrumbs__item">
		<a href="" class="breadcrumbs__item__link">
			My purchase
		</a>
	</li>
</ul>
<div class="row">
	<div class="col-sm-3">
		<div class="sidebar">			
			<h2 class="sidebar_title">
				My Account
			</h2>
			<ul class="sidebar__list">
				<li class="sidebar__list__item">
					<a href="" class="sidebar__list__item__link active">
						My Designs
					</a>
				</li>
				<li class="sidebar__list__item">
					<a href="" class="sidebar__list__item__link">
						My Purchases
					</a>
				</li>
				<li class="sidebar__list__item">
					<a href="" class="sidebar__list__item__link">
						My Sold Items
					</a>
				</li>
				<li class="sidebar__list__item">
					<a href="" class="sidebar__list__item__link">
						My Campaigns
					</a>
				</li>
				<li class="sidebar__list__item">
					<a href="" class="sidebar__list__item__link">
						Wishlist
					</a>
				</li>
				<li class="sidebar__list__item">
					<a href="" class="sidebar__list__item__link">
						Reviews
					</a>
				</li>
				<li class="sidebar__list__item">
					<a href="" class="sidebar__list__item__link">
						Invite & earn
					</a>
				</li>
				<li class="sidebar__list__item">
					<a href="" class="sidebar__list__item__link">
						Settings
					</a>
				</li>
			</ul>
		</div>
	</div>
	<div class="col-sm-9">
		<h2 class="head__title">
			My Purchases
		</h2>
		<div class="row">
			<div class="col-md-3 col-sm-5 p-t10">
				<div class="form-group form-search">
					<input type="text" class="form-control" placeholder="Search entire store here...">
				</div>
			</div>
			<div class="col-md-1 col-sm-2 sm-right p-0 p-t10">
				<p class="text-page p-t5 m-b0">Sort By:</p>				
			</div>
			<div class="col-md-3 col-sm-5 p-t10">
				<div class="form-group">
					<select class="form-control">
						<option value="">All categories</option>
						<option value="">Pig</option>
						<option value="">Raccoon</option>
					</select>
				</div>
			</div>
			<div class="col-md-1 md-right p-t10">
				<p class="text-page p-t5 m-b0">Date:</p>				
			</div>
			<div class="col-md-2 col-sm-6 p-t10">
				<div class="form-group form-date">
					<input type="text" class="form-control" data-datepicker>
				</div>			
			</div>
			<div class="col-md-2 col-sm-6 p-t10">
				<div class="form-group form-date form-last-date">
					<input type="text" class="form-control" data-datepicker>
				</div>			
			</div>
		</div>
		<table class="order__table responsive__table m-t5 m-b40">
			<thead>
				<tr>
					<th>Order #</th>
					<th>Order date</th>
					<th>Status</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td data-th="Order #:">
						#15269
					</td>
					<td data-th="Order date:">
						06/14/2015
					</td>
					<td data-th="Status:">
						<span class="order__pending">
							<i class="m-r5 icon-pending"></i>
							Pending
						</span>
					</td>
					<td data-th="Action:">
						<a href="" class="order__link">
							View details
						</a>
					</td>
				</tr>
				<tr>
					<td data-th="Order #:">
						#15269
					</td>
					<td data-th="Order date:">
						06/14/2015
					</td>
					<td data-th="Status:">
						<span class="order__pending">
							<i class="m-r5 icon-pending"></i>
							Pending
						</span>
					</td>
					<td data-th="Action:">
						<a href="" class="order__link">
							View details
						</a>
					</td>
				</tr>
				<tr>
					<td data-th="Order #:">
						#15269
					</td>
					<td data-th="Order date:">
						06/14/2015
					</td>
					<td data-th="Status:">
						<span class="order__completed">
							<i class="m-r5 icon-complete"></i>
							Completed
						</span>
					</td>
					<td data-th="Action:">
						<a href="" class="order__link">
							View details
						</a>
					</td>
				</tr>
				<tr>
					<td data-th="Order #:">
						#15269
					</td>
					<td data-th="Order date:">
						06/14/2015
					</td>
					<td data-th="Status:">
						<span class="order__completed">
							<i class="m-r5 icon-complete"></i>
							Completed
						</span>
					</td>
					<td data-th="Action:">
						<a href="" class="order__link">
							View details
						</a>
					</td>
				</tr>
				<tr>
					<td data-th="Order #:">
						#15269
					</td>
					<td data-th="Order date:">
						06/14/2015
					</td>
					<td data-th="Status:">
						<span class="order__cancelled">
							<i class="m-r5 icon-cancelled"></i>
							Cancelled
						</span>
					</td>
					<td data-th="Action:">
						<a href="" class="order__link">
							View details
						</a>
					</td>
				</tr>
				<tr>
					<td data-th="Order #:">
						#15269
					</td>
					<td data-th="Order date:">
						06/14/2015
					</td>
					<td data-th="Status:">
						<span class="order__cancelled">
							<i class="m-r5 icon-cancelled"></i>
							Cancelled
						</span>
					</td>
					<td data-th="Action:">
						<a href="" class="order__link">
							View details
						</a>
					</td>
				</tr>
			</tbody>
		</table>
	</div>
</div>