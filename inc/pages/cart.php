<table class="cart">
	<thead>
		<tr>
			<th colspan="2">
				<label class="cb-checkbox check__all">
					<input type="checkbox">
					Check all
				</label>
			</th>
			<th>
				Product Name
			</th>
			<th>
				Unit Price
			</th>
			<th>
				QTY
			</th>
			<th>
				Total
			</th>
			<th></th>
		</tr>
	</thead>	
	<tbody>
		<tr class="clear__block">
			<td>
				<label class="cb-checkbox">
					<input  type="checkbox">					
				</label>
			</td>
			<td class="checkout__product__image">
				<div class="cart__picture">
					<img src="images/products/shirt1.png" alt="" class="cart__avatar">
				</div>
			</td>
			<td data-th="Product Name:" class="checkout__product__name">
				<h2 class="cart__title">
					Product With Custom Options
				</h2>
				<ul class="cart__list">
					<li class="cart__list__item">
						T-Shirt Size <span>L</span>
					</li>
					<li class="cart__list__item">
						T-Shirt Color <span>Pink</span>
					</li>
					<li class="cart__list__item">
						Additional Options <br>
						<span>Embroidered logo to the chest</span>
					</li>
				</ul>
			</td>
			<td data-th="Unit Price:" class="checkout__product__total">
				$22.00
			</td>
			<td data-th="Qty:" class="checkout__product__total">
				<div class="quantity">		           
		            <div class="quantity__block input-group">
		                <button class="quantity__decrement input-group-btn" disabled="disabled">-</button>
		                <input class="quantity__control form-control" type="text" value="8">    
		                <button class="quantity__increment input-group-btn">+</button>          
		            </div>
		        </div>
			</td>
			<td data-th="Total:" class="checkout__product__total">
				$44.00
			</td>
			<td data-th="Remove product:" class="checkout__product__remove">
				<a href="" class="remove__checkout">x</a>
			</td>
		</tr>
		<tr class="clear__block">
			<td>
				<label class="cb-checkbox">
					<input  type="checkbox">					
				</label>
			</td>
			<td class="checkout__product__image">
				<div class="cart__picture">
					<img src="images/products/shirt2.png" alt="" class="cart__avatar">
				</div>
			</td>
			<td data-th="Product Name:" class="checkout__product__name">
				<h2 class="cart__title">
					Product With Custom Options
				</h2>
				<ul class="cart__list">
					<li class="cart__list__item">
						T-Shirt Size <span>L</span>
					</li>
					<li class="cart__list__item">
						T-Shirt Color <span>Pink</span>
					</li>
					<li class="cart__list__item">
						Additional Options <br>
						<span>Embroidered logo to the chest</span>
					</li>
				</ul>
			</td>
			<td data-th="Unit Price:" class="checkout__product__total">
				$22.00
			</td>
			<td data-th="Qty:" class="checkout__product__total">
				<div class="quantity">		           
		            <div class="quantity__block input-group">
		                <button class="quantity__decrement input-group-btn" disabled="disabled">-</button>
		                <input class="quantity__control form-control" type="text" value="8">    
		                <button class="quantity__increment input-group-btn">+</button>          
		            </div>
		        </div>
			</td>
			<td data-th="Total:" class="checkout__product__total">
				$44.00
			</td>
			<td data-th="Remove product:" class="checkout__product__remove">
				<a href="" class="remove__checkout">x</a>
			</td>
		</tr>
		<tr class="clear__block">
			<td>
				<label class="cb-checkbox">
					<input  type="checkbox">					
				</label>
			</td>
			<td class="checkout__product__image">
				<div class="cart__picture">
					<img src="images/products/shirt3.png" alt="" class="cart__avatar">
				</div>
			</td>
			<td data-th="Product Name:" class="checkout__product__name">
				<h2 class="cart__title">
					Product With Custom Options
				</h2>
				<ul class="cart__list">
					<li class="cart__list__item">
						T-Shirt Size <span>L</span>
					</li>
					<li class="cart__list__item">
						T-Shirt Color <span>Pink</span>
					</li>
					<li class="cart__list__item">
						Additional Options <br>
						<span>Embroidered logo to the chest</span>
					</li>
				</ul>
			</td>
			<td data-th="Unit Price:" class="checkout__product__total">
				$22.00
			</td>
			<td data-th="Qty:" class="checkout__product__total">
				<div class="quantity">		           
		            <div class="quantity__block input-group">
		                <button class="quantity__decrement input-group-btn" disabled="disabled">-</button>
		                <input class="quantity__control form-control" type="text" value="8">    
		                <button class="quantity__increment input-group-btn">+</button>          
		            </div>
		        </div>
			</td>
			<td data-th="Total:" class="checkout__product__total">
				$44.00
			</td>
			<td data-th="Remove product:" class="checkout__product__remove">
				<a href="" class="remove__checkout">x</a>
			</td>
		</tr>
	</tbody>
</table>

<div class="row">
	<div class="col-xs-12">
		<div class="b-Top p-t20 text-right">
			<p class="cart__subtotal">
				Subtotal: <span>$88.00</span>
			</p>			
		</div>
	</div>
</div>

<div class="row m-tb40">
	<div class="col-sm-6">
		<button class="btn btn__primary m-t0">
			Continue Shopping
		</button>
	</div>
	<div class="col-sm-6 sm-right">
		<button class="btn btn__default">
			Proceed to Checkout <i class="icon-left"></i>
		</button>
	</div>
</div>