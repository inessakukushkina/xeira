<div class="wrapper">
	<header class="nav header">
        <div class="container">
           <a href="" class="header__logo">
				<img src="images/logo/white-justShirt.png" class="logo__picture" alt="">
			</a>
			<div class="pull-right">
				<div class="dropdown">
					<a data-toggle="dropdown" class="dropdown__link" href="#">En <i class="fa fa-caret-down"></i></a>
					<ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
						<li>
							<a href="">
								<i class="icon-deutch"></i>
								DE
							</a>
						</li>
						<li>
							<a href="">
								<i class="icon-britain"></i>
								EN
							</a>
						</li>
					</ul>
				</div>
			</div>        
        </div> 
    </header> 
    <div class="container">