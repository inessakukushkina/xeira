<div class="row m-t30 m-b100">
	<div class="col-lg-9 col-md-8">
		<div class="post">
			<div class="post__img-box big__post-img">
				<img class="post__image" src="images/blog/post-1.jpg" alt=""/>
			</div>
			<div class="post__content">
				<span class="post__content__category">Some category</span>

				<h4 class="post__content__title__main">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Saepe maxime vel vero omnis</h4>

				<span class="post__content__date">October, 2015</span>

				<p class="post__content__text">Let’s have a peek into the mists of the Spreadshirt crystal ball and see what the future holds in store in terms of designs that will do well in October. Our loyal and faithful in-house oracle reveals sound inside knowledge, and you’d be well advised to heed to the suggestions offered in this month’s design tip section. Read up on scary Halloween tips and October clues now.
				Our loyal and faithful in-house oracle reveals sound inside knowledge, and you’d be well advised to heed to the suggestions offered in this month’s design tip section. Read up on scary Halloween tips and October clues now.</p>

				<a class="btn-post post__content__btn" href="">Read more</a>
			</div>
		</div>

		<div class="row">
			<div class="col-sm-6 post-item">
				<div class="post">
					<div class="post__img-box">
						<img class="post__image" src="images/blog/post-2.jpg" alt=""/>
					</div>
					<div class="post__content">
						<span class="post__content__category">Some category</span>

						<h4 class="post__content__title">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Saepe maxime vel vero omnis</h4>

						<span class="post__content__date">October, 2015</span>

						<p class="post__content__text">Let’s have a peek into the mists of the Spreadshirt crystal ball and see what the future holds in store in terms of designs that will do well in October. Our loyal and faithful in-house oracle reveals. Let’s have a peek into the mists of the Spreadshirt crystal ball and see what</p>

						<a class="btn-post post__content__btn" href="">Read more</a>
					</div>
				</div>
			</div>

			<div class="col-sm-6 post-item">
				<div class="post">
					<div class="post__img-box">
						<img class="post__image" src="images/blog/post-3.jpg" alt=""/>
					</div>
					<div class="post__content">
						<span class="post__content__category">Some category</span>

						<h4 class="post__content__title">Sample Fashion Product</h4>

						<span class="post__content__date">October, 2015</span>

						<p class="post__content__text">Let’s have a peek into the mists of the Spreadshirt crystal ball and see what the future holds in store in terms of designs that will do well in October. Our loyal and faithful in-house oracle reveals ...</p>

						<a class="btn-post post__content__btn" href="">Read more</a>
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-sm-6 post-item">
				<div class="post">
					<div class="post__img-box">
						<img class="post__image" src="images/blog/post-4.jpg" alt=""/>
					</div>
					<div class="post__content">
						<span class="post__content__category">Some category</span>

						<h4 class="post__content__title">Sample Fashion Product</h4>

						<span class="post__content__date">October, 2015</span>

						<p class="post__content__text">Let’s have a peek into the mists of the Spreadshirt crystal ball and see what the future holds in store in terms of designs that will do well in October. Our loyal and faithful in-house oracle reveals. Let’s have a peek into the mists of the Spreadshirt crystal ball and see what the future holds</p>

						<a class="btn-post post__content__btn" href="">Read more</a>
					</div>
				</div>
			</div>

			<div class="col-sm-6 post-item">
				<div class="post">
					<div class="post__img-box">
						<img class="post__image" src="images/blog/post-5.jpg" alt=""/>
					</div>
					<div class="post__content">
						<span class="post__content__category">Some category</span>

						<h4 class="post__content__title">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Saepe maxime vel vero omnis</h4>

						<span class="post__content__date">October, 2015</span>

						<p class="post__content__text">Let’s have a peek into the mists of the Spreadshirt crystal ball and see what the future holds in store in terms of designs that will do well in October. Our loyal and faithful in-house oracle reveals ...</p>

						<a class="btn-post post__content__btn" href="">Read more</a>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-lg-3 col-md-4">
		<div class="post__banner">
			<img src="images/blog/post_banner.jpg" alt=""/>

			<div class="post__banner__text-box">
				<h4 class="post__banner__text">Video Tutorial: Applying Vector and Effects</h4>
				<a class="btn-post-reverse" href="">Read more</a>
			</div>
		</div>


		<div class="month-posts">
			<div class="month-posts__header">
				<h4 class="month-posts__title">May 2015</h4>
			</div>

			<div class="month-posts__slider">
				<div>
					
					
					<a href="" class="month-posts__post">
						<div class="month-posts__post__img-box">
							<img src="images/blog/mini-post-3.jpg" alt=""/>
						</div>
						<div class="month-posts__post__content">
							<span class="month-posts__post__title">Trending Colors – The Best-Selling Tones6</span>
							<span class="month-posts__post__date">September, 2015</span>
						</div>
					</a>
					<a href="" class="month-posts__post">
						<div class="month-posts__post__img-box">
							<img src="images/blog/mini-post-4.jpg" alt=""/>
						</div>
						<div class="month-posts__post__content">
							<span class="month-posts__post__title">Now that’s Fresh!</span>
							<span class="month-posts__post__date">September, 2015</span>
						</div>
					</a>
					<a href="" class="month-posts__post">
						<div class="month-posts__post__img-box">
							<img src="images/blog/mini-post-5.jpg" alt=""/>
						</div>
						<div class="month-posts__post__content">
							<span class="month-posts__post__title">Now that’s Fresh!</span>
							<span class="month-posts__post__date">September, 2015</span>
						</div>
					</a>

					<div class="month-posts__link-wrap">
						<a class="month-posts__link" href="">More posts for this month</a>
					</div>
				</div>
				<div>
					<a href="" class="month-posts__post">
						<div class="month-posts__post__img-box">
							<img src="images/blog/mini-post-1.jpg" alt=""/>
						</div>
						<div class="month-posts__post__content">
							<span class="month-posts__post__title">Trending Colors – The Best-Selling Tones6</span>
							<span class="month-posts__post__date">September, 2015</span>
						</div>
					</a>
					<a href="" class="month-posts__post">
						<div class="month-posts__post__img-box">
							<img src="images/blog/mini-post-2.jpg" alt=""/>
						</div>
						<div class="month-posts__post__content">
							<span class="month-posts__post__title">Now that’s Fresh!</span>
							<span class="month-posts__post__date">September, 2015</span>
						</div>
					</a>
					<a href="" class="month-posts__post">
						<div class="month-posts__post__img-box">
							<img src="images/blog/mini-post-3.jpg" alt=""/>
						</div>
						<div class="month-posts__post__content">
							<span class="month-posts__post__title">Trending Colors – The Best-Selling Tones6</span>
							<span class="month-posts__post__date">September, 2015</span>
						</div>
					</a>
					<a href="" class="month-posts__post">
						<div class="month-posts__post__img-box">
							<img src="images/blog/mini-post-4.jpg" alt=""/>
						</div>
						<div class="month-posts__post__content">
							<span class="month-posts__post__title">Now that’s Fresh!</span>
							<span class="month-posts__post__date">September, 2015</span>
						</div>
					</a>
					<a href="" class="month-posts__post">
						<div class="month-posts__post__img-box">
							<img src="images/blog/mini-post-5.jpg" alt=""/>
						</div>
						<div class="month-posts__post__content">
							<span class="month-posts__post__title">Now that’s Fresh!</span>
							<span class="month-posts__post__date">September, 2015</span>
						</div>
					</a>

					<div class="month-posts__link-wrap">
						<a class="month-posts__link" href="">More posts for this month</a>
					</div>
				</div>
				<div>
					<a href="" class="month-posts__post">
						<div class="month-posts__post__img-box">
							<img src="images/blog/mini-post-1.jpg" alt=""/>
						</div>
						<div class="month-posts__post__content">
							<span class="month-posts__post__title">Trending Colors – The Best-Selling Tones6</span>
							<span class="month-posts__post__date">September, 2015</span>
						</div>
					</a>
					<a href="" class="month-posts__post">
						<div class="month-posts__post__img-box">
							<img src="images/blog/mini-post-2.jpg" alt=""/>
						</div>
						<div class="month-posts__post__content">
							<span class="month-posts__post__title">Now that’s Fresh!</span>
							<span class="month-posts__post__date">September, 2015</span>
						</div>
					</a>
					<a href="" class="month-posts__post">
						<div class="month-posts__post__img-box">
							<img src="images/blog/mini-post-3.jpg" alt=""/>
						</div>
						<div class="month-posts__post__content">
							<span class="month-posts__post__title">Trending Colors – The Best-Selling Tones6</span>
							<span class="month-posts__post__date">September, 2015</span>
						</div>
					</a>
					<a href="" class="month-posts__post">
						<div class="month-posts__post__img-box">
							<img src="images/blog/mini-post-4.jpg" alt=""/>
						</div>
						<div class="month-posts__post__content">
							<span class="month-posts__post__title">Now that’s Fresh!</span>
							<span class="month-posts__post__date">September, 2015</span>
						</div>
					</a>
					<a href="" class="month-posts__post">
						<div class="month-posts__post__img-box">
							<img src="images/blog/mini-post-5.jpg" alt=""/>
						</div>
						<div class="month-posts__post__content">
							<span class="month-posts__post__title">Now that’s Fresh!</span>
							<span class="month-posts__post__date">September, 2015</span>
						</div>
					</a>

					<div class="month-posts__link-wrap">
						<a class="month-posts__link" href="">More posts for this month</a>
					</div>
				</div>
				<div>
					<a href="" class="month-posts__post">
						<div class="month-posts__post__img-box">
							<img src="images/blog/mini-post-1.jpg" alt=""/>
						</div>
						<div class="month-posts__post__content">
							<span class="month-posts__post__title">Trending Colors – The Best-Selling Tones6</span>
							<span class="month-posts__post__date">September, 2015</span>
						</div>
					</a>
					<a href="" class="month-posts__post">
						<div class="month-posts__post__img-box">
							<img src="images/blog/mini-post-2.jpg" alt=""/>
						</div>
						<div class="month-posts__post__content">
							<span class="month-posts__post__title">Now that’s Fresh!</span>
							<span class="month-posts__post__date">September, 2015</span>
						</div>
					</a>
					<a href="" class="month-posts__post">
						<div class="month-posts__post__img-box">
							<img src="images/blog/mini-post-3.jpg" alt=""/>
						</div>
						<div class="month-posts__post__content">
							<span class="month-posts__post__title">Trending Colors – The Best-Selling Tones6</span>
							<span class="month-posts__post__date">September, 2015</span>
						</div>
					</a>
					<a href="" class="month-posts__post">
						<div class="month-posts__post__img-box">
							<img src="images/blog/mini-post-4.jpg" alt=""/>
						</div>
						<div class="month-posts__post__content">
							<span class="month-posts__post__title">Now that’s Fresh!</span>
							<span class="month-posts__post__date">September, 2015</span>
						</div>
					</a>
					<a href="" class="month-posts__post">
						<div class="month-posts__post__img-box">
							<img src="images/blog/mini-post-5.jpg" alt=""/>
						</div>
						<div class="month-posts__post__content">
							<span class="month-posts__post__title">Now that’s Fresh!</span>
							<span class="month-posts__post__date">September, 2015</span>
						</div>
					</a>

					<div class="month-posts__link-wrap">
						<a class="month-posts__link" href="">More posts for this month</a>
					</div>
				</div>
				<div>
					<a href="" class="month-posts__post">
						<div class="month-posts__post__img-box">
							<img src="images/blog/mini-post-1.jpg" alt=""/>
						</div>
						<div class="month-posts__post__content">
							<span class="month-posts__post__title">Trending Colors – The Best-Selling Tones6</span>
							<span class="month-posts__post__date">September, 2015</span>
						</div>
					</a>
					<a href="" class="month-posts__post">
						<div class="month-posts__post__img-box">
							<img src="images/blog/mini-post-2.jpg" alt=""/>
						</div>
						<div class="month-posts__post__content">
							<span class="month-posts__post__title">Now that’s Fresh!</span>
							<span class="month-posts__post__date">September, 2015</span>
						</div>
					</a>
					<a href="" class="month-posts__post">
						<div class="month-posts__post__img-box">
							<img src="images/blog/mini-post-3.jpg" alt=""/>
						</div>
						<div class="month-posts__post__content">
							<span class="month-posts__post__title">Trending Colors – The Best-Selling Tones6</span>
							<span class="month-posts__post__date">September, 2015</span>
						</div>
					</a>
					<a href="" class="month-posts__post">
						<div class="month-posts__post__img-box">
							<img src="images/blog/mini-post-4.jpg" alt=""/>
						</div>
						<div class="month-posts__post__content">
							<span class="month-posts__post__title">Now that’s Fresh!</span>
							<span class="month-posts__post__date">September, 2015</span>
						</div>
					</a>
					<a href="" class="month-posts__post">
						<div class="month-posts__post__img-box">
							<img src="images/blog/mini-post-5.jpg" alt=""/>
						</div>
						<div class="month-posts__post__content">
							<span class="month-posts__post__title">Now that’s Fresh!</span>
							<span class="month-posts__post__date">September, 2015</span>
						</div>
					</a>

					<div class="month-posts__link-wrap">
						<a class="month-posts__link" href="">More posts for this month</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>