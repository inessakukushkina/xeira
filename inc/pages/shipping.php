<div class="row m-t30 p-b100">
    <div class="col-sm-3 col-md-2">
        <div class="sidebar">
            <h4 class="sidebar__title2">About us</h4>

            <ul class="sidebar__menu">
                <li class="sidebar__menu__item">
                    <a href="">The Company</a>
                </li>
                <li class="sidebar__menu__item">
                    <a href="">Impressum</a>
                </li>
                <li class="sidebar__menu__item">
                    <a href="">Contact</a>
                </li>
                <li class="sidebar__menu__item active">
                    <a href="">Shipping</a>
                </li>
                <li class="sidebar__menu__item">
                    <a href="">Return Policy</a>
                </li>
                <li class="sidebar__menu__item">
                    <a href="">Privacy Policy</a>
                </li>
                <li class="sidebar__menu__item">
                    <a href="">Terms and Condition</a>
                </li>
                <li class="sidebar__menu__item">
                    <a href="">Order Precess</a>
                </li>
                <li class="sidebar__menu__item">
                    <a href="">Blog</a>
                </li>
            </ul>
        </div>
    </div>
    <div class="col-sm-9 col-md-10">
        <div class="page-content page-content--m0">
            <div class="page-content__row page-content__row--bp0">
                <div class="row">
                    <div class="col-sm-6 col-lg-5">
                        <h5 class="page-title">Zahlung und Versand</h5>
                        <p>
                            Die Lieferung erfolgt innerhalb der unten Aufgeführten Länder. Alle Versandkosten inklusive der gesetzlichem Mehrwertsteuer. Versandkosten innerhalb Deutschlands 3,50 Euro  pauschal mit DPD (Versichert)
                            <strong class="page-content__block delivery-free">Ab 35€ Versandkosten Frei !!!</strong>
                        </p>

                        <h5 class="page-title">Versandkosten ins Ausland</h5>
                        <p>Österreich, Belgien, Luxemburg, Niederlande, Vereinigtes Königreich (UK) pauschal 4,10- Euro mit DHL/ Deutsche Post.</p>
                    </div>
                    <div class="col-sm-6 col-lg-offset-1 margin-top-xs-28">
                        <h5 class="page-title">Lieferfristen</h5>

                        <p>Soweit in der Artikelbeschreibung nichts angegeben ist, erfolgt die Zustellung der Ware innerhalb Deutschland von 1-2 Werktagen nach Zahlungseingang.</p>
                        <p>Bei Auslands Lieferungen innerhalb von 5-10 Tagen, an Sonn- und Feiertagen erfolgt keine Zustellung.</p>
                    </div>
                </div>
            </div>

            <div class="page-content__row">
                <div class="row">
                    <div class="col-lg-5 col-sm-6">
                        <h5 class="page-title">Zahlungsbedingungen (Lieferungen innerhalb Deuschlands)</h5>
                        <p>
                            <span class="page-content__block">Vorkasse per Überweisung Versand erfolgt nach Geldeingang auf unser Konto</span>
                            <span class="page-content__block">Zahlung per Scheck -  Versand erfolgt nach Geldeingang auf unser Konto</span>
                            <span class="page-content__block">Zahlung per Paypal</span>
                            <span class="page-content__block">Lastschrift per Paypal</span>
                            <span class="page-content__block">Mastercard per Paypal </span>
                            <span class="page-content__block">Visa per Paypal </span>
                            <span class="page-content__block">American Express per Paypal </span>
                            <span class="page-content__block">Zahlung per Nachnahme Aufpreis 7,70 Euro</span>
                            <span class="page-content__block">Zahlung per Sofortüberweisung</span>
                            <span class="page-content__block">Barzahlung bei Abholung (nur nach vorheriger Vereinbarung)</span>
                            <span class="page-content__block">Rechnugs Kauf über Billsafe Aufpreis 1,50€.</span>
                        </p>
                    </div>
                    <div class="col-sm-6 col-lg-offset-1 margin-top-xs-28">
                        <h5 class="page-title">Zahlungsbedingungen EU Länder</h5>
                        <p>
                            <span class="page-content__block">Lieferungen Ausland EU</span>
                            <span class="page-content__block">Vorkasse per Überweisung Versand erfolgt nach Geldeingang auf unser Konto</span>
                            <span class="page-content__block">Zahlung per Paypal</span>
                            <span class="page-content__block">Zahlung per Sofort Überweisung </span>
                            <span class="page-content__block">Lieferungen Schweiz</span>
                        </p>
                        <p>
                            <span class="page-content__block">Zahlung per Paypal</span>
                            <span class="page-content__block">Vorkasse per Überweisung Versand erfolgt nach Geldeingang auf unser Konto</span>
                            <span class="page-content__block">Zahhlung per Scheck  Versand erfolgt nach Geldeingang auf unser Konto</span>
                            <span class="page-content__block">Zahlung per Sofort Überweisung</span>
                            <span class="page-content__block">Unsere Bankverbindung</span>
                        </p>
                    </div>
                </div>
            </div>

            <div class="page-content__row">
                <p>
                    <span class="page-content__block">Kontoinhaber: JUSTSHIRT RETAIL </span>
                    <span class="page-content__block">Bankinstitut: Volksbank Griesheim eG</span>
                    <span class="page-content__block">Kontonummer: 0012132000</span>
                    <span class="page-content__block">Bankleitzahl: 501 904 00</span>
                    <span class="page-content__block">IBAN: DE06 5019 0400 0012 1320 00</span>
                    <span class="page-content__block">BIC: GENODE51FGH</span>
                </p>
                <p>
                    <span class="page-content__block">Unsere Anschrift</span>
                </p>
                <p>
                    <span class="page-content__block">JUSTSHIRT RETAIL</span>
                </p>
                <p>
                    <span class="page-content__block">Inh. Satwant Singh Pahal</span>
                </p>
            </div>
        </div>
    </div>
</div>