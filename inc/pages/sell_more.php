<div class="sell__more clearfix">
	<div class="container">
		<div class="row">
			<div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
				<img src="images/blog/heart.png" alt="" class="sell__more__picture">
				<h2 class="sell__more__title">
					Earn Money by Selling Your Artwork
				</h2>
				<p class="sell__more__text">
					We ensure high quality products, beautiful printing, fast delivery and award-winning service.
				</p>
			</div>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-lg-6 col-lg-offset-3 p-tb40">
		<h2 class="text-center">
			Your design can be used for these products
		</h2>
		<div class="row">
			<div class="col-sm-3 col-xs-6 text-center">
				<div class="list-sell">
					<i class="icon-pink-shirt"></i>
				</div>
				<p class="text-muted">
					T-shirts
				</p>
			</div>
			<div class="col-sm-3 col-xs-6 text-center">
				<div class="list-sell">
					<i class="icon-pink-caps"></i>
				</div>
				<p class="text-muted">
					Caps
				</p>
			</div>
			<div class="col-sm-3 col-xs-6 text-center">
				<div class="list-sell">
					<i class="icon-pink-suspenders"></i>
				</div>
				<p class="text-muted">
					Suspenders
				</p>
			</div>
			<div class="col-sm-3 col-xs-6 text-center">
				<div class="list-sell">
					<i class="icon-pink-cups"></i>
				</div>
				<p class="text-muted">
					Cups
				</p>
			</div>
			<div class="col-sm-3 col-xs-6 text-center">
				<div class="list-sell">
					<i class="icon-pink-towel"></i>
				</div>
				<p class="text-muted">
					Towels
				</p>
			</div>
			<div class="col-sm-3 col-xs-6 text-center">
				<div class="list-sell">
					<i class="icon-pink-pillo"></i>
				</div>
				<p class="text-muted">
					Pillows
				</p>
			</div>
			<div class="col-sm-3 col-xs-6 text-center">
				<div class="list-sell">
					<i class="icon-pink-belts"></i>
				</div>
				<p class="text-muted">
					Belts
				</p>
			</div>
			<div class="col-sm-3 col-xs-6 text-center">
				<div class="list-sell">
					<i class="icon-points"></i>
				</div>
				<p class="text-muted">
					and more
				</p>
			</div>
		</div>
		<p class="sell__text">
			Proin iaculis tincidunt sodales. Proin tristique, sem et bibendum pellentesque, enim diam semper ipsum, sit amet tincidunt nisl ante et nisi. Aliquam consectetur lacus euismod elit aliquet, quis accumsan orci elementum. Morbi elementum quis dolor eu malesuada. 
		</p>
		<div class="row">
			<div class="col-xs-12 text-center">
				<a href="" class="btn__default">
					Start Selling
				</a>
			</div>
		</div>
	</div>
</div>