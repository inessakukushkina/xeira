<?php
	$_GET['id'] = isset($_GET['id']) ? $_GET['id'] : 'catalog';
	$id = $_GET['id'];
?>
<?php require_once 'inc/config/all.php'; ?>
<?php require_once 'inc/modules/header.php';?>

<?php
	if($id == 'checkout' || $id == 'checkout-success'){
		require_once 'inc/modules/head_checkout.php';		
	} else{
		require_once 'inc/modules/head_login.php';
	}
?>
<?php if($id == 'homepage'){ ?>
		</div>
	<?php
		require_once 'inc/modules/slider.php';	
	}
?>
		<div class="custom-form">
			<?php 
				require_once "inc/pages/" . $id . ".php"; 
			?>
		</div>
	</div>
</div>
<?php require_once 'inc/modules/footer.php';?>
<?php
	if($id == 'create-shirt'){ ?>
		<script>
			var selectors = {
		        'tail'   : 10,
		        'editor' : document.getElementById('editor'),
		        'text'   : document.getElementById('text'),
		        'media'  : document.getElementById('media')
		    }

			getPosition(selectors.editor, selectors.text, selectors.tail);
		</script>
	<?php }
?>