<div class="row m-t30 p-b100">
    <div class="col-sm-3 col-md-2">
        <div class="sidebar">
            <h4 class="sidebar__title2">About us</h4>

            <ul class="sidebar__menu">
                <li class="sidebar__menu__item">
                    <a href="">The Company</a>
                </li>
                <li class="sidebar__menu__item">
                    <a href="">Impressum</a>
                </li>
                <li class="sidebar__menu__item active">
                    <a href="">Contact</a>
                </li>
                <li class="sidebar__menu__item">
                    <a href="">Shipping</a>
                </li>
                <li class="sidebar__menu__item">
                    <a href="">Return Policy</a>
                </li>
                <li class="sidebar__menu__item">
                    <a href="">Privacy Policy</a>
                </li>
                <li class="sidebar__menu__item">
                    <a href="">Terms and Condition</a>
                </li>
                <li class="sidebar__menu__item">
                    <a href="">Order Precess</a>
                </li>
                <li class="sidebar__menu__item">
                    <a href="">Blog</a>
                </li>
            </ul>
        </div>
    </div>
    <div class="col-lg-6 col-md-8 col-sm-9">
        <h4 class="contact__title">
            <span>Get in touch</span>
        </h4>
        <p class="main_text">
            We are always happy to make valuable new contacts
        </p>
        <div class="row">
            <div class="col-sm-6">
                <div class="form-group form-group--bigger form-name">
                    <input type="text" class="form-control" placeholder="First Name">
                </div>
            </div>           
            <div class="col-sm-6">
                <div class="form-group form-group--bigger form-mail">
                    <input type="email" class="form-control" placeholder="Email">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <div class="form-group form-group--bigger">
                    <textarea name="" class="form-control" rows="8" placeholder="Your message"></textarea>
                </div>
            </div>
            <div class="col-sm-12 text-right">
                <button class="btn__default form-group-btn">Send</button>
            </div>
        </div>
    </div>
</div>