<div class="row m-t30 p-b100">
    <div class="col-sm-3 col-md-2">
        <div class="sidebar">
            <h4 class="sidebar__title2">About us</h4>

            <ul class="sidebar__menu">
                <li class="sidebar__menu__item">
                    <a href="">The Company</a>
                </li>
                <li class="sidebar__menu__item">
                    <a href="">Impressum</a>
                </li>
                <li class="sidebar__menu__item">
                    <a href="">Contact</a>
                </li>
                <li class="sidebar__menu__item active">
                    <a href="">Shipping</a>
                </li>
                <li class="sidebar__menu__item">
                    <a href="">Return Policy</a>
                </li>
                <li class="sidebar__menu__item">
                    <a href="">Privacy Policy</a>
                </li>
                <li class="sidebar__menu__item">
                    <a href="">Terms and Condition</a>
                </li>
                <li class="sidebar__menu__item">
                    <a href="">Order Precess</a>
                </li>
                <li class="sidebar__menu__item">
                    <a href="">Blog</a>
                </li>
            </ul>
        </div>
    </div>
    <div class="col-sm-9 col-md-10">
        <div class="page-content page-content--m0">
            <div class="page-content__img-box">
                <img class="page-content__image" src="images/about/team.jpg" alt=""/>
            </div>
            <div class="page-content__row page-content__row--bp0">
                <h4 class="page-title">Über unser Unternehmen</h4>
                <p>Wir sind ein junges Unternehmen und unser Motto ist es neue Trends zu fairen Preisen anzubieten.</p>
                <p>In unserem Unternehmen steht der Kunde im Mittelpunkt und deshalb ist unser kreatives Team stets daran Interessiert Ihnen die besten Produkte und den besten Service anzubieten.</p>
                <p>Falls sie weitere Fragen haben, nehmen Sie bitte Kontakt auf.</p>
                <p>Gefällt Ihnen unsere Seite? Dann teilen Sie Justshirt.de mit Ihren Freunden auf Facebook!!!</p>
            </div>
        </div>
    </div>
</div>