<ul class="breadcrumbs">
	<li class="breadcrumbs__item">
		<a href="" class="breadcrumbs__item__link">
			My account
		</a>
	</li>
	<li class="breadcrumbs__item">
		<a href="" class="breadcrumbs__item__link">
			My campaigns
		</a>
	</li>
</ul>
<div class="row">
	<div class="col-sm-3">
		<div class="sidebar">			
			<h2 class="sidebar_title">
				My Account
			</h2>
			<ul class="sidebar__list">
				<li class="sidebar__list__item">
					<a href="" class="sidebar__list__item__link active">
						My Designs
					</a>
				</li>
				<li class="sidebar__list__item">
					<a href="" class="sidebar__list__item__link">
						My Purchases
					</a>
				</li>
				<li class="sidebar__list__item">
					<a href="" class="sidebar__list__item__link">
						My Sold Items
					</a>
				</li>
				<li class="sidebar__list__item">
					<a href="" class="sidebar__list__item__link">
						My Campaigns
					</a>
				</li>
				<li class="sidebar__list__item">
					<a href="" class="sidebar__list__item__link">
						Wishlist
					</a>
				</li>
				<li class="sidebar__list__item">
					<a href="" class="sidebar__list__item__link">
						Reviews
					</a>
				</li>
				<li class="sidebar__list__item">
					<a href="" class="sidebar__list__item__link">
						Invite & earn
					</a>
				</li>
				<li class="sidebar__list__item">
					<a href="" class="sidebar__list__item__link">
						Settings
					</a>
				</li>
			</ul>
		</div>
	</div>
	<div class="col-sm-9">
		<h2 class="head__title">
			My Wishlist
		</h2>
		<div class="row">
			<div class="col-xs-12">
				<div class="product__preview wishlist__block clearfix add-to-cart__block" data-continue>
					<div class="row">
						<div class="col-md-4">
							<div class="product__preview__picture">
								<img src="images/products/shirt1.png" alt="" class="product__preview__avatar product__picture__image">
							</div>
							<span class="product__preview__content">								
								<h5 class="product__preview__title">
									Sample Fashion Product
								</h5>						
								<ul class="product__preview__color_list">
									<li class="product__preview__color_list__item">
										<span class="color__block color__black"></span>
									</li>
									<li class="product__preview__color_list__item">
										<span class="color__block color__orange"></span>
									</li>
									<li class="product__preview__color_list__item">
										<span class="color__block color__green"></span>
									</li>
								</ul>
								<p class="product__availability">
									Added: 10/07/2015
								</p>
								<span class="product__preview__price">
									<span class="product__preview__price__active">
										$ 15.00
										<span class="product__preview__price__old">$ 25.00</span>
										<div class="product__preview__label label__sale" data-content="Lorem ipsum dolor sit amet"></div>
									</span>
								</span>
							</span>
						</div>
						<div class="col-md-8">
							<div class="clearfix">
								<i class="icon-close"></i>
								<p class="product__price pull-left">
									$ 50.00
								</p>
								<div class="pull-sm-right text-right">
									<p class="product__availability">
										Availability: In Stock
									</p>
									<p class="product__required">
										* Required Fields
									</p>
								</div>
							</div>
							<p class="product__subtitle">
								<span class="product__required">*</span> <strong>Color:</strong> gray
							</p>
							<div class="clearfix">
								<div class="color__block active">
									<div class="color__block__item">
										<div class="color__mark color__pink"></div>
									</div>
								</div>
								<div class="color__block">
									<div class="color__block__item">
										<div class="color__mark color__red"></div>
									</div>
								</div>
								<div class="color__block">
									<div class="color__block__item">
										<div class="color__mark color__green"></div>
									</div>
								</div>
							</div>
							<p class="product__subtitle clearfix">
								<span class="product__required">*</span> <strong>Size:</strong> S
							</p>
							<div class="clearfix">
								<div class="size__block active">
									<div class="size__block__item">S</div>
								</div>
								<div class="size__block">
									<div class="size__block__item">L</div>
								</div>
								<div class="size__block">
									<div class="size__block__item">M</div>
								</div>
								<div class="size__block">
									<div class="size__block__item">XL</div>
								</div>
							</div>
							<p class="product__subtitle">
								<span class="product__required"></span> <strong>Shipping:</strong> Free shipping
							</p>
							<div class="clearfix m-t20">
								<div class="quantity clearfix">
									<p class="quantity__text">Qty:</p> 
									<div class="quantity__block input-group">
										<button class="quantity__decrement input-group-btn" disabled="disabled">-</button>
										<input class="quantity__control form-control" type="text" value="1">	
										<button class="quantity__increment input-group-btn">+</button>			
									</div>
								</div>
								<button class="product__add_to_cart">
									Continue <i class="icon-left"></i>
								</button>
							</div>
						</div>
					</div>
				</div>
				<div class="product__preview wishlist__block clearfix" data-add>
					<div class="row">
						<div class="col-md-4">
							<div class="product__preview__picture">
								<img src="images/products/shirt1.png" alt="" class="product__preview__avatar product__picture__image">
							</div>
						</div>
						<div class="col-md-5">
							<h5 class="product__preview__title">
								Sample Fashion Product
							</h5>						
							<ul class="product__preview__color_list">
								<li class="product__preview__color_list__item">
									<div class="color__block color__black"></div>
								</li>
								<li class="product__preview__color_list__item">
									<div class="color__block color__orange"></div>
								</li>
								<li class="product__preview__color_list__item">
									<div class="color__block color__green"></div>
								</li>
							</ul>
							<p class="wishlist__block__text">
								Added: 10/07/2015
							</p>
							<div class="product__preview__price">
								<p class="product__preview__price__active">
									$ 15.00
									<span class="product__preview__price__old">$ 25.00</span>						
								</p>
								<div class="product__preview__label label__sale" data-content="Lorem ipsum dolor sit amet"></div>
							</div>
						</div>
						<div class="col-md-3">
							<a href="" class="remove__checkout pull-md-right">x</a>
							<button class="btn__accept btn__continue m-0">
								Add to cart
							</button>
						</div>
					</div>
				</div>
				<!--<div class="product__preview wishlist__block clearfix">
					<div class="row">
						<div class="col-md-4">
							<div class="product__preview__picture">
								<img src="images/products/shirt2.png" alt="" class="product__preview__avatar product__picture__image">
							</div>
						</div>
						<div class="col-md-5">
							<h5 class="product__preview__title clearfix">
								Sample Fashion Product
							</h5>						
							<ul class="product__preview__color_list">
								<li class="product__preview__color_list__item">
									<div class="color__block color__black"></div>
								</li>
								<li class="product__preview__color_list__item">
									<div class="color__block color__orange"></div>
								</li>
								<li class="product__preview__color_list__item">
									<div class="color__block color__green"></div>
								</li>
							</ul>
							<p class="wishlist__block__text">
								Added: 10/07/2015
							</p>
							<div class="product__preview__price">
								<p class="product__preview__price__active">
									$ 15.00
									<span class="product__preview__price__old">$ 25.00</span>						
								</p>
								<div class="product__preview__content">
									<div class="product__preview__label label__sale">
										Sale
									</div>											
								</div>
							</div>
						</div>
						<div class="col-md-3">
							<a href="" class="remove__checkout pull-md-right">x</a>
							<a href="" class="btn__accept m-0">
								Add to cart
							</a>
						</div>
					</div>
				</div>
				<div class="product__preview wishlist__block clearfix">
					<div class="row">
						<div class="col-md-4">
							<div class="product__preview__picture">
								<img src="images/products/shirt3.png" alt="" class="product__preview__avatar product__picture__image">
							</div>
						</div>
						<div class="col-md-5">
							<h5 class="product__preview__title">
								Sample Fashion Product
							</h5>						
							<ul class="product__preview__color_list">
								<li class="product__preview__color_list__item">
									<div class="color__block color__black"></div>
								</li>
								<li class="product__preview__color_list__item">
									<div class="color__block color__orange"></div>
								</li>
								<li class="product__preview__color_list__item">
									<div class="color__block color__green"></div>
								</li>
							</ul>
							<p class="wishlist__block__text">
								Added: 10/07/2015
							</p>
							<div class="product__preview__price">
								<p class="product__preview__price__active">
									$ 15.00
									<span class="product__preview__price__old">$ 25.00</span>						
								</p>
								<div class="product__preview__content">
									<div class="product__preview__label label__sale">
										Sale
									</div>											
								</div>
							</div>
						</div>
						<div class="col-md-3">
							<a href="" class="remove__checkout pull-md-right">x</a>
							<a href="" class="btn__accept m-0">
								Add to cart
							</a>
						</div>
					</div>
				</div>
				<div class="product__preview wishlist__block clearfix">
					<div class="row">
						<div class="col-md-4">
							<div class="product__preview__picture">
								<img src="images/products/shirt1.png" alt="" class="product__preview__avatar product__picture__image">
							</div>
						</div>
						<div class="col-md-5">
							<h5 class="product__preview__title">
								Sample Fashion Product
							</h5>						
							<ul class="product__preview__color_list">
								<li class="product__preview__color_list__item">
									<div class="color__block color__black"></div>
								</li>
								<li class="product__preview__color_list__item">
									<div class="color__block color__orange"></div>
								</li>
								<li class="product__preview__color_list__item">
									<div class="color__block color__green"></div>
								</li>
							</ul>
							<p class="wishlist__block__text">
								Added: 10/07/2015
							</p>
							<div class="product__preview__price">
								<p class="product__preview__price__active">
									$ 15.00
									<span class="product__preview__price__old">$ 25.00</span>						
								</p>
								<div class="product__preview__content">
									<div class="product__preview__label label__sale">
										Sale
									</div>											
								</div>
							</div>
						</div>
						<div class="col-md-3">
							<a href="" class="remove__checkout pull-md-right">x</a>
							<a href="" class="btn__accept m-0">
								Add to cart
							</a>
						</div>
					</div>
				</div>-->
				<ul class="pagination">
			        <li class="pagination__item">
			            <a href="" class="pagination__item__link disabled">
			                «
			            </a>
			        </li>
			        <li class="pagination__item">
			            <a href="" class="pagination__item__link active">
			                1
			            </a>
			        </li>
			        <li class="pagination__item">
			            <a href="" class="pagination__item__link">
			                2
			            </a>
			        </li>
			        <li class="pagination__item">
			            <a href="" class="pagination__item__link">
			                3
			            </a>
			        </li>
			        <li class="pagination__item">
			            <a href="" class="pagination__item__link">
			              »
			            </a>
			        </li>
			    </ul>
			</div>
		</div>
	</div>
</div>