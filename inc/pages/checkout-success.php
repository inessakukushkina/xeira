<div class="row">
	<div class="col-xs-12">
		<h2 class="head__title p-t20">
			Thank you for your order
		</h2>
	</div>
</div>
<div class="row">
	<div class="col-xs-12">
		<div class="checkout__success">
			<p class="checkout__success__text">
				Lorem ipsum dolor sit amet, consectetur adipisicing elit. 
				Ipsam sint magni ratione delectus, id laborum ex aut non eius, 
				reiciendis quae beatae at fuga cum dolores dicta nihil! Culpa, totam!
			</p>			
		</div>
		<div class="product__share b-None m-t0">					
			<div class="row">
				<div class="col-sm-6">
					<p class="product__share__text p-t5">
						Share: 
						<a href=""><i class="icon-facebook-gray"></i></a>
						<a href=""><i class="icon-twitter-gray"></i></a>
						<a href=""><i class="icon-google-gray"></i></a>							
					</p>
				</div>
				<div class="col-sm-6 sm-right">
					<button class="btn-invite">
						Invite Friends
					</button>
				</div>
			</div>
		</div>
	</div>
</div>
</div>