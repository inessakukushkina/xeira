<div class="row">
	<div class="col-md-6 col-md-offset-3">
		<div class="sign__block">
			<h4 class="sign__block__title">
				<span>Sign in</span>
				Welcome to JUSTSHIRT
			</h4>
			<div class="sign__block__social">
				<a href="">
					<i class="icon-facebook m-r10"></i>
				</a>
				<a href="">
					<i class="icon-twitter m-r10"></i>
				</a>
				<a href="">
					<i class="icon-google"></i>
				</a>
			</div>
			<div class="block__or">
				<span class="block__or__text">or</span>
			</div>
			<div class="form-group has-error">
				<input type="text" class="form-control" placeholder="Email address">
				<p class="error__text">
					Please insert a correct e-mail address
				</p>
			</div>
			<div class="form-group has-error">
				<input type="text" class="form-control" placeholder="Password">
				<p class="error__text">
					Please check you typed your password correctly
				</p>
			</div>
			<div class="row">
				<div class="col-sm-6">
					<label class="cb-checkbox">
						<input type="checkbox">
						Remember me
					</label>
				</div>
				<div class="col-sm-6 sm-right">
					<a href="" class="sign__block__link">
						Forgot password?
					</a>
				</div>
			</div>
			<button class="sign__block__accept">
				Sign in
			</button>
			<p class="sign__block__text">
				Don’t have an account? <a href="" class="sign__block__link">
						Join Now
				</a>
			</p>
		</div>
	</div>
</div>