<!DOCTYPE html>
<!--[if !IE]><!--><script>if(/*@cc_on!@*/false){document.documentElement.className+=' ie10';}</script><!--<![endif]-->
<!--[if lt IE 9]><script src="https://html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<!--[if IE 9 ]><html class="ie9"><![endif]-->
<!--[if IE 8 ]><html class="ie8"><![endif]-->
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?php echo current_title() . ' | ' . 'Justshirt'; ?></title>
    <link rel="shortcut icon" href="images/favicon.ico" />
    <meta name="viewport" content="width=device-width, user-scalable=no"> 
    
     <!-- SEO --> 
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="theme-color" content="#e4436a">

    <!-- css -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/material-design-iconic-font/2.2.0/css/material-design-iconic-font.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css">
    <link rel="stylesheet" href="css/bootstrap.css">
    <link rel="stylesheet" href="css/main.css">    

    <!-- Google fonts api -->
    <link href='http://fonts.googleapis.com/css?family=Ubuntu:300,400,700' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Lato:400,700' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>

    
    <!-- Library -->   
    <script src="//code.jquery.com/jquery.min.js"></script>
    <script src="//code.jquery.com/ui/1.11.2/jquery-ui.min.js"></script>

    <script src="//cdn.datatables.net/1.10.8/js/jquery.dataTables.min.js"></script>
</head>
<body>
<!--[if lte IE 8]>
<div id="browser-status"> Dear Web User, your browser is out of date! <a href="http://browsehappy.com/" target="_blank">
  Please Upgrade Your Browser</a></div><![endif]-->