<ul class="breadcrumbs">
	<li class="breadcrumbs__item">
		<a href="" class="breadcrumbs__item__link">
			My account
		</a>
	</li>
	<li class="breadcrumbs__item">
		<a href="" class="breadcrumbs__item__link">
			Invite & Earn
		</a>
	</li>
</ul>
<div class="row">
	<div class="col-sm-3">
		<div class="sidebar">			
			<h2 class="sidebar_title">
				My Account
			</h2>
			<ul class="sidebar__list">
				<li class="sidebar__list__item">
					<a href="" class="sidebar__list__item__link active">
						My Designs
					</a>
				</li>
				<li class="sidebar__list__item">
					<a href="" class="sidebar__list__item__link">
						My Purchases
					</a>
				</li>
				<li class="sidebar__list__item">
					<a href="" class="sidebar__list__item__link">
						My Sold Items
					</a>
				</li>
				<li class="sidebar__list__item">
					<a href="" class="sidebar__list__item__link">
						My Campaigns
					</a>
				</li>
				<li class="sidebar__list__item">
					<a href="" class="sidebar__list__item__link">
						Wishlist
					</a>
				</li>
				<li class="sidebar__list__item">
					<a href="" class="sidebar__list__item__link">
						Reviews
					</a>
				</li>
				<li class="sidebar__list__item">
					<a href="" class="sidebar__list__item__link">
						Invite & earn
					</a>
				</li>
				<li class="sidebar__list__item">
					<a href="" class="sidebar__list__item__link">
						Settings
					</a>
				</li>
			</ul>
		</div>
	</div>
	<div class="col-sm-9">
		<h2 class="head__title">
			Settings
		</h2>
		<h3 class="head__subtitle">
			Profile
		</h3>
		<div class="row m-t20">
			<div class="col-sm-4">
				<p class="form-label p-t7">
					First name
				</p>
			</div>
			<div class="col-sm-8">
				<div class="form-group">
					<input type="text" class="form-control">
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-4">
				<p class="form-label p-t7">
					Last name
				</p>
			</div>
			<div class="col-sm-8">
				<div class="form-group">
					<input type="text" class="form-control">
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-4">
				<p class="form-label p-t7">
					Email
				</p>
			</div>
			<div class="col-sm-8">
				<div class="form-group">
					<input type="text" class="form-control" value="email.example.com" disabled="disabled">
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-4">
				<p class="form-label p-t7">
					Phone number
				</p>
			</div>
			<div class="col-sm-5">
				<div class="form-group">
					<input type="text" class="form-control">
				</div>
			</div>
		</div>
		<h3 class="head__subtitle">
			Address
		</h3>
		<div class="row">
			<div class="col-xs-12">
				<div class="address">
					<div class="row">
						<div class="col-sm-6">
							<p class="text-lighten">
								Sharon Reynolds
							</p>
							<p class="text-page">
								(645) 645 6464
							</p>
							<p class="text-page">
								Mulholland Drive 12
							</p>
							<p class="text-page">
								Los Angeles California
							</p>
							<p class="text-page">
								US
							</p>
						</div>
						<div class="col-sm-6 sm-right">
							<button class="btn__edit">
								<i class="fa fa-pencil"></i>
							</button>
							<button class="btn__trash">
								<i class="fa fa-trash"></i>
							</button>
							<button class="btn__default m-t40">
								<i class="fa fa-check-circle"></i>
								Default address
							</button>
						</div>
					</div>					
				</div>
				<div class="address">
					<div class="row">
						<div class="col-sm-6">
							<p class="text-lighten">
								Sharon Reynolds
							</p>
							<p class="text-page">
								(645) 645 6464
							</p>
							<p class="text-page">
								Mulholland Drive 12
							</p>
							<p class="text-page">
								Los Angeles California
							</p>
							<p class="text-page">
								US
							</p>
						</div>
						<div class="col-sm-6 sm-right">
							<button class="btn__edit">
								<i class="fa fa-pencil"></i>
							</button>
							<button class="btn__trash">
								<i class="fa fa-trash"></i>
							</button>
							<button class="btn__primary m-t40">
								Use this address
							</button>
						</div>
					</div>					
				</div>
			</div>
		</div>
		<div class="row address__settings m-t20 is-hidden">
			<div class="col-xs-12">
				<div class="b-Bottom">
					<div class="row">
						<div class="col-sm-4">
							<p class="form-label p-t7">
								Phone number
							</p>
						</div>
						<div class="col-sm-8">
							<div class="form-group">
								<input type="text" class="form-control">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-4">
							<p class="form-label p-t7">
								Zip Code
							</p>
						</div>
						<div class="col-sm-5">
							<div class="form-group">
								<input type="text" class="form-control">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-4">
							<p class="form-label p-t7">
								State
							</p>
						</div>
						<div class="col-sm-8">
							<div class="form-group">
								<input type="text" class="form-control">
							</div>
						</div>
					</div>	
					<div class="row">
						<div class="col-sm-4">
							<p class="form-label p-t7">
								City
							</p>
						</div>
						<div class="col-sm-8">
							<div class="form-group">
								<input type="text" class="form-control">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-4">
							<p class="form-label p-t7">
								Street Address
							</p>
						</div>
						<div class="col-sm-8">
							<div class="form-group">
								<input type="text" class="form-control">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12 text-right">
							<button class="btn__primary m-r5" data-address>
								Cancel
							</button>
							<button class="btn__default" data-address>
								Apply
							</button>
						</div>
					</div>	
				</div>								
			</div>
		</div>

		<div class="row">
			<div class="col-xs-12 m-tb20">
				<a href="" class="link__page-16 show__address" data-address>
					+ Add new address
				</a>
			</div>
		</div>	

		<h3 class="head__subtitle">
			Password
		</h3>
		<div class="row m-t20 password__settings">
			<div class="col-sm-4">
				<div class="form-group">
					<input type="text" class="form-control" value="******" disabled="disabled">
				</div>
			</div>
			<div class="col-sm-5">
				<a href="" class="link__page-16">
					<i class="fa fa-rotate-left m-r5"></i> Change password
				</a>
			</div>
		</div>
	</div>
</div>