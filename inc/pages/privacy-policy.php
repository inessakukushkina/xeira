<div class="row m-t30 p-b100">
    <div class="col-sm-3 col-md-2">
        <div class="sidebar">
            <h4 class="sidebar__title2">About us</h4>

            <ul class="sidebar__menu">
                <li class="sidebar__menu__item">
                    <a href="">The Company</a>
                </li>
                <li class="sidebar__menu__item">
                    <a href="">Impressum</a>
                </li>
                <li class="sidebar__menu__item">
                    <a href="">Contact</a>
                </li>
                <li class="sidebar__menu__item">
                    <a href="">Shipping</a>
                </li>
                <li class="sidebar__menu__item">
                    <a href="">Return Policy</a>
                </li>
                <li class="sidebar__menu__item active">
                    <a href="">Privacy Policy</a>
                </li>
                <li class="sidebar__menu__item">
                    <a href="">Terms and Condition</a>
                </li>
                <li class="sidebar__menu__item">
                    <a href="">Order Precess</a>
                </li>
                <li class="sidebar__menu__item">
                    <a href="">Blog</a>
                </li>
            </ul>
        </div>
    </div>
    <div class="col-sm-9 col-md-10">
        <h5 class="page-title">Datenschutzerklärung</h5>

        <p class="page-text">Wir freuen uns, dass Sie unsere Website besuchen und bedanken uns für Ihr Interesse an unserem Unternehmen und unseren Produkten. Der Schutz Ihrer Privatsphäre bei der Nutzung unserer Webseiten ist uns wichtig. Daher nehmen Sie bitte nachstehende Informationen zur Kenntnis:</p>

        <div class="page-content">
            <div class="page-content__row">
                <div class="page-content__number">
                    <span class="page-content__number-label">1</span>
                </div>
                <div class="page-content__container">
                    <h6 class="page-content__title">Erhebung, Verarbeitung und Nutzung personenbezogener Daten</h6>
                    <p>Sie können unsere Webseiten grundsätzlich besuchen, ohne dass wir personenbezogene Daten von Ihnen erheben. Personenbezogene Daten werden nur erhoben, wenn Sie uns diese von sich aus zur Durchführung eines Vertrages, bei der Eröffnung eines Kundenkontos oder im Rahmen der Kontaktaufnahme mitteilen. Diese Daten werden ohne Ihre ausdrückliche Einwilligung jeweils allein zur Vertragsabwicklung und Bearbeitung Ihrer Anfragen genutzt. Nach vollständiger Abwicklung des Vertrages werden Ihre Daten mit Rücksicht auf steuer- und handelsrechtliche Aufbewahrungsfristen gespeichert, nach Ablauf dieser Fristen jedoch gelöscht.</p>
                    <p>Darüber hinaus werden personenbezogene Daten erhoben, wenn Sie sich zu unserem E-Mail-Newsletter anmelden. Diese Daten werden von uns zu eigenen Werbezwecken in Form unseres E-Mail-Newsletters genutzt, sofern Sie hierin ausdrücklich eingewilligt haben wie folgt: "Newsletter abonnieren"</p>
                    <p>Sie können den Newsletter jederzeit über den dafür vorgesehenen Link im Newsletter oder durch entsprechende Nachricht an uns abbestellen. Nach erfolgter Abmeldung wird Ihre E-Mailadresse unverzüglich gelöscht.</p>
                </div>
            </div>

            <div class="page-content__row">
                <div class="page-content__number">
                    <span class="page-content__number-label">2</span>
                </div>
                <div class="page-content__container">
                    <h6 class="page-content__title">Weitergabe personenbezogener Daten</h6>
                    <p>Für die Bestellabwicklung werden personenbezogenen Daten nötig. Diese Daten werden ohne Ihre ausdrückliche Einwilligung jeweils allein zur Vertragsabwicklung und Bearbeitung Ihrer Anfragen genutzt. Nach vollständiger Abwicklung des Vertrages werden Ihre Daten mit Rücksicht auf steuer- und handelsrechtliche Aufbewahrungsfristen gespeichert, nach Ablauf dieser Fristen jedoch gelöscht.</p>
                    <p>Die von uns erhobenen personenbezogenen Daten werden im Rahmen der Vertragsabwicklung an das mit der Lieferung beauftragte Transportunternehmen weitergegeben, soweit dies zur Lieferung der Ware erforderlich ist. Wir geben Ihre Zahlungsdaten im Rahmen der Abwicklung von Zahlungen an das beauftragte Kreditinstitut weiter.</p>
                </div>
            </div>

            <div class="page-content__row">
                <div class="page-content__number">
                    <span class="page-content__number-label">3</span>
                </div>
                <div class="page-content__container">
                    <h6 class="page-content__title">Bonitäts- bzw. Identitätsprüfung und Scoring bei Rechnungskauf</h6>
                    <p>Für die Bestellabwicklung werden personenbezogenen Daten nötig. Diese Daten werden ohne Ihre ausdrückliche Einwilligung jeweils allein zur Vertragsabwicklung und Bearbeitung Ihrer Anfragen genutzt. Nach vollständiger Abwicklung des Vertrages werden Ihre Daten mit Rücksicht auf steuer- und handelsrechtliche Aufbewahrungsfristen gespeichert, nach Ablauf dieser Fristen jedoch gelöscht.</p>
                    <p>Die von uns erhobenen personenbezogenen Daten werden im Rahmen der Vertragsabwicklung an das mit der Lieferung beauftragte Transportunternehmen weitergegeben, soweit dies zur Lieferung der Ware erforderlich ist. Wir geben Ihre Zahlungsdaten im Rahmen der Abwicklung von Zahlungen an das beauftragte Kreditinstitut weiter.</p>

                    <div class="page-content__row">
                        <h6 class="page-content__title page-content__title--has-dot">3.1</h6>
                        <p>Sollten wir in Vorleistung treten (z.B. Lieferung auf Rechnung), behalten wir uns vor, eine Bonitätsprüfung auf der Grundlage athematisch-statistischer Verfahren durchzuführen, um unsere berechtigten Interessen zu wahren.</p>
                    </div>

                    <div class="page-content__row">
                        <h6 class="page-content__title page-content__title--has-dot">3.2</h6>
                        <p>Bei Auswahl der Zahlungsart „Versand auf Rechnung ( Billsafe)“ werden Sie im Bestellablauf aufgefordert, Ihre persönlichen Daten (Vor- und Nachname, Straße, Hausnummer, Postleitzahl, Ort, Geburtsdatum, E-Mail-Adresse und Telefonnummer) anzugeben. PayPal (Europe) S.à r.l. et Cie, S.C.A., 22-24 Boulevard Royal, L-2449 Luxemburg, prüft auf Basis der von Ihnen angegebenen persönlichen Daten sowie weiterer Daten (wie etwa Warenkorb, Rechnungsbetrag, Bestellhistorie, Zahlungserfahrungen), ob die von Ihnen ausgewählte Zahlungsmöglichkeit im Hinblick auf Zahlungs- und/oder Forderungsausfallrisiken gewährt werden kann. Zur Entscheidung im Rahmen der Antragsprüfung können neben PayPal-internen Kriterien auch Identitäts-und Bonitätsinformationen von folgenden Auskunfteien einbezogen werden:</p>

                        <ul class="page-content__list">
                            <li>- SCHUFA Holding AG, Kormoranweg 5, D-65201 Wiesbaden, Tel.: +49(0)611-9278-0, Fax: -109</li>
                            <li>- Deltavista GmbH, Freisinger Landstraße 74, D-80939 München, Tel.: +49 (0)721-25511-777, Fax: -22</li>
                            <li>- accumio finance services GmbH Customer Care Service Center, Postfach 110254, 30099 Hannover</li>
                            <li>- infoscore Consumer Data GmbH (arvato), Rheinstraße 99, D-76532 Baden-Baden, Tel.: +49 (0)7221-5040-1000, Fax: -1001</li>
                            <li>- Bürgel Wirtschaftsinformationen GmbH & Co. KG, Gasstraße 18, D-22761 Hamburg, Tel.: +49 (0)40-89803-0, Fax: -777</li>
                            <li>- mediafinanz AG, Weiße Breite 5, 49084 Osnabrück Tel.: +49 (0)541- 2029-0, Fax: -101</li>
                            <li>- CEG Creditreform Consumer GmbH, Hellersbergstraße 11, D-41460 Neuss, Tel.: +49 (0)2131-109-501, Fax: -557</li>
                        </ul>

                        <p>
                            Die Bonitätsauskunft kann Wahrscheinlichkeitswerte enthalten (sog. Score-Werte). Soweit Score-Werte in das Ergebnis der Bonitätsauskunft einfließen, haben diese ihre Grundlage in einem wissenschaftlich anerkannten mathematisch-statistischen Verfahren, in die Berechnung der Score-Werte fließen unter anderem Anschriftendaten ein. Zum Zwecke der Identitätsprüfung werden Ihre persönlichen Daten an eine der obigen Auskunfteien übermittelt. Die Auskunftei teilt daraufhin den Grad der Übereinstimmung der bei ihr gespeicherten Personalien mit den von Ihnen angegebenen Daten in Prozentwerten sowie ggf. einen Hinweis auf eine zurückliegend bei der Auskunftei durchgeführte ausweisgestützte Legitimationsprüfung mit. PayPal kann somit anhand der übermittelten Übereinstimmungsraten erkennen, ob eine Person unter der von Ihnen angegebenen Anschrift im Datenbestand der Auskunftei gespeichert ist. Unter der nachstehenden Internetadresse erhalten Sie weitere Informationen über die Datenschutzbestimmungen von PayPals BillSAFE:
                            <a href="http://www.billsafe.de/privacy-policy/merchant">http://www.billsafe.de/privacy-policy/merchant</a>
                        </p>
                    </div>
                </div>
            </div>

            <div class="page-content__row">
                <div class="page-content__number">
                    <span class="page-content__number-label">4</span>
                </div>
                <div class="page-content__container">
                    <h6 class="page-content__title">Cookies</h6>
                    <p>Diese Website nutzt so genannte „Cookies“, welche dazu dienen, unsere Internetpräsenz insgesamt nutzerfreundlicher, effektiver sowie sicherer zu machen – etwa wenn es darum geht, das Navigieren auf unserer Website zu beschleunigen. Zudem versetzen uns Cookies in die Lage, die Häufigkeit von Seitenaufrufen und die allgemeine Navigation zu messen. Bei Cookies handelt es sich um kleine Textdateien, die auf Ihrem Computersystem abgelegt werden. Wir weisen Sie darauf hin, dass einige dieser Cookies von unserem Server auf Ihr Computersystem überspielt werden, wobei es sich dabei meist um so genannte "Session-Cookies " handelt. „Session-Cookies“ zeichnen sich dadurch aus, dass diese automatisch nach Ende der Browser-Sitzung wieder von Ihrer Festplatte gelöscht werden. Andere Cookies verbleiben auf Ihrem Computersystem und ermöglichen es uns, Ihr Computersystem bei Ihrem nächsten Besuch wieder zu erkennen (sog. dauerhafte Cookies). Selbstverständlich können Sie Cookies jederzeit ablehnen, sofern Ihr Browser dies zulässt. Bitte beachten Sie, dass bestimmte Funktionen dieser Website möglicherweise nicht oder nur eingeschränkt genutzt werden können, wenn Ihr Browser so eingestellt ist, dass keine Cookies (unserer Website) angenommen werden.</p>
                </div>
            </div>

            <div class="page-content__row">
                <div class="page-content__number">
                    <span class="page-content__number-label">5</span>
                </div>
                <div class="page-content__container">
                    <h6 class="page-content__title">Verwendung von Twitter-Plugins</h6>
                    <p>Wir haben in unsere Internetpräsenz sogenannte Twitter-Widgets und/oder Twitter-Schaltflächen (nachfolgend “Twitter-Plugin”) des sozialen Netzwerkes twitter.com (nachfolgend „Twitter“) eingebunden. Twitter ist ein Unternehmen der Twitter Inc. 795 Folsom St., Suite 600, San Francisco, CA 94107, USA.</p>
                    <p>Bei jedem Aufruf einer Webseite unserer Internetpräsenz, die mit einem solchen Twitter-Plugin versehen ist, veranlasst das Plugin, dass der von Ihnen verwendete Browser die optische Darstellung des Twitter-Plugins vom Twitter-Server lädt und darstellt. Durch die Einbindung des Twitter-Plugins erhält Twitter Log-Daten und platziert auf Ihrem Rechner einen Cookie zur Identifikation Ihres Browsers. Die Log-Daten enthalten unter anderem Ihre IP-Adresse, den Browsertype, das Betriebssystem, die zuvor aufgerufene Webseite, besuchte Webseiten, Ihren Standort, Ihren Mobilfunkanbieter, Ihr mobiles Gerät, Applikations-IDs, Suchbegriffe sowie Cookie-Informationen.</p>
                    <p>Twitter verwendet diese Log-Daten, um die Dienste bereitzustellen und sie zu bewerten, anzupassen und zu verbessern. Insbesondere können die Daten verwendet werden um Inhalte auf Twitter auf Basis von besuchten Websites mit Twitter-Plugins auf Sie abzustimmen. Dies erfolgt unabhängig davon ob Sie einen Twitter-Account besitzen oder nicht. Unter der nachstehenden Internetadresse finden Sie die Datenschutzhinweise von Twitter mit näheren Informationen zur Erhebung und Nutzung von Daten durch Twitter: http://twitter.com/privacy</p>
                    <p>Um der Verwendung von Cookies auf Ihrem Computer generell zu widersprechen, können Sie Ihren Internetbrowser so einstellen, dass zukünftig keine Cookies mehr auf Ihrem Computer abgelegt werden können bzw. bereits abgelegte Cookies gelöscht werden. Das Abschalten sämtlicher Cookies kann jedoch dazu führen, dass einige Dienste von Twitter nicht mehr ausgeführt werden können.</p>
                </div>
            </div>

            <div class="page-content__row">
                <div class="page-content__number">
                    <span class="page-content__number-label">6</span>
                </div>
                <div class="page-content__container">
                    <h6 class="page-content__title">Einsatz von Google AdSense</h6>
                    <p>Diese Website benutzt Google AdSense, einen Webanzeigendienst der Google Inc. („Google“). Google AdSense verwendet sog. „DoubleClick DART Cookies“ („Cookies“), dies sind Textdateien, die auf Ihrem Computer gespeichert werden und die eine Analyse der Benutzung der Website durch Sie ermöglichen. Darüber hinaus verwendet Google AdSense zur Sammlung von Informationen auch sog. "Web-Bacons" (kleine unsichtbare Grafiken), durch deren Verwendung einfache Aktionen wie der Besucherverkehr auf der Webseite aufgezeichnet, gesammelt und ausgewertet werden können. Die durch den Cookie und/ oder Web-Beacon erzeugten Informationen (einschließlich Ihrer IP-Adresse) über Ihre Benutzung dieser Website werden in der Regel an einen Server von Google in den USA übertragen und dort gespeichert.</p>
                    <p>
                        Google benutzt die so erhaltenen Informationen, um eine Auswertung Ihres Nutzungsverhaltens im Hinblick auf die AdSense-Anzeigen durchzuführen. Die im Rahmen von Google AdSense von Ihrem Browser übermittelte IP-Adresse wird nicht mit anderen Daten von Google zusammengeführt. Die von Google erhobenen Informationen werden unter Umständen an Dritte übertragen, sofern dies gesetzlich vorgeschrieben ist und/ oder soweit Dritte diese Daten im Auftrag von Google verarbeiten. Sie können der Setzung von Cookies für Anzeigenvorgaben dauerhaft widersprechen, indem Sie diese durch eine entsprechende Einstellung Ihrer Browser-Software verhindern oder das unter folgendem Link verfügbare Browser-Plug-in herunterladen und installieren:
                        <span class="page-content__block">
                            <a href="http://www.google.com/ads/preferences/html/intl/de/plugin/">http://www.google.com/ads/preferences/html/intl/de/plugin/</a>
                        </span>
                    </p>
                    <p>Bitte beachten Sie, dass bestimmte Funktionen dieser Website möglicherweise nicht oder nur eingeschränkt genutzt werden können, wenn Sie der Verwendung von Cookies widersprochen haben.</p>
                </div>
            </div>

            <div class="page-content__row">
                <div class="page-content__number">
                    <span class="page-content__number-label">7</span>
                </div>
                <div class="page-content__container">
                    <h6 class="page-content__title">Einsatz von Google AdWords Conversion-Tracking</h6>
                    <p>Diese Webseite nutzt das Online-Werbeprogramm „Google AdWords“ und im Rahmen von Google AdWords das Conversion-Tracking. Das Cookie für Conversion-Tracking wird gesetzt, wenn ein Nutzer auf eine von Google geschaltete Anzeige klickt. Bei Cookies handelt es sich um kleine Textdateien, die auf Ihrem Computersystem abgelegt werden. Diese Cookies verlieren nach 30 Tagen ihre Gültigkeit und dienen nicht der persönlichen Identifizierung. Besucht der Nutzer bestimmte Seiten dieser Website und das Cookie ist noch nicht abgelaufen, können Google und wir erkennen, dass der Nutzer auf die Anzeige geklickt hat und zu dieser Seite weitergeleitet wurde. Jeder Google AdWords-Kunde erhält ein anderes Cookie. Cookies können somit nicht über die Websites von AdWords-Kunden nachverfolgt werden. Die mithilfe des Conversion-Cookies eingeholten Informationen dienen dazu, Conversion-Statistiken für AdWords-Kunden zu erstellen, die sich für Conversion-Tracking entschieden haben. Die Kunden erfahren die Gesamtanzahl der Nutzer, die auf ihre Anzeige geklickt haben und zu einer mit einem Conversion-Tracking-Tag versehenen Seite weitergeleitet wurden. Sie erhalten jedoch keine Informationen, mit denen sich Nutzer persönlich identifizieren lassen. Wenn Sie nicht am Tracking teilnehmen möchten, können Sie dieser Nutzung widersprechen, indem Sie das Cookie des Google Conversion-Trackings über ihren Internet-Browser unter Nutzereinstellungen leicht deaktivieren. Sie werden sodann nicht in die Conversion-Tracking Statistiken aufgenommen.</p>
                    <p>
                        Unter der nachstehenden Internetadresse erhalten Sie weitere Informationen über die Datenschutzbestimmungen von Google:
                        <span class="page-content__block">
                            <a href="http://www.google.de/policies/privacy/">http://www.google.de/policies/privacy/</a>
                        </span>
                    </p>
                </div>
            </div>

            <div class="page-content__row">
                <div class="page-content__number">
                    <span class="page-content__number-label">8</span>
                </div>
                <div class="page-content__container">
                    <h6 class="page-content__title">Verwendung von Facebook-Plugins</h6>
                    <p>
                        Wir haben in unsere Internetpräsenz sogenannte Plugins des sozialen Netzwerkes facebook.com (nachfolgend „Facebook“) integriert. Facebook ist ein Unternehmen der Facebook Inc., 1601 S. California Ave, Palo Alto, CA 94304, USA. Eine Auflistung und das Aussehen dieser Plugins von Facebook kann unter der nachfolgenden Internetadresse eingesehen werden:
                        <a href="http://developers.facebook.com/plugins">http://developers.facebook.com/plugins</a>
                    </p>
                    <p>Bei jedem Aufruf einer Webseite unserer Internetpräsenz, die mit einem solchen Plugin versehen ist, veranlasst das Plugin, dass der von Ihnen verwendete Browser die optische Darstellung des Plugins vom Facebook-Server lädt und darstellt. Durch die Einbindung der Plugins erhält Facebook die Information, dass Ihr Browser die entsprechende Seite unseres Webauftritts aufgerufen hat, auch wenn Sie kein Facebook-Benutzerkonto besitzen oder gerade nicht bei Facebook eingeloggt sind. Diese Information (einschließlich Ihrer IP-Adresse) wird von Ihrem Browser direkt an einen Server von Facebook in den USA übermittelt und dort gespeichert.</p>
                    <p>
                        Wenn Sie Mitglied bei Facebook und während des Besuchs unserer Internetpräsenz bei Facebook eingeloggt sind, erkennt Facebook durch die von dem Plugin gesendete Information, welche bestimmte Webseite unserer Internetpräsenz Sie gerade besuchen und weist dies Ihrem persönlichen Benutzerkonto auf Facebook zu, unabhängig davon, ob Sie eines der Plugins betätigen. Betätigen Sie nun eines der Plugins, beispielsweise durch Anklicken des „Gefällt mir“-Buttons oder durch die Abgabe eines Kommentares, wird dies an Ihr persönliches Benutzerkonto auf Facebook gesendet und dort gespeichert. Um zu verhindern, dass Facebook die gesammelten Daten Ihrem Benutzerkonto auf Facebook zuordnet, müssen Sie sich bei Facebook ausloggen, bevor Sie unsere Internetpräsenz besuchen. Um die Datenerhebung und -weitergabe Ihrer Besucherdaten durch Facebook-Plugins für die Zukunft zu blocken, können Sie für einige Internetbrowser unter nachfolgendem Link ein Browser-Add-On von „Facebook Blocker“ beziehen, bitte löschen Sie das Browser-Add-On nicht, solange Sie die Facebook-Plugins blocken möchten:
                        <span class="page-content__block">
                            <a href="http://webgraph.com/resources/facebookblocker/">http://webgraph.com/resources/facebookblocker/</a>
                        </span>
                    </p>
                    <p>
                        Unter der nachstehenden Internetadresse finden Sie die Datenschutzhinweise von Facebook mit näheren Informationen zur Erhebung und Nutzung von Daten durch Facebook, zu Ihren diesbezüglichen Rechten sowie zu den Einstellungsmöglichkeiten zum Schutz Ihrer Privatsphäre:
                        <span class="page-content__block">
                            <a href="http://www.facebook.com/policy.php">http://www.facebook.com/policy.php</a>
                        </span>
                    </p>
                </div>
            </div>

            <div class="page-content__row">
                <div class="page-content__number">
                    <span class="page-content__number-label">9</span>
                </div>
                <div class="page-content__container">
                    <h6 class="page-content__title">Verwendung der Google „+1“-Schaltfläche</h6>
                    <p>Wir verwenden auf unserer Webseite die „+1“-Schaltfläche des sozialen Netzwerkes Google+ (Google Plus) der Google Inc., 1600 Amphitheatre Parkway, Mountain View, Kalifornien, 94043 USA, (nachfolgend „Google“).</p>
                    <p>Bei jedem Aufruf einer Webseite unserer Internetpräsenz, die mit einer „+1“-Schaltfläche versehen ist, veranlasst die „+1“-Schaltfläche, dass der von Ihnen verwendete Browser die optische Darstellung der „+1“-Schaltfläche vom Google-Server lädt und darstellt. Dabei wird dem Google-Server mitgeteilt, welche bestimmte Webseite unserer Internetpräsenz Sie gerade besuchen. Google protokolliert Ihren Browserverlauf beim Anzeigen einer „+1“-Schaltfläche für die Dauer von bis zu zwei Wochen zu Systemwartungs- und Fehlerbehebungszwecken. Eine weitergehende Auswertung Ihres Besuchs einer Webseite unserer Internetpräsenz mit einer „+1“-Schaltfläche erfolgt nicht.</p>
                    <p>Betätigen Sie die „+1“-Schaltfläche während Sie bei Google+ (Google Plus) eingeloggt sind, erfasst Google über Ihr Google-Profil Informationen über die von Ihnen empfohlene URL, Ihre IP-Adresse und andere browserbezogene Informationen, damit Ihre „+1“-Empfehlung gespeichert und öffentlich zugänglich gemacht werden kann. Ihre „+1“-Empfehlungen können als Hinweise zusammen mit Ihrem Profilnamen und Ihrem Foto in Google-Diensten, wie etwa in Suchergebnissen oder in Ihrem Google-Profil (als „+1“-Tab in Ihrem Google-Profil), oder an anderen Stellen auf Webseiten und Anzeigen im Internet eingeblendet werden.</p>
                    <p>
                        Unter der nachstehenden Internetadresse finden Sie die Datenschutzhinweise von Google zur „+1“-Schaltfläche mit näheren Informationen zur Erfassung, Weitergabe und Nutzung von Daten durch Google, zu Ihren diesbezüglichen Rechten sowie zu Ihren Profileinstellungsmöglichkeiten:
                        <span class="page-content__block">
                            <a href="https://www.google.com/intl/de/+/policy/+1button.html">https://www.google.com/intl/de/+/policy/+1button.html</a>
                        </span>
                    </p>
                </div>
            </div>

            <div class="page-content__row">
                <div class="page-content__number">
                    <span class="page-content__number-label">10</span>
                </div>
                <div class="page-content__container">
                    <h6 class="page-content__title">Informationen zu den Rechten des Kunden und Kontakte</h6>
                    <p>Sie haben ein Recht auf unentgeltliche Auskunft über Ihre gespeicherten Daten sowie ggf. ein Recht auf Berichtigung, Sperrung oder Löschung dieser Daten. Wenn Sie weitere Fragen zur Erhebung, Verarbeitung oder Nutzung Ihrer personenbezogenen Daten haben, kontaktieren Sie uns bitte. Gleiches gilt für Auskünfte, Sperrung, Löschungs- und Berichtigungswünsche hinsichtlich Ihrer personenbezogenen Daten sowie für Widerrufe erteilter Einwilligungen. Die Kontaktadresse finden Sie in unserem Impressum.</p>
                </div>
            </div>
        </div>
    </div>
</div>