<div class="col-sm-6 col-sm-offset-3">
	<div class="cart__empty">
		<h1 class="cart__empty__title">
			Shopping Cart
		</h1>
		<p class="cart__empty__text">
			Your Cart is empty
		</p>
		<i class="icon-cart-empty m-tb20"></i>
		<a href="" class="btn btn-invite">
			Continue Shopping
		</a>
	</div>
</div>