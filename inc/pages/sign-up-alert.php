<div class="row">
	<div class="col-md-6 col-md-offset-3">
		<div class="sign__block">
			<h4 class="sign__block__title">
				<span>Sign Up</span>
				Welcome to JUSTSHIRT
			</h4>
			<div class="sign__block__social">
				<a href="">
					<i class="icon-facebook m-r10"></i>
				</a>
				<a href="">
					<i class="icon-twitter m-r10"></i>
				</a>
				<a href="">
					<i class="icon-google"></i>
				</a>
			</div>
			<div class="block__or">
				<span class="block__or__text">or</span>
			</div>
			<div class="form-group has-error">
				<input type="text" class="form-control" placeholder="First name">
				<p class="error__text">
					Please enter First name
				</p>
			</div>
			<div class="form-group has-error">
				<input type="text" class="form-control" placeholder="Last name">
				<p class="error__text">
					Please enter Last name
				</p>
			</div>
			<div class="form-group has-error">
				<input type="text" class="form-control" placeholder="Email">
				<p class="error__text">
					Please enter Email address
				</p>
			</div>
			<div class="form-group has-error">
				<input type="text" class="form-control" placeholder="Password">
				<p class="error__text">
					Please enter Password
				</p>
			</div>
			<div class="form-group has-error">
				<input type="text" class="form-control" placeholder="Confirm password">
				<p class="error__text">
					Please confirm password
				</p>
			</div>
			<div class="row">
				<div class="col-xs-12">
					<label class="cb-checkbox">
						<input type="checkbox">
						Agree to <a href="" class="sign__block__link">Terms and Conditions</a>
					</label>
				</div>
			</div>
			<button class="sign__block__accept">
				Save
			</button>
			<p class="sign__block__text">
				Already have an account?
				<a href="" class="sign__block__link">
					Sign in
				</a>
			</p>
		</div>
	</div>
</div>