<ul class="breadcrumbs">
	<li class="breadcrumbs__item">
		<a href="" class="breadcrumbs__item__link">
			Shop
		</a>
	</li>
	<li class="breadcrumbs__item">
		<a href="" class="breadcrumbs__item__link">
			T-Shirts
		</a>
	</li>
	<li class="breadcrumbs__item">
		<a href="" class="breadcrumbs__item__link">
			Women's
		</a>
	</li>
</ul>
<div class="row">
	<div class="col-sm-3">
		<div class="sidebar">
			<h2 class="sidebar_title">
				Categories
			</h2>
			<ul class="sidebar__list">
				<li class="sidebar__list__item">
					<a href="" class="sidebar__list__item__link active">
						T-Shirts
					</a>
				</li>
				<li class="sidebar__list__item">
					<a href="" class="sidebar__list__item__link">
						Cups
					</a>
				</li>
				<li class="sidebar__list__item">
					<a href="" class="sidebar__list__item__link">
						Suspenders
					</a>
				</li>
				<li class="sidebar__list__item">
					<a href="" class="sidebar__list__item__link">
						Cup
					</a>
				</li>
				<li class="sidebar__list__item">
					<a href="" class="sidebar__list__item__link">
						Towel
					</a>
				</li>
				<li class="sidebar__list__item">
					<a href="" class="sidebar__list__item__link">
						Pillows
					</a>
				</li>
				<li class="sidebar__list__item">
					<a href="" class="sidebar__list__item__link">
						Belts
					</a>
				</li>
			</ul>
			<h2 class="sidebar_title">
				Categories: Women's
			</h2>
			<ul class="sidebar__list">
				<li class="sidebar__list__item">
					<a href="" class="sidebar__list__item__link">
						Men's
					</a>
				</li>
				<li class="sidebar__list__item">
					<a href="" class="sidebar__list__item__link active">
						Women's
					</a>
				</li>
				<li class="sidebar__list__item">
					<a href="" class="sidebar__list__item__link">
						Kids & Babies
					</a>
				</li>
			</ul>
			<h2 class="sidebar_title">
				Shop By
			</h2>			
			<div class="filter__block">
				<h2 class="sidebar_title">
					Filters
				</h2>
				<h3 class="filter__block__title">
					Price
				</h3>
				<div class="row">
					<div class="col-xs-12">
						<label class="cb-radio filter__block__label">
			                <input type="radio" name="filter-name">
			                $ 0.00 - $ 99.99 <span>(6)</span>
			            </label>
						<label class="cb-radio filter__block__label">
			                <input type="radio" name="filter-name">
			               	$ 100.00 and above <span>(5)</span>
			            </label>
					</div>
				</div>	

				<h3 class="filter__block__title">
					Manufacturer
				</h3>
				<div class="row">
					<div class="col-xs-12">
						<ul class="filter__list">
							<li class="filter__list__item">
								<a href="" class="filter__list__item__link active">
									The Brand <span>(3)</span>
								</a>
								<div class="filter__list__item__submenu">
									<label class="cb-checkbox filter__block__label w-100">
						                <input type="checkbox">
						                Versache
						            </label>
									<label class="cb-checkbox filter__block__label w-100">
						                <input type="checkbox">
						                D & G
						            </label>
								</div>
							</li>
							<li class="filter__list__item">
								<a href="" class="filter__list__item__link active">
									Company <span>(2)</span>
								</a>
								<div class="filter__list__item__submenu">
									<label class="cb-checkbox filter__block__label w-100">
						                <input type="checkbox">
						                Lorem
						            </label>
									<label class="cb-checkbox filter__block__label w-100">
						                <input type="checkbox">
						                Ipsum
						            </label>
								</div>
							</li>
							<li class="filter__list__item">
								<a href="" class="filter__list__item__link active">
									Logo Fashion <span>(1)</span>
								</a>
								<div class="filter__list__item__submenu">
									<label class="cb-checkbox filter__block__label w-100">
						                <input type="checkbox">
						                Lorem
						            </label>
								</div>
							</li>
						</ul>
					</div>
				</div>
				
				<h3 class="filter__block__title">
					Color
				</h3>
				<div class="row">
					<div class="col-xs-6">					
						<div class="color__block active">
							<div class="color__block__item">
								<div class="color__mark color__pink"></div>
							</div>
							<p class="color__block__count">(4)</p>
						</div>
					</div>
					<div class="col-xs-6">
						<div class="color__block">
							<div class="color__block__item">
								<div class="color__mark color__red"></div>
							</div>
							<p class="color__block__count">(4)</p>
						</div>
					</div>
					<div class="col-xs-6">
						<div class="color__block">
							<div class="color__block__item">
								<div class="color__mark color__orange"></div>
							</div>
							<p class="color__block__count">(6)</p>
						</div>
					</div>
					<div class="col-xs-6">
						<div class="color__block">
							<div class="color__block__item">
								<div class="color__mark color__green"></div>
							</div>
							<p class="color__block__count">(2)</p>
						</div>
					</div>
				</div>

				<h3 class="filter__block__title">
					Size
				</h3>
				<div class="row">
					<div class="col-xs-6">					
						<div class="size__block active">
							<div class="size__block__item">S</div>
							<p class="color__block__count">(5)</p>
						</div>
					</div>
					<div class="col-xs-6">					
						<div class="size__block">
							<div class="size__block__item">L</div>
							<p class="color__block__count">(6)</p>
						</div>
					</div>
					<div class="col-xs-6">					
						<div class="size__block">
							<div class="size__block__item">M</div>
							<p class="color__block__count">(6)</p>
						</div>
					</div>
					<div class="col-xs-6">					
						<div class="size__block">
							<div class="size__block__item">XL</div>
							<p class="color__block__count">(4)</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-sm-9">
		<h2 class="head__title">
			T-Shirts
		</h2>
		<div class="row">
			<div class="col-xs-12">
				<ul class="sorting__list">
					<li class="sorting__list__item">
						Items 1 to 8 of 11 total
					</li>
					<li class="sorting__list__item">
						Sort By 
						<select class="chosen-select">
							<option value="">Position</option>
							<option value="">Price</option>
						</select>
					</li>
					<li class="sorting__list__item">
						Show 
						<select class="chosen-select">
							<option value="">3</option>
							<option value="">5</option>
							<option value="">8</option>
							<option value="">10</option>
						</select>
						per page
					</li>
				</ul>
			</div>
		</div>	
		<div class="row catalog">
			<div class="col-sm-6 col-md-4" >
				<a href="" class="product__preview">
					<span class="product__preview__picture">
						<img src="images/products/shirt1.png" alt="" class="product__preview__avatar">
					</span>
					<span class="product__preview__content">
						<span class="product__preview__label label__sale" data-content="Lorem ipsum dolor sit amet"></span>
						<h5 class="product__preview__title">
							Sample Fashion Product Одинадцатиклассница
						</h5>						
						<ul class="product__preview__color_list">
							<li class="product__preview__color_list__item">
								<span class="color__block color__black"></span>
							</li>
							<li class="product__preview__color_list__item">
								<span class="color__block color__orange"></span>
							</li>
							<li class="product__preview__color_list__item">
								<span class="color__block color__green"></span>
							</li>
						</ul>
						<span class="product__preview__price">
							<span class="product__preview__price__active">
								$ 25.00
							</span>
						</span>
					</span>
				</a>				
			</div>
			<div class="col-sm-6 col-md-4">
				<a href="" class="product__preview">
					<span class="product__preview__picture">
						<img src="images/products/shirt2.png" alt="" class="product__preview__avatar">
					</span>
					<span class="product__preview__content">
						<span class="product__preview__label label__new" data-content="New"></span>
						<h5 class="product__preview__title">
							Sample Fashion Product
						</h5>
						<ul class="product__preview__color_list">
							<li class="product__preview__color_list__item">
								<span class="color__block color__black"></span>
							</li>
							<li class="product__preview__color_list__item">
								<span class="color__block color__orange"></span>
							</li>
							<li class="product__preview__color_list__item">
								<span class="color__block color__green"></span>
							</li>
						</ul>
						<span class="product__preview__price">
							<span class="product__preview__price__active">
								$ 15.00
								<span class="product__preview__price__old">$ 25.00</span>
							</span>
						</span>
					</span>
				</a>
			</div>
			<div class="col-sm-6 col-md-4">
				<a href="" class="product__preview">
					<span class="product__preview__picture">
						<img src="images/products/shirt3.png" alt="" class="product__preview__avatar">
					</span>
					<span class="product__preview__content">
						<span class="product__preview__label label__sale" data-content="Lorem ipsum dolor sit amet"></span>
						<h5 class="product__preview__title">
							Sample Fashion Product
						</h5>						
						<ul class="product__preview__color_list">
							<li class="product__preview__color_list__item">
								<span class="color__block color__black"></span>
							</li>
							<li class="product__preview__color_list__item">
								<span class="color__block color__orange"></span>
							</li>
							<li class="product__preview__color_list__item">
								<span class="color__block color__green"></span>
							</li>
						</ul>
						<span class="product__preview__price">
							<span class="product__preview__price__active">
								$ 30.00
							</span>
						</span>
					</span>
				</a>
			</div>
			<div class="col-sm-6 col-md-4">
				<a href="" class="product__preview">
					<span class="product__preview__picture">
						<img src="images/products/shirt1.png" alt="" class="product__preview__avatar">
					</span>
					<span class="product__preview__content">
						<span class="product__preview__label label__sale" data-content="Lorem ipsum"></span>
						<h5 class="product__preview__title">
							Sample Fashion Product
						</h5>
						<ul class="product__preview__color_list">
							<li class="product__preview__color_list__item">
								<span class="color__block color__black"></span>
							</li>
							<li class="product__preview__color_list__item">
								<span class="color__block color__orange"></span>
							</li>
							<li class="product__preview__color_list__item">
								<span class="color__block color__green"></span>
							</li>
						</ul>
						<span class="product__preview__price">
							<span class="product__preview__price__active">
								$ 25.00
							</span>
						</span>
					</span>
				</a>				
			</div>
			<div class="col-sm-6 col-md-4">
				<a href="" class="product__preview">
					<span class="product__preview__picture">
						<img src="images/products/shirt2.png" alt="" class="product__preview__avatar">
					</span>
					<span class="product__preview__content">
						<span class="product__preview__label label__new" data-content="New"></span>
						<h5 class="product__preview__title">
							Sample Fashion Product
						</h5>						
						<ul class="product__preview__color_list">
							<li class="product__preview__color_list__item">
								<span class="color__block color__black"></span>
							</li>
							<li class="product__preview__color_list__item">
								<span class="color__block color__orange"></span>
							</li>
							<li class="product__preview__color_list__item">
								<span class="color__block color__green"></span>
							</li>
						</ul>
						<span class="product__preview__price">
							<span class="product__preview__price__active">
								$ 15.00
								<span class="product__preview__price__old">$ 25.00</span>
							</span>
						</span>
					</span>
				</a>
			</div>
			<div class="col-sm-6 col-md-4">
				<a href="" class="product__preview">
					<span class="product__preview__picture">
						<img src="images/products/shirt3.png" alt="" class="product__preview__avatar">
					</span>
					<span class="product__preview__content">
						<span class="product__preview__label label__campaign" data-content="Campaign" data-campaign="11 days left"></span>
						<h5 class="product__preview__title">
							Sample Fashion Product
						</h5>						
						<ul class="product__preview__color_list">
							<li class="product__preview__color_list__item">
								<span class="color__block color__black"></span>
							</li>
							<li class="product__preview__color_list__item">
								<span class="color__block color__orange"></span>
							</li>
							<li class="product__preview__color_list__item">
								<span class="color__block color__green"></span>
							</li>
						</ul>
						<span class="product__preview__price">
							<span class="product__preview__price__active">
								$ 30.00
							</span>
						</span>
					</span>
				</a>
			</div>
		</div>
		<ul class="pagination">
	        <li class="pagination__item">
	            <a href="" class="pagination__item__link disabled">
	                «
	            </a>
	        </li>
	        <li class="pagination__item">
	            <a href="" class="pagination__item__link active">
	                1
	            </a>
	        </li>
	        <li class="pagination__item">
	            <a href="" class="pagination__item__link">
	                2
	            </a>
	        </li>
	        <li class="pagination__item">
	            <a href="" class="pagination__item__link">
	                3
	            </a>
	        </li>
	        <li class="pagination__item">
	            <a href="" class="pagination__item__link">
	              »
	            </a>
	        </li>
	    </ul>
	</div>
</div>