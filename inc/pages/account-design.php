<ul class="breadcrumbs">
	<li class="breadcrumbs__item">
		<a href="" class="breadcrumbs__item__link">
			My account
		</a>
	</li>
	<li class="breadcrumbs__item">
		<a href="" class="breadcrumbs__item__link">
			My designs
		</a>
	</li>
</ul>
<div class="row">
	<div class="col-sm-3">
		<div class="sidebar">			
			<h2 class="sidebar_title">
				My Account
			</h2>
			<ul class="sidebar__list">
				<li class="sidebar__list__item">
					<a href="" class="sidebar__list__item__link active">
						My Designs
					</a>
				</li>
				<li class="sidebar__list__item">
					<a href="" class="sidebar__list__item__link">
						My Purchases
					</a>
				</li>
				<li class="sidebar__list__item">
					<a href="" class="sidebar__list__item__link">
						My Sold Items
					</a>
				</li>
				<li class="sidebar__list__item">
					<a href="" class="sidebar__list__item__link">
						My Campaigns
					</a>
				</li>
				<li class="sidebar__list__item">
					<a href="" class="sidebar__list__item__link">
						Wishlist
					</a>
				</li>
				<li class="sidebar__list__item">
					<a href="" class="sidebar__list__item__link">
						Reviews
					</a>
				</li>
				<li class="sidebar__list__item">
					<a href="" class="sidebar__list__item__link">
						Invite & earn
					</a>
				</li>
				<li class="sidebar__list__item">
					<a href="" class="sidebar__list__item__link">
						Settings
					</a>
				</li>
			</ul>
		</div>
	</div>
	<div class="col-sm-9">
		<h2 class="head__title">
			My Designs
		</h2>
		<div class="row m-t10">
			<div class="pull-md-left p-rl15">
				<p class="text-page p-t15 m-b0">Sort By:</p>				
			</div>
			<div class="col-md-3 p-t10">
				<div class="form-group">
					<select class="form-control">
						<option value="">All categories</option>
						<option value="">Pig</option>
						<option value="">Raccoon</option>
					</select>
				</div>
			</div>
			<div class="col-md-4 col-lg-3">
				<button class="btn__accept m-0">
					Upload design
				</button>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<div class="b-Top m-t10">
					<div class="row">
						<div class="col-sm-6 col-md-4">
							<a href="" class="profile__design__link">
								<span class="profile__design">
									<button class="profile__design__remove">
										<i class="fa fa-pencil"></i>
									</button>
									<button class="profile__design__trash">
										<i class="fa fa-trash"></i>
									</button>
									<img src="images/profile/profile1.png" class="profile__design__picture">
									<span class="profile__design__price">
										$5.00
									</span>
								</span>
							</a>				
						</div>
						<div class="col-sm-6 col-md-4">
							<a href="" class="profile__design__link">
								<span class="profile__design">
									<button class="profile__design__remove">
										<i class="fa fa-pencil"></i>
									</button>
									<button class="profile__design__trash">
										<i class="fa fa-trash"></i>
									</button>
									<img src="images/profile/profile2.png" class="profile__design__picture">
									<span class="profile__design__price">
										$5.00
									</span>
								</span>
							</a>				
						</div>
						<div class="col-sm-6 col-md-4">
							<a href="" class="profile__design__link">
								<span class="profile__design">
									<button class="profile__design__remove">
										<i class="fa fa-pencil"></i>
									</button>
									<button class="profile__design__trash">
										<i class="fa fa-trash"></i>
									</button>
									<img src="images/profile/profile3.png" class="profile__design__picture">
									<span class="profile__design__price">
										Free
									</span>
								</span>
							</a>				
						</div>
						<div class="col-sm-6 col-md-4">
							<a href="" class="profile__design__link">
								<span class="profile__design">
									<button class="profile__design__remove">
										<i class="fa fa-pencil"></i>
									</button>
									<button class="profile__design__trash">
										<i class="fa fa-trash"></i>
									</button>
									<img src="images/profile/profile1.png" class="profile__design__picture">
									<span class="profile__design__price">
										$5.00
									</span>
								</span>
							</a>				
						</div>
						<div class="col-sm-6 col-md-4">
							<a href="" class="profile__design__link">
								<span class="profile__design">
									<button class="profile__design__remove">
										<i class="fa fa-pencil"></i>
									</button>
									<button class="profile__design__trash">
										<i class="fa fa-trash"></i>
									</button>
									<img src="images/profile/profile2.png" class="profile__design__picture">
									<span class="profile__design__price">
										$5.00
									</span>
								</span>
							</a>				
						</div>
						<div class="col-sm-6 col-md-4">
							<a href="" class="profile__design__link">
								<span class="profile__design">
									<button class="profile__design__remove">
										<i class="fa fa-pencil"></i>
									</button>
									<button class="profile__design__trash">
										<i class="fa fa-trash"></i>
									</button>
									<img src="images/profile/profile3.png" class="profile__design__picture">
									<span class="profile__design__price">
										Free
									</span>
								</span>
							</a>				
						</div>
						<div class="col-sm-6 col-md-4">
							<a href="" class="profile__design__link">
								<span class="profile__design">
									<button class="profile__design__remove">
										<i class="fa fa-pencil"></i>
									</button>
									<button class="profile__design__trash">
										<i class="fa fa-trash"></i>
									</button>
									<img src="images/profile/profile1.png" class="profile__design__picture">
									<span class="profile__design__price">
										$5.00
									</span>
								</span>
							</a>				
						</div>
						<div class="col-sm-6 col-md-4">
							<a href="" class="profile__design__link">
								<span class="profile__design">
									<button class="profile__design__remove">
										<i class="fa fa-pencil"></i>
									</button>
									<button class="profile__design__trash">
										<i class="fa fa-trash"></i>
									</button>
									<img src="images/profile/profile2.png" class="profile__design__picture">
									<span class="profile__design__price">
										$5.00
									</span>
								</span>
							</a>				
						</div>
						<div class="col-sm-6 col-md-4">
							<a href="" class="profile__design__link">
								<span class="profile__design">
									<button class="profile__design__remove">
										<i class="fa fa-pencil"></i>
									</button>
									<button class="profile__design__trash">
										<i class="fa fa-trash"></i>
									</button>
									<img src="images/profile/profile3.png" class="profile__design__picture">
									<span class="profile__design__price">
										Free
									</span>
								</span>
							</a>				
						</div>
						<div class="col-sm-6 col-md-4">
							<a href="" class="profile__design__link">
								<span class="profile__design">
									<button class="profile__design__remove">
										<i class="fa fa-pencil"></i>
									</button>
									<button class="profile__design__trash">
										<i class="fa fa-trash"></i>
									</button>
									<img src="images/profile/profile1.png" class="profile__design__picture">
									<span class="profile__design__price">
										$5.00
									</span>
								</span>
							</a>				
						</div>
						<div class="col-sm-6 col-md-4">
							<a href="" class="profile__design__link">
								<span class="profile__design">
									<button class="profile__design__remove">
										<i class="fa fa-pencil"></i>
									</button>
									<button class="profile__design__trash">
										<i class="fa fa-trash"></i>
									</button>
									<img src="images/profile/profile2.png" class="profile__design__picture">
									<span class="profile__design__price">
										$5.00
									</span>
								</span>
							</a>				
						</div>
						<div class="col-sm-6 col-md-4">
							<a href="" class="profile__design__link">
								<span class="profile__design">
									<button class="profile__design__remove">
										<i class="fa fa-pencil"></i>
									</button>
									<button class="profile__design__trash">
										<i class="fa fa-trash"></i>
									</button>
									<img src="images/profile/profile3.png" class="profile__design__picture">
									<span class="profile__design__price">
										Free
									</span>
								</span>
							</a>				
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<ul class="pagination">
			        <li class="pagination__item">
			            <a href="" class="pagination__item__link disabled">
			                «
			            </a>
			        </li>
			        <li class="pagination__item">
			            <a href="" class="pagination__item__link active">
			                1
			            </a>
			        </li>
			        <li class="pagination__item">
			            <a href="" class="pagination__item__link">
			                2
			            </a>
			        </li>
			        <li class="pagination__item">
			            <a href="" class="pagination__item__link">
			                3
			            </a>
			        </li>
			        <li class="pagination__item">
			            <a href="" class="pagination__item__link">
			              »
			            </a>
			        </li>
			    </ul>
			</div>
		</div>		
	</div>
</div>