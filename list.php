<?php
	$_GET['id'] = isset($_GET['id']) ? $_GET['id'] : 'catalog';
	$id = $_GET['id'];
?>
<?php require_once 'inc/config/all.php'; ?>
<?php require_once 'inc/modules/header.php';?>
<div class="container p-t40">
	<h2 class="head__title text-center">
		Xeira's list page
	</h2>
	<div class="row">
		<div class="col-xs-12">
			<a href="guidline.php">Guidline</a>
		</div>
	</div>
	<div class="row">
		<ul class="col-xs-12 m-t20">
			<?php 				
				$dir = opendir("inc/pages/"); 
				if ($dir) {
					while (false !== ($file = readdir($dir))) {
						if ($file != "." && $file != "..") {
							$file = preg_replace("/^(.*)\.[\w]{1,3}$/", "$1", $file); 

							echo "
								<li class='col-sm-2 m-b20' style='display:inline-block; text-transform:capitalize'>
									<a href=/index.php?id=".$file.">".$file."</a>
								</li>
							";
						}
					}
					closedir($dir); 
				}
			?>
		</ul>
	</div>
</div>