<div class="row">
	<div class="col-xs-12">
		<div class="thank__block">
			<i class="icon-thank"></i>			
			<h2 class="thank__block__title">
				Thanks for your order
			</h2>
			<p class="thank__block__text">
				Your order is being processed and we will send you status update message soon. 
			</p>
			<p class="thank__block__red-text">
				Invite your friends and earn money!
			</p>
			<button class="btn btn__default">
				Invite Friends
			</button>
			<p class="product__share__text">
				Share:								
			</p>
			<a href=""><i class="icon-facebook-gray"></i></a>
			<a href=""><i class="icon-twitter-gray"></i></a>
			<a href=""><i class="icon-google-gray"></i></a>			
		</div>
	</div>
</div>