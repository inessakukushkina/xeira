<div class="row">
	<div class="col-md-6 col-md-offset-3">
		<div class="sign__block">
			<h4 class="sign__block__title">
				<span>Forgot Password</span>
			</h4>
			<div class="form-group">
				<input type="text" class="form-control" placeholder="Email address">
			</div>
			<a href="" class="sign__block__accept">
				Save
			</a>
			<p class="sign__block__text">
				<a href="" class="sign__block__link">
					<i class="fa fa-long-arrow-left"></i> Back to Sign In
				</a>
			</p>
		</div>
	</div>
</div>