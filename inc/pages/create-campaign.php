<ul class="breadcrumbs">
	<li class="breadcrumbs__item">
		<a href="" class="breadcrumbs__item__link">
			Create
		</a>
	</li>
	<li class="breadcrumbs__item">
		<a href="" class="breadcrumbs__item__link">
			T-Shirts
		</a>
	</li>
</ul>
<div class="configurator__steps">
	<div class="row">
		<div class="col-xs-12">
			<h2 class="head__title">
				Create your campaign
			</h2>
		</div>
	</div>
	<ol class="configurator__list">
		<li class="configurator__list__item active" data-list>
			Create your design
		</li>
		<li class="configurator__list__item" data-list>
			Set a goal
		</li>
		<li class="configurator__list__item" data-list>
			Add a description
		</li>
	</ol>
</div>
<div class="is-relative block__upload">
	<ul class="configurator__aside clearfix">
		<li class="configurator__aside__item">
			<a href="" class="configurator__aside__item__link">
				<i class="icon-gray-shirt"></i>
			</a>
		</li>
		<li class="configurator__aside__item">
			<a href="" class="configurator__aside__item__link">
				<i class="icon-gray-cups"></i>
			</a>
		</li>
		<li class="configurator__aside__item">
			<a href="" class="configurator__aside__item__link">
				<i class="icon-gray-suspenders"></i>
			</a>
		</li>
		<li class="configurator__aside__item">
			<a href="" class="configurator__aside__item__link">
				<i class="icon-gray-cup"></i>
			</a>
		</li>
		<li class="configurator__aside__item">
			<a href="" class="configurator__aside__item__link">
				<i class="icon-gray-towel"></i>
			</a>
		</li>
		<li class="configurator__aside__item">
			<a href="" class="configurator__aside__item__link">
				<i class="icon-gray-pillo"></i>
			</a>
		</li>
		<li class="configurator__aside__item">
			<a href="" class="configurator__aside__item__link">
				<i class="icon-gray-belts"></i>
			</a>
		</li>
	</ul>
	<div class="configurator__settings">
		<div class="configurator__settings__item active" data-accordeon>
			<h3 class="configurator__settings__title">
				Style & design
			</h3>
			<div class="configurator__settings__item__body" data-accordeon-body>
				<p class="text-lighten">
					Choose style & design
				</p>
				<div class="form-group">
					<select class="chosen-select">
						<option value=""></option>
						<option value="">Women's Tees</option>
					</select>
				</div>			
				<div class="clearfix">
					<div class="configurator__settings__filter">
						<p class="text-lighten">
							Select size
						</p>
						<select class="chosen-select">
							<option value="">All sizes</option>
							<option value="">S</option>
							<option value="">M</option>
							<option value="">L</option>
						</select>
					</div>
					<div class="configurator__settings__filter">
						<p class="text-lighten">
							Print type
						</p>
						<select class="chosen-select">
							<option value="">All</option>
							<option value="">Trees</option>
							<option value="">Plants</option>
						</select>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12">
						<div class="configurator__create__settings">
							<div class="configurator__create__settings__item clearfix" data-popover>
								<div class="configurator__create__settings__item__content">
									<div class="popover popover__left" data-toggle-popover>
										<div class="popover__block">
											<div style="background-color:red;" class="popover__block__color"></div>
										</div>
										<div class="popover__block">
											<div style="background-color:pink;" class="popover__block__color"></div>
										</div>
										<div class="popover__block">
											<div style="background-color:orange;" class="popover__block__color"></div>
										</div>
										<div class="popover__block">
											<div style="background-color:green;" class="popover__block__color color__green"></div>
										</div>
									</div>
									<i class="icon-long-shirt"></i>
									<h3 class="configurator__create__settings__item__title">
										Teespring Women’s
									</h3>
									<p class="configurator__create__settings__item__text">
										Premium materials XS - 4XL
									</p>
								</div>							
							</div>
							<div class="configurator__create__settings__item clearfix" data-popover>
								<div class="configurator__create__settings__item__content">
									<div class="popover popover__left" data-toggle-popover>
										<div class="popover__color">
											<div class="popover__block">
												<div style="background-color:red;" class="popover__block__color"></div>
											</div>
											<div class="popover__block">
												<div style="background-color:pink;" class="popover__block__color"></div>
											</div>
											<div class="popover__block">
												<div style="background-color:orange;" class="popover__block__color"></div>
											</div>
											<div class="popover__block">
												<div style="background-color:green;" class="popover__block__color"></div>
											</div>
											<div class="popover__block">
												<div style="background-color:red;" class="popover__block__color"></div>
											</div>
											<div class="popover__block">
												<div style="background-color:pink;" class="popover__block__color"></div>
											</div>
											<div class="popover__block">
												<div style="background-color:orange;" class="popover__block__color"></div>
											</div>
										</div>									
										<div class="popover__block">
											<div style="background-color:green;" class="popover__block__color"></div>
										</div>
									</div>
									<i class="icon-middle-shirt"></i>
									<h3 class="configurator__create__settings__item__title">
										American Apparel Women's Fitted Tee
									</h3>
									<p class="configurator__create__settings__item__text">
										Top of the line S (4) - 2 XL (18-20)
									</p>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-xs-12">
						<div class="configurator__create__settings">
							<div for="chose-shirt3" class="configurator__create__settings__item clearfix" data-popover>
								<div class="configurator__create__settings__item__content">
									<div class="popover popover__left" data-toggle-popover>
										<div class="popover__block">
											<div class="popover__block__color color__red"></div>
										</div>
										<div class="popover__block">
											<div class="popover__block__color color__pink"></div>
										</div>
										<div class="popover__block">
											<div class="popover__block__color color__orange"></div>
										</div>
									</div>
									<i class="icon-middle-shirt"></i>
									<h3 class="configurator__create__settings__item__title">
										Canvas Triblend Tee
									</h3>
									<p class="configurator__create__settings__item__text">
										Premium materials S – 2XL
									</p>
								</div>					
							</div>
							<div for="chose-shirt4" class="configurator__create__settings__item clearfix" data-popover>
								<div class="configurator__create__settings__item__content">
									<div class="popover popover__left" data-toggle-popover>
										<div class="popover__block">
											<div class="popover__block__color color__red"></div>
										</div>
									</div>
									<i class="icon-small-shirt"></i>
									<h3 class="configurator__create__settings__item__title">
										Teespring Women’s
									</h3>	
									<p class="configurator__create__settings__item__text">
										Premium materials XS - 4XL
									</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="configurator__settings__item" data-accordeon>
			<h3 class="configurator__settings__title" >
				Text
			</h3>
			<div class="configurator__settings__item__body" data-accordeon-body>
				<p class="text-lighten">
					Enter text below
				</p>
				<div class="row">
					<div class="col-xs-12">
						<div class="form-group">
							<input type="text" class="form-control">
						</div>
					</div>
				</div>
				<p class="text-lighten">
					Choose a font
				</p>
				<div class="row">
					<div class="col-xs-8">
						<div class="form-group">
							<select class="chosen-select">
								<option value="">Comic Sans</option>
								<option value="">Ubuntu</option>
								<option value="">Lato</option>
								<option value="">Open Sans</option>
							</select>
						</div>
					</div>
					<div class="col-xs-4">
						<div class="form-group">
							<div class="input-group color">
								<input type="text" value="" class="form-control" data-color />
								<span class="input-group-addon">
									<span class="input-group-addon_content">
										<i></i>
									</span>							
								</span>
							</div>
						</div>
					</div>
				</div>
				<p class="text-lighten">
					Add on outline
				</p>
				<div class="row">
					<div class="col-xs-8">
						<div class="form-group">
							<select class="chosen-select">
								<option value="">No outline</option>
								<option value="">Ubuntu</option>
								<option value="">Lato</option>
								<option value="">Open Sans</option>
							</select>
						</div>					
					</div>
					<div class="col-xs-4">
						<div class="form-group">
							<div class="input-group color">
								<input type="text" value="" class="form-control" data-color />
								<span class="input-group-addon">
									<span class="input-group-addon_content">
										<i></i>
									</span>							
								</span>
							</div>
						</div>
					</div>
				</div>
				<label class="cb-checkbox cb-gray m-t15">
					<input type="checkbox">
					Snap to center
				</label>
				<div class="row">
					<div class="col-xs-12 p-tb10">
						<button class="btn-change-element">
							<i class="icon-double"></i>
						</button>
						<button class="btn-change-element">
							<i class="icon-center"></i>
						</button>
						<button class="btn-change-element">
							<i class="icon-front"></i>
						</button>
						<button class="btn-change-element">
							<i class="icon-horizontal"></i>
						</button>
						<button class="btn-change-element">
							<i class="icon-vertical"></i>
						</button>
					</div>
				</div>			
			</div>
		</div>
		<div class="configurator__settings__item" data-accordeon>
			<h3 class="configurator__settings__title">
				Art
			</h3>
			<div class="configurator__settings__item__body" data-accordeon-body>
				<div class="p-tb110 text-center">
					<button class="btn-browse">
						Browse Artwork
					</button>	
					<div class="block__or">
						<span class="block__or__text">or</span>
					</div>
					<button class="btn-browse">
						Upload your own
					</button>
				</div>
			</div>
		</div>
		<div class="row m-tb20">
			<div class="col-sm-6">
				<p class="cost__text">Base cost @ 50 shirts</p>
				<p class="cost">$7.14</p>
			</div>
			<div class="col-sm-6">
				<button class="btn__accept" data-forward>
					Continue
				</button>
			</div>
		</div>
	</div>

	<div class="configurator__content">
		<img class="configurator__content__picture" src="images/configurator/shirt.png" alt="">
		<button class="btn__primary">
			Front
		</button>
		<button class="btn__primary">
			Back
		</button>
	</div>
</div>

<div class="block__upload is-hidden">
	<div class="row">
		<div class="col-sm-6">
			<div class="configurator__body">
				<h3 class="configurator__body__title">
					Sales goal
				</h3>
				<div class="row">
					<div class="col-xs-12">
						<div id="slider" class="configurator__body__slider m-tb20">
							<div class="configurator__body__slider__content" data-slider></div>		
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-4 col-sm-6">
						<div class="input-group slider__value">
							<span class="input-group-addon"># of shirts</span>
							<input id="slider-count" type="text" class="form-control text-center">	
						</div>
					</div>
					<div class="col-lg-8 col-sm-6">
						<p class="title_text">
							Your goal is the number of shirts you’re aiming to sell, but we’ll print your campaign as long as you sell enough to generate a profit
						</p>
					</div>
				</div>
				<h3 class="configurator__body__title">
					Estimated profit
				</h3>
				<h1 class="pink_text text-bold">$801+</h1>
				<h3 class="configurator__body__title">
					Style & design
				</h3>
				<div class="configurator__body__product clearfix">
					<div class="configurator__body__product__picture">
						<img src="images/products/shirt1.png" alt="">
					</div>
					<div class="configurator__body__product__content">
						<h6 class="configurator__body__product__content__title">
							Teespring Premium Tee
						</h6>
						<p class="configurator__body__product__content__text">
							$13.36 profit/sale
						</p>
						<div class="configurator__color">
							<div class="color__block color__black"></div>
						</div>
						<div class="configurator__color">
							<div class="color__block color__green"></div>
						</div>
						<div class="configurator__color">
							<div class="color__block color__orange"></div>
						</div>
						<div class="configurator__color color__empty"></div>
						<div class="configurator__color color__empty"></div>
						<div class="configurator__color color__empty"></div>
						<p class="configurator__body__product__price">
							22.00
						</p>
					</div>					
				</div>
				<p class="text-muted segoe__family m-t20 p-b10 b-Bottom">
					You can add 10 more product options.
				</p>
				<div class="row is-hide add-style">
					<div class="col-sm-3">
						<div class="form-group">
							<select type="text" class="form-control">
								<option value="">V-Neck Tees</option>
								<option value="">Tees</option>
							</select>
						</div>
					</div>
					<div class="col-sm-6">
						<div class="form-group">
							<select type="text" class="form-control">
								<option value="">Canvas Ringspun V-Neck</option>
								<option value="">V-Neck</option>
							</select>
						</div>
					</div>
					<div class="col-sm-3">
						<button class="btn__default btn-small w-100">
							Add
						</button>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12">
						<a href="" class="link__page-16" data-toggle-block=".add-style">
							<i class="icon-circle-plus m-r5"></i> Add style
						</a>
					</div>
				</div>
				<p class="title_text segoe__family m-t20">
					Optimize your campaign by adding an additional style
				</p>
			</div>
			<div class="row">
				<div class="col-xs-12">
					<button class="btn__primary m-r20" data-back="">
						Back
					</button>
					<button class="btn__default" data-forward="">
						Continue
					</button>
				</div>
			</div>			
		</div>
		<div class="col-sm-6 text-center p-t20">
			<img src="images/configurator/shirt.png" alt="">
			<button class="btn__primary">
				Front
			</button>
			<button class="btn__primary">
				Back
			</button>
		</div>
	</div>
</div>

<div class="block__upload is-hidden">
	<div class="row">
		<div class="col-sm-6">
			<div class="configurator__body">
				<h3 class="configurator__body__title">
					Details
				</h3>
				<p class="form-label">
					Campaign title
				</p>
				<div class="form-group">
					<input type="text" class="form-control">
				</div>
				<p class="form-label">
					Description
				</p>
				<div class="form-group">
					<textarea id="description" class="form-control" rows="10"></textarea>
				</div>
				<div class="row">
					<div class="col-xs-12">
						<a href="" class="link__page-16" >
							<i class="icon-circle-plus m-r5"></i> Tag your company
						</a>
					</div>
				</div>
				<p class="form-label m-t20">
					Duration
				</p>
				<div class="form-group">
					<select type="text" class="form-control">
						<option value="">7 Days (Ending Wednesday, Augest 5, 2015)</option>
						<option value="">Ending Wednesday</option>
					</select>
					<p class="title_text p-t5">
						<small>
							US orders will arrive 10-14 days after the end of the campaign. 
							<a href="" class="link__page-12" >
								Learn more
							</a>
						</small>
					</p>
				</div>				
				<p class="form-label">
					URL
				</p>
				<div class="input-group">
			      	<span class="input-group-addon">justshirt.com/</span>
			      	<input type="text" class="form-control" placeholder="Username">
			    </div>
			    <h3 class="configurator__body__title">
					Shipping options
				</h3>
				<label class="cb-checkbox w-100">
					<input type="checkbox">
					Allow buyers to pick up their orders from you
				</label>
				<p class="title_text">
					<small>
						Please note pick-up is currently only available to US Addresses.
					</small>
				</p>
				<p class="form-label m-t20">
					Where/when will buyers be able to pick up their shirts?
				</p>
				<div class="form-group">
					<textarea rows="5" class="form-control"></textarea>
					<p class="title_text p-t5">
						<small>
							Buyers will see these instructions when choosing between shipping/pick-up
						</small>
					</p>
				</div>
				<div class="row">
					<div class="col-sm-6">
						<p class="form-label">
							First name
						</p>
						<div class="form-group">
							<input type="text" class="form-control">
						</div>
					</div>
					<div class="col-sm-6">
						<p class="form-label">
							Last name
						</p>
						<div class="form-group">
							<input type="text" class="form-control">
						</div>
					</div>
				</div>	
				<div class="row">
					<div class="col-sm-8">
						<p class="form-label">
							Contry
						</p>
						<div class="form-group">
							<select type="text" class="form-control">
								<option value="">USA</option>
								<option value="">Belarus</option>
							</select>
						</div>
					</div>
					<div class="col-sm-4">
						<p class="form-label">
							Zipcode
						</p>
						<div class="form-group">
							<input type="text" class="form-control">
						</div>					
					</div>
				</div>
				<div class="row">
					<div class="col-sm-6">
						<p class="form-label">
							State
						</p>
						<div class="form-group">
							<select type="text" class="form-control">
								<option value="">USA</option>
								<option value="">Belarus</option>
							</select>
						</div>
					</div>
					<div class="col-sm-6">
						<p class="form-label">
							City
						</p>
						<div class="form-group">
							<input type="text" class="form-control">
						</div>					
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12">
						<p class="form-label">
							Address
						</p>
						<div class="form-group">
							<input type="text" class="form-control">
						</div>
					</div>
				</div>
				 <h3 class="configurator__body__title">
					Display Options
				</h3>
				<label class="cb-checkbox w-100">
					<input type="checkbox">
					Show the back side by default
				</label>
			</div>
			<div class="row">
				<div class="col-xs-12">
					<button class="btn__primary m-r20" data-back="">
						Back
					</button>
					<button class="btn__default">
						Launch
					</button>
				</div>
			</div>
		</div>
		<div class="col-sm-6 text-center p-t20">
			<img src="images/configurator/shirt.png" alt="">
			<button class="btn__primary">
				Front
			</button>
			<button class="btn__primary">
				Back
			</button>
		</div>
	</div>
</div>