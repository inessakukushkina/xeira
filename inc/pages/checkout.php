<div class="checkout">
	<div class="row">
		<div class="col-md-8">
			<h2 class="head__title p-t20">
				Checkout
			</h2>
		</div>
	</div>
	<div class="row">
		<div class="col-md-8">		
			<div class="checkout__body">
				<h3 class="checkout__body__title">
					Checkout methods
				</h3>
				<div class="row">
					<div class="col-sm-3">
						<label class="cb-radio w-100">
			                <input type="radio" name="group-name">
			                Guest
			            </label>
					</div>
					<div class="col-sm-3">
						<label class="cb-radio w-100">
			                <input type="radio" name="group-name">
			               	Register
			            </label>
					</div>
					<div class="col-sm-6">
						<label class="cb-radio w-100">
			                <input type="radio" name="group-name">
			                I have an account
			            </label>
					</div>
				</div>
				<h3 class="checkout__body__title">
					Products
				</h3>
				<table class="checkout__products">
					<thead>
						<tr>
							<th></th>
							<th>Product Name</th>
							<th>QTY</th>
							<th>Total</th>
							<th></th>
						</tr>
					</thead>
					<tbody>
						<tr class="clear__block">
							<td data-th="Product Image:" class="checkout__product__image">
								<div class="product__preview">
									<div class="product__preview__picture">
										<img src="images/products/shirt1.png" alt="" class="product__preview__avatar">
									</div>
								</div>
							</td>
							<td data-th="Product Name:" class="checkout__product__name">
								<h2 class="checkout__products__title">
									Product With Custom Options
								</h2>
								<ul class="checkout__products__list">
									<li class="checkout__products__list__item">
										T-Shirt Size <span>L</span>
									</li>
									<li class="checkout__products__list__item">
										T-Shirt Color <span>Pink</span>
									</li>
									<li class="checkout__products__list__item">
										Additional Options <br>
										<span>Embroidered logo to the chest</span>
									</li>
								</ul>
							</td>
							<td data-th="QTY:" class="checkout__product__qty">
								1
							</td>
							<td data-th="Total:" class="checkout__product__total">
								$22.00
							</td>
							<td data-th="Remove product:" class="checkout__product__remove">
								<a href="" class="remove__checkout">x</a>
							</td>
						</tr>
						<tr class="clear__block">
							<td data-th="Product Image:" class="checkout__product__image">
								<div class="product__preview">
									<div class="product__preview__picture">
										<img src="images/products/shirt2.png" alt="" class="product__preview__avatar">
									</div>
								</div>
							</td>
							<td data-th="Product Name:" class="checkout__product__name">
								<h2 class="checkout__products__title">
									Product With Custom Options
								</h2>
								<ul class="checkout__products__list">
									<li class="checkout__products__list__item">
										T-Shirt Size <span>L</span>
									</li>
									<li class="checkout__products__list__item">
										T-Shirt Color <span>Pink</span>
									</li>
									<li class="checkout__products__list__item">
										Additional Options <br>
										<span>Embroidered logo to the chest</span>
									</li>
								</ul>
							</td>
							<td data-th="QTY:" class="checkout__product__qty">
								1
							</td>
							<td data-th="Total:" class="checkout__product__total">
								$22.00
							</td>
							<td data-th="Remove product:" class="checkout__product__remove">
								<a href="" class="remove__checkout">x</a>
							</td>
						</tr>
						<tr class="clear__block">
							<td data-th="Product Image:" class="checkout__product__image">
								<div class="product__preview">
									<div class="product__preview__picture">
										<img src="images/products/shirt3.png" alt="" class="product__preview__avatar">
									</div>
								</div>
							</td>
							<td data-th="Product Name:" class="checkout__product__name">
								<h2 class="checkout__products__title">
									Product With Custom Options
								</h2>
								<ul class="checkout__products__list">
									<li class="checkout__products__list__item">
										T-Shirt Size <span>L</span>
									</li>
									<li class="checkout__products__list__item">
										T-Shirt Color <span>Pink</span>
									</li>
									<li class="checkout__products__list__item">
										Additional Options <br>
										<span>Embroidered logo to the chest</span>
									</li>
								</ul>
							</td>
							<td data-th="QTY:" class="checkout__product__qty">
								1
							</td>
							<td data-th="Total:" class="checkout__product__total">
								$22.00
							</td>
							<td data-th="Remove product:" class="checkout__product__remove">
								<a href="" class="remove__checkout">x</a>
							</td>
						</tr>
					</tbody>
				</table>
				<h3 class="checkout__body__title">
					Delivery method
				</h3>
				<div class="row">
					<div class="col-sm-8 col-md-6">
						<select class="chosen-select">
			                <option value="">Fedex 1 - 5 days - $15.98</option>
			                <option value="">DHL 1 - 3 days - $31.01</option>
			            </select>
					</div>
				</div>
				<h3 class="checkout__body__title">
					Shipping Info
				</h3>
				<div class="row">
					<div class="col-sm-6">
						<p class="form-label">
							First name
						</p>
						<div class="form-group">
							<input type="text" class="form-control">
						</div>
					</div>
					<div class="col-sm-6">
						<p class="form-label">
							Last name
						</p>
						<div class="form-group">
							<input type="text" class="form-control">
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-6">
						<p class="form-label">
							Phone number
						</p>
						<div class="form-group">
							<input type="text" class="form-control">
						</div>
					</div>
					<div class="col-sm-6">
						<p class="form-label">
							E-mail
						</p>
						<div class="form-group">
							<input type="text" class="form-control">
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-6">
						<p class="form-label">
							Country
						</p>
						<div class="form-group">
							<select name="billingCountry" class="form-control select2-hidden-accessible parsley-success" data-parsley-required="true" data-parsley-trigger="change" tabindex="-1" aria-hidden="true" data-parsley-id="14">
								<option value="">Select country</option>
								<option value="AF" data-geoid="1149361">Afghanistan</option>
								<option value="AL" data-geoid="783754">Albania</option>
								<option value="DZ" data-geoid="2589581">Algeria</option>
								<option value="AS" data-geoid="5880801">American Samoa</option>
								<option value="AD" data-geoid="3041565">Andorra</option>
								<option value="AO" data-geoid="3351879">Angola</option>
								<option value="AI" data-geoid="3573511">Anguilla</option>
								<option value="AQ" data-geoid="6697173">Antarctica</option>
								<option value="AG" data-geoid="3576396">Antigua and Barbuda</option>
								<option value="AR" data-geoid="3865483">Argentina</option>
								<option value="AM" data-geoid="174982">Armenia</option>
								<option value="AW" data-geoid="3577279">Aruba</option>
								<option value="AU" data-geoid="2077456">Australia</option>
								<option value="AT" data-geoid="2782113">Austria</option>
								<option value="AZ" data-geoid="587116">Azerbaijan</option>
								<option value="BS" data-geoid="3572887">Bahamas</option>
								<option value="BH" data-geoid="290291">Bahrain</option>
								<option value="BD" data-geoid="1210997">Bangladesh</option>
								<option value="BB" data-geoid="3374084">Barbados</option>
								<option value="BY" data-geoid="630336">Belarus</option>
								<option value="BE" data-geoid="2802361">Belgium</option>
								<option value="BZ" data-geoid="3582678">Belize</option>
								<option value="BJ" data-geoid="2395170">Benin</option>
								<option value="BM" data-geoid="3573345">Bermuda</option>
								<option value="BT" data-geoid="1252634">Bhutan</option>
								<option value="BO" data-geoid="3923057">Bolivia</option>
								<option value="BQ" data-geoid="7626844">Bonaire</option>
								<option value="BA" data-geoid="3277605">Bosnia and Herzegovina</option>
								<option value="BW" data-geoid="933860">Botswana</option>
								<option value="BV" data-geoid="3371123">Bouvet Island</option>
								<option value="BR" data-geoid="3469034">Brazil</option>
								<option value="IO" data-geoid="1282588">British Indian Ocean Territory</option>
								<option value="VG" data-geoid="3577718">British Virgin Islands</option>
								<option value="BN" data-geoid="1820814">Brunei</option>
								<option value="BG" data-geoid="732800">Bulgaria</option>
								<option value="BF" data-geoid="2361809">Burkina Faso</option>
								<option value="BI" data-geoid="433561">Burundi</option>
								<option value="KH" data-geoid="1831722">Cambodia</option>
								<option value="CM" data-geoid="2233387">Cameroon</option>
								<option value="CA" data-geoid="6251999">Canada</option>
								<option value="CV" data-geoid="3374766">Cape Verde</option>
								<option value="KY" data-geoid="3580718">Cayman Islands</option>
								<option value="CF" data-geoid="239880">Central African Republic</option>
								<option value="TD" data-geoid="2434508">Chad</option>
								<option value="CL" data-geoid="3895114">Chile</option>
								<option value="CN" data-geoid="1814991">China</option>
								<option value="CX" data-geoid="2078138">Christmas Island</option>
								<option value="CC" data-geoid="1547376">Cocos [Keeling] Islands</option>
								<option value="CO" data-geoid="3686110">Colombia</option>
								<option value="KM" data-geoid="921929">Comoros</option>
								<option value="CK" data-geoid="1899402">Cook Islands</option>
								<option value="CR" data-geoid="3624060">Costa Rica</option>
								<option value="HR" data-geoid="3202326">Croatia</option>
								<option value="CU" data-geoid="3562981">Cuba</option>
								<option value="CW" data-geoid="7626836">Curacao</option>
								<option value="CY" data-geoid="146669">Cyprus</option>
								<option value="CZ" data-geoid="3077311">Czech Republic</option>
								<option value="CD" data-geoid="203312">Democratic Republic of the Congo</option>
								<option value="DK" data-geoid="2623032">Denmark</option>
								<option value="DJ" data-geoid="223816">Djibouti</option>
								<option value="DM" data-geoid="3575830">Dominica</option>
								<option value="DO" data-geoid="3508796">Dominican Republic</option>
								<option value="TL" data-geoid="1966436">East Timor</option>
								<option value="EC" data-geoid="3658394">Ecuador</option>
								<option value="EG" data-geoid="357994">Egypt</option>
								<option value="SV" data-geoid="3585968">El Salvador</option>
								<option value="GQ" data-geoid="2309096">Equatorial Guinea</option>
								<option value="ER" data-geoid="338010">Eritrea</option>
								<option value="EE" data-geoid="453733">Estonia</option>
								<option value="ET" data-geoid="337996">Ethiopia</option>
								<option value="FK" data-geoid="3474414">Falkland Islands</option>
								<option value="FO" data-geoid="2622320">Faroe Islands</option>
								<option value="FJ" data-geoid="2205218">Fiji</option>
								<option value="FI" data-geoid="660013">Finland</option>
								<option value="FR" data-geoid="3017382">France</option>
								<option value="GF" data-geoid="3381670">French Guiana</option>
								<option value="PF" data-geoid="4030656">French Polynesia</option>
								<option value="TF" data-geoid="1546748">French Southern Territories</option>
								<option value="GA" data-geoid="2400553">Gabon</option>
								<option value="GM" data-geoid="2413451">Gambia</option>
								<option value="GE" data-geoid="614540">Georgia</option>
								<option value="DE" data-geoid="2921044">Germany</option>
								<option value="GH" data-geoid="2300660">Ghana</option>
								<option value="GI" data-geoid="2411586">Gibraltar</option>
								<option value="GR" data-geoid="390903">Greece</option>
								<option value="GL" data-geoid="3425505">Greenland</option>
								<option value="GD" data-geoid="3580239">Grenada</option>
								<option value="GP" data-geoid="3579143">Guadeloupe</option>
								<option value="GU" data-geoid="4043988">Guam</option>
								<option value="GT" data-geoid="3595528">Guatemala</option>
								<option value="GG" data-geoid="3042362">Guernsey</option>
								<option value="GN" data-geoid="2420477">Guinea</option>
								<option value="GW" data-geoid="2372248">Guinea-Bissau</option>
								<option value="GY" data-geoid="3378535">Guyana</option>
								<option value="HT" data-geoid="3723988">Haiti</option>
								<option value="HM" data-geoid="1547314">Heard Island and McDonald Islands</option>
								<option value="HN" data-geoid="3608932">Honduras</option>
								<option value="HK" data-geoid="1819730">Hong Kong</option>
								<option value="HU" data-geoid="719819">Hungary</option>
								<option value="IS" data-geoid="2629691">Iceland</option>
								<option value="IN" data-geoid="1269750">India</option>
								<option value="ID" data-geoid="1643084">Indonesia</option>
								<option value="IR" data-geoid="130758">Iran</option>
								<option value="IQ" data-geoid="99237">Iraq</option>
								<option value="IE" data-geoid="2963597">Ireland</option>
								<option value="IM" data-geoid="3042225">Isle of Man</option>
								<option value="IL" data-geoid="294640">Israel</option>
								<option value="IT" data-geoid="3175395">Italy</option>
								<option value="CI" data-geoid="2287781">Ivory Coast</option>
								<option value="JM" data-geoid="3489940">Jamaica</option>
								<option value="JP" data-geoid="1861060">Japan</option>
								<option value="JE" data-geoid="3042142">Jersey</option>
								<option value="JO" data-geoid="248816">Jordan</option>
								<option value="KZ" data-geoid="1522867">Kazakhstan</option>
								<option value="KE" data-geoid="192950">Kenya</option>
								<option value="KI" data-geoid="4030945">Kiribati</option>
								<option value="XK" data-geoid="831053">Kosovo</option>
								<option value="KW" data-geoid="285570">Kuwait</option>
								<option value="KG" data-geoid="1527747">Kyrgyzstan</option>
								<option value="LA" data-geoid="1655842">Laos</option>
								<option value="LV" data-geoid="458258">Latvia</option>
								<option value="LB" data-geoid="272103">Lebanon</option>
								<option value="LS" data-geoid="932692">Lesotho</option>
								<option value="LR" data-geoid="2275384">Liberia</option>
								<option value="LY" data-geoid="2215636">Libya</option>
								<option value="LI" data-geoid="3042058">Liechtenstein</option>
								<option value="LT" data-geoid="597427">Lithuania</option>
								<option value="LU" data-geoid="2960313">Luxembourg</option>
								<option value="MO" data-geoid="1821275">Macao</option>
								<option value="MK" data-geoid="718075">Macedonia</option>
								<option value="MG" data-geoid="1062947">Madagascar</option>
								<option value="MW" data-geoid="927384">Malawi</option>
								<option value="MY" data-geoid="1733045">Malaysia</option>
								<option value="MV" data-geoid="1282028">Maldives</option>
								<option value="ML" data-geoid="2453866">Mali</option>
								<option value="MT" data-geoid="2562770">Malta</option>
								<option value="MH" data-geoid="2080185">Marshall Islands</option>
								<option value="MQ" data-geoid="3570311">Martinique</option>
								<option value="MR" data-geoid="2378080">Mauritania</option>
								<option value="MU" data-geoid="934292">Mauritius</option>
								<option value="YT" data-geoid="1024031">Mayotte</option>
								<option value="MX" data-geoid="3996063">Mexico</option>
								<option value="FM" data-geoid="2081918">Micronesia</option>
								<option value="MD" data-geoid="617790">Moldova</option>
								<option value="MC" data-geoid="2993457">Monaco</option>
								<option value="MN" data-geoid="2029969">Mongolia</option>
								<option value="ME" data-geoid="3194884">Montenegro</option>
								<option value="MS" data-geoid="3578097">Montserrat</option>
								<option value="MA" data-geoid="2542007">Morocco</option>
								<option value="MZ" data-geoid="1036973">Mozambique</option>
								<option value="MM" data-geoid="1327865">Myanmar [Burma]</option>
								<option value="NA" data-geoid="3355338">Namibia</option>
								<option value="NR" data-geoid="2110425">Nauru</option>
								<option value="NP" data-geoid="1282988">Nepal</option>
								<option value="NL" data-geoid="2750405">Netherlands</option>
								<option value="NC" data-geoid="2139685">New Caledonia</option>
								<option value="NZ" data-geoid="2186224">New Zealand</option>
								<option value="NI" data-geoid="3617476">Nicaragua</option>
								<option value="NE" data-geoid="2440476">Niger</option>
								<option value="NG" data-geoid="2328926">Nigeria</option>
								<option value="NU" data-geoid="4036232">Niue</option>
								<option value="NF" data-geoid="2155115">Norfolk Island</option>
								<option value="KP" data-geoid="1873107">North Korea</option>
								<option value="MP" data-geoid="4041468">Northern Mariana Islands</option>
								<option value="NO" data-geoid="3144096">Norway</option>
								<option value="OM" data-geoid="286963">Oman</option>
								<option value="PK" data-geoid="1168579">Pakistan</option>
								<option value="PW" data-geoid="1559582">Palau</option>
								<option value="PS" data-geoid="6254930">Palestine</option>
								<option value="PA" data-geoid="3703430">Panama</option>
								<option value="PG" data-geoid="2088628">Papua New Guinea</option>
								<option value="PY" data-geoid="3437598">Paraguay</option>
								<option value="PE" data-geoid="3932488">Peru</option>
								<option value="PH" data-geoid="1694008">Philippines</option>
								<option value="PN" data-geoid="4030699">Pitcairn Islands</option>
								<option value="PL" data-geoid="798544">Poland</option>
								<option value="PT" data-geoid="2264397">Portugal</option>
								<option value="PR" data-geoid="4566966">Puerto Rico</option>
								<option value="QA" data-geoid="289688">Qatar</option>
								<option value="CG" data-geoid="2260494">Republic of the Congo</option>
								<option value="RO" data-geoid="798549">Romania</option>
								<option value="RU" data-geoid="2017370">Russia</option>
								<option value="RW" data-geoid="49518">Rwanda</option>
								<option value="RE" data-geoid="935317">Réunion</option>
								<option value="BL" data-geoid="3578476">Saint Barthélemy</option>
								<option value="SH" data-geoid="3370751">Saint Helena</option>
								<option value="KN" data-geoid="3575174">Saint Kitts and Nevis</option>
								<option value="LC" data-geoid="3576468">Saint Lucia</option>
								<option value="MF" data-geoid="3578421">Saint Martin</option>
								<option value="PM" data-geoid="3424932">Saint Pierre and Miquelon</option>
								<option value="VC" data-geoid="3577815">Saint Vincent and the Grenadines</option>
								<option value="WS" data-geoid="4034894">Samoa</option>
								<option value="SM" data-geoid="3168068">San Marino</option>
								<option value="SA" data-geoid="102358">Saudi Arabia</option>
								<option value="SN" data-geoid="2245662">Senegal</option>
								<option value="RS" data-geoid="6290252">Serbia</option>
								<option value="SC" data-geoid="241170">Seychelles</option>
								<option value="SL" data-geoid="2403846">Sierra Leone</option>
								<option value="SG" data-geoid="1880251">Singapore</option>
								<option value="SX" data-geoid="7609695">Sint Maarten</option>
								<option value="SK" data-geoid="3057568">Slovakia</option>
								<option value="SI" data-geoid="3190538">Slovenia</option>
								<option value="SB" data-geoid="2103350">Solomon Islands</option>
								<option value="SO" data-geoid="51537">Somalia</option>
								<option value="ZA" data-geoid="953987">South Africa</option>
								<option value="GS" data-geoid="3474415">South Georgia and the South Sandwich Islands</option>
								<option value="KR" data-geoid="1835841">South Korea</option>
								<option value="SS" data-geoid="7909807">South Sudan</option>
								<option value="ES" data-geoid="2510769">Spain</option>
								<option value="LK" data-geoid="1227603">Sri Lanka</option>
								<option value="SD" data-geoid="366755">Sudan</option>
								<option value="SR" data-geoid="3382998">Suriname</option>
								<option value="SJ" data-geoid="607072">Svalbard and Jan Mayen</option>
								<option value="SZ" data-geoid="934841">Swaziland</option>
								<option value="SE" data-geoid="2661886">Sweden</option>
								<option value="CH" data-geoid="2658434">Switzerland</option>
								<option value="SY" data-geoid="163843">Syria</option>
								<option value="ST" data-geoid="2410758">São Tomé and Príncipe</option>
								<option value="TW" data-geoid="1668284">Taiwan</option>
								<option value="TJ" data-geoid="1220409">Tajikistan</option>
								<option value="TZ" data-geoid="149590">Tanzania</option>
								<option value="TH" data-geoid="1605651">Thailand</option>
								<option value="TG" data-geoid="2363686">Togo</option>
								<option value="TK" data-geoid="4031074">Tokelau</option>
								<option value="TO" data-geoid="4032283">Tonga</option>
								<option value="TT" data-geoid="3573591">Trinidad and Tobago</option>
								<option value="TN" data-geoid="2464461">Tunisia</option>
								<option value="TR" data-geoid="298795">Turkey</option>
								<option value="TM" data-geoid="1218197">Turkmenistan</option>
								<option value="TC" data-geoid="3576916">Turks and Caicos Islands</option>
								<option value="TV" data-geoid="2110297">Tuvalu</option>
								<option value="UM" data-geoid="5854968">U.S. Minor Outlying Islands</option>
								<option value="VI" data-geoid="4796775">U.S. Virgin Islands</option>
								<option value="UG" data-geoid="226074">Uganda</option>
								<option value="UA" data-geoid="690791">Ukraine</option>
								<option value="AE" data-geoid="290557">United Arab Emirates</option>
								<option value="GB" data-geoid="2635167">United Kingdom</option>
								<option value="US" data-geoid="6252001">United States</option>
								<option value="UY" data-geoid="3439705">Uruguay</option>
								<option value="UZ" data-geoid="1512440">Uzbekistan</option>
								<option value="VU" data-geoid="2134431">Vanuatu</option>
								<option value="VA" data-geoid="3164670">Vatican City</option>
								<option value="VE" data-geoid="3625428">Venezuela</option>
								<option value="VN" data-geoid="1562822">Vietnam</option>
								<option value="WF" data-geoid="4034749">Wallis and Futuna</option>
								<option value="EH" data-geoid="2461445">Western Sahara</option>
								<option value="YE" data-geoid="69543">Yemen</option>
								<option value="ZM" data-geoid="895949">Zambia</option>
								<option value="ZW" data-geoid="878675">Zimbabwe</option>
								<option value="AX" data-geoid="661882">Åland</option>
							</select>
						</div>
					</div>
					<div class="col-sm-3">
						<p class="form-label">
							Zip code
						</p>
						<div class="form-group">
							<input type="text" class="form-control">
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-6">
						<p class="form-label">
							State
						</p>
						<div class="form-group">
							<select name="billingState" class="form-control select2-hidden-accessible" data-parsley-required="true" data-parsley-trigger="change" tabindex="-1" aria-hidden="true" data-parsley-id="18"></select>
						</div>
					</div>
					<div class="col-sm-6">
						<p class="form-label">
							City
						</p>
						<div class="form-group">
							<select name="billingCity" class="form-control select2-hidden-accessible" data-parsley-required="true" data-parsley-trigger="change" tabindex="-1" aria-hidden="true" data-parsley-id="20"></select>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-6">
						<p class="form-label">
							Address
						</p>
						<div class="form-group">
							<input type="text" class="form-control">
						</div>
					</div>
				</div>

				<h3 class="checkout__body__title">
					Payment info
				</h3>
				<div class="credit-card clearfix">
		            <div class="card card-front">
		                <div class="row xs-hidden">
		                    <div class="col-xs-12">
		                    	<p class="form-label">Card Number</p>
		                    </div>
		                    <div class="col-xs-6 col-sm-3 form-group">
		                        <input type="text" class="form-control" placeholder="0000" maxlength="4">
		                    </div>
		                    <div class="col-xs-6 col-sm-3 form-group">
		                        <input type="text" class="form-control" placeholder="0000" maxlength="4">
		                    </div>
		                    <div class="col-xs-6 col-sm-3 form-group">
		                        <input type="text" class="form-control" placeholder="0000" maxlength="4">
		                    </div>
		                    <div class="col-xs-6 col-sm-3 form-group">
		                        <input type="text" class="form-control" placeholder="0000">
		                    </div>
		                </div>

		                <div class="row xs-min-hidden">
		                    <div class="col-xs-12 form-group">
		                        <input type="text" class="form-control" placeholder="0000 0000 0000 0000" maxlength="16">
		                    </div>
		                </div>

		                <div class="row">
		                    <div class="col-xs-12">
		                    	<p class="form-label">Expiration Date</p>
		                    </div>
		                    <div class="col-sm-6">
		                        <div class="row">
		                            <div class="col-xs-6 form-group">
		                                <input type="text" class="form-control" placeholder="MM" maxlength="2">
		                            </div>
		                            <div class="col-xs-6 form-group">
		                                <input type="text" class="form-control" placeholder="YY" maxlength="2">
		                            </div>
		                        </div>
		                    </div>
		                </div>

		                <div class="row">
		                    <div class="col-xs-12">
		                    	<p class="form-label">Card Holder Name</p>
		                    </div>
		                    <div class="col-xs-12 form-group">
		                        <input type="text" class="form-control input-filled" placeholder="Full Name">
		                    </div>
		                </div>
		            </div>
		            <div class="card card-back">
		                <div class="row">
		                    <div class="col-sm-3 pull-sm-right">
		                        <p class="form-label clearfix">
		                            <small class="pull-left">CVC / CSV</small>
		                            <i class="icon-question primary pull-right m-t3"></i>
		                        </p>
		                        <div class="form-group has-icon left-key">
		                            <input type="text" class="form-control" placeholder="cvc">
		                        </div>
		                    </div>
		                </div>
		            </div>
		        </div>


				<h3 class="checkout__body__title">
					Billing Info
				</h3>
				<div class="row">
					<div class="col-xs-12">
						<label class="cb-checkbox">
							<input type="checkbox">
							Same as Shipping Address?
						</label>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-6">
						<p class="form-label">
							First name
						</p>
						<div class="form-group">
							<input type="text" class="form-control">
						</div>
					</div>
					<div class="col-sm-6">
						<p class="form-label">
							Last name
						</p>
						<div class="form-group">
							<input type="text" class="form-control">
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-6">
						<p class="form-label">
							Phone number
						</p>
						<div class="form-group">
							<input type="text" class="form-control">
						</div>
					</div>
					<div class="col-sm-6">
						<p class="form-label">
							E-mail
						</p>
						<div class="form-group">
							<input type="text" class="form-control">
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-6">
						<p class="form-label">
							Country
						</p>
						<div class="form-group">
							<select name="billingCountry" class="form-control select2-hidden-accessible parsley-success" data-parsley-required="true" data-parsley-trigger="change" tabindex="-1" aria-hidden="true" data-parsley-id="14">
								<option value="">Select country</option>
								<option value="AF" data-geoid="1149361">Afghanistan</option>
								<option value="AL" data-geoid="783754">Albania</option>
								<option value="DZ" data-geoid="2589581">Algeria</option>
								<option value="AS" data-geoid="5880801">American Samoa</option>
								<option value="AD" data-geoid="3041565">Andorra</option>
								<option value="AO" data-geoid="3351879">Angola</option>
								<option value="AI" data-geoid="3573511">Anguilla</option>
								<option value="AQ" data-geoid="6697173">Antarctica</option>
								<option value="AG" data-geoid="3576396">Antigua and Barbuda</option>
								<option value="AR" data-geoid="3865483">Argentina</option>
								<option value="AM" data-geoid="174982">Armenia</option>
								<option value="AW" data-geoid="3577279">Aruba</option>
								<option value="AU" data-geoid="2077456">Australia</option>
								<option value="AT" data-geoid="2782113">Austria</option>
								<option value="AZ" data-geoid="587116">Azerbaijan</option>
								<option value="BS" data-geoid="3572887">Bahamas</option>
								<option value="BH" data-geoid="290291">Bahrain</option>
								<option value="BD" data-geoid="1210997">Bangladesh</option>
								<option value="BB" data-geoid="3374084">Barbados</option>
								<option value="BY" data-geoid="630336">Belarus</option>
								<option value="BE" data-geoid="2802361">Belgium</option>
								<option value="BZ" data-geoid="3582678">Belize</option>
								<option value="BJ" data-geoid="2395170">Benin</option>
								<option value="BM" data-geoid="3573345">Bermuda</option>
								<option value="BT" data-geoid="1252634">Bhutan</option>
								<option value="BO" data-geoid="3923057">Bolivia</option>
								<option value="BQ" data-geoid="7626844">Bonaire</option>
								<option value="BA" data-geoid="3277605">Bosnia and Herzegovina</option>
								<option value="BW" data-geoid="933860">Botswana</option>
								<option value="BV" data-geoid="3371123">Bouvet Island</option>
								<option value="BR" data-geoid="3469034">Brazil</option>
								<option value="IO" data-geoid="1282588">British Indian Ocean Territory</option>
								<option value="VG" data-geoid="3577718">British Virgin Islands</option>
								<option value="BN" data-geoid="1820814">Brunei</option>
								<option value="BG" data-geoid="732800">Bulgaria</option>
								<option value="BF" data-geoid="2361809">Burkina Faso</option>
								<option value="BI" data-geoid="433561">Burundi</option>
								<option value="KH" data-geoid="1831722">Cambodia</option>
								<option value="CM" data-geoid="2233387">Cameroon</option>
								<option value="CA" data-geoid="6251999">Canada</option>
								<option value="CV" data-geoid="3374766">Cape Verde</option>
								<option value="KY" data-geoid="3580718">Cayman Islands</option>
								<option value="CF" data-geoid="239880">Central African Republic</option>
								<option value="TD" data-geoid="2434508">Chad</option>
								<option value="CL" data-geoid="3895114">Chile</option>
								<option value="CN" data-geoid="1814991">China</option>
								<option value="CX" data-geoid="2078138">Christmas Island</option>
								<option value="CC" data-geoid="1547376">Cocos [Keeling] Islands</option>
								<option value="CO" data-geoid="3686110">Colombia</option>
								<option value="KM" data-geoid="921929">Comoros</option>
								<option value="CK" data-geoid="1899402">Cook Islands</option>
								<option value="CR" data-geoid="3624060">Costa Rica</option>
								<option value="HR" data-geoid="3202326">Croatia</option>
								<option value="CU" data-geoid="3562981">Cuba</option>
								<option value="CW" data-geoid="7626836">Curacao</option>
								<option value="CY" data-geoid="146669">Cyprus</option>
								<option value="CZ" data-geoid="3077311">Czech Republic</option>
								<option value="CD" data-geoid="203312">Democratic Republic of the Congo</option>
								<option value="DK" data-geoid="2623032">Denmark</option>
								<option value="DJ" data-geoid="223816">Djibouti</option>
								<option value="DM" data-geoid="3575830">Dominica</option>
								<option value="DO" data-geoid="3508796">Dominican Republic</option>
								<option value="TL" data-geoid="1966436">East Timor</option>
								<option value="EC" data-geoid="3658394">Ecuador</option>
								<option value="EG" data-geoid="357994">Egypt</option>
								<option value="SV" data-geoid="3585968">El Salvador</option>
								<option value="GQ" data-geoid="2309096">Equatorial Guinea</option>
								<option value="ER" data-geoid="338010">Eritrea</option>
								<option value="EE" data-geoid="453733">Estonia</option>
								<option value="ET" data-geoid="337996">Ethiopia</option>
								<option value="FK" data-geoid="3474414">Falkland Islands</option>
								<option value="FO" data-geoid="2622320">Faroe Islands</option>
								<option value="FJ" data-geoid="2205218">Fiji</option>
								<option value="FI" data-geoid="660013">Finland</option>
								<option value="FR" data-geoid="3017382">France</option>
								<option value="GF" data-geoid="3381670">French Guiana</option>
								<option value="PF" data-geoid="4030656">French Polynesia</option>
								<option value="TF" data-geoid="1546748">French Southern Territories</option>
								<option value="GA" data-geoid="2400553">Gabon</option>
								<option value="GM" data-geoid="2413451">Gambia</option>
								<option value="GE" data-geoid="614540">Georgia</option>
								<option value="DE" data-geoid="2921044">Germany</option>
								<option value="GH" data-geoid="2300660">Ghana</option>
								<option value="GI" data-geoid="2411586">Gibraltar</option>
								<option value="GR" data-geoid="390903">Greece</option>
								<option value="GL" data-geoid="3425505">Greenland</option>
								<option value="GD" data-geoid="3580239">Grenada</option>
								<option value="GP" data-geoid="3579143">Guadeloupe</option>
								<option value="GU" data-geoid="4043988">Guam</option>
								<option value="GT" data-geoid="3595528">Guatemala</option>
								<option value="GG" data-geoid="3042362">Guernsey</option>
								<option value="GN" data-geoid="2420477">Guinea</option>
								<option value="GW" data-geoid="2372248">Guinea-Bissau</option>
								<option value="GY" data-geoid="3378535">Guyana</option>
								<option value="HT" data-geoid="3723988">Haiti</option>
								<option value="HM" data-geoid="1547314">Heard Island and McDonald Islands</option>
								<option value="HN" data-geoid="3608932">Honduras</option>
								<option value="HK" data-geoid="1819730">Hong Kong</option>
								<option value="HU" data-geoid="719819">Hungary</option>
								<option value="IS" data-geoid="2629691">Iceland</option>
								<option value="IN" data-geoid="1269750">India</option>
								<option value="ID" data-geoid="1643084">Indonesia</option>
								<option value="IR" data-geoid="130758">Iran</option>
								<option value="IQ" data-geoid="99237">Iraq</option>
								<option value="IE" data-geoid="2963597">Ireland</option>
								<option value="IM" data-geoid="3042225">Isle of Man</option>
								<option value="IL" data-geoid="294640">Israel</option>
								<option value="IT" data-geoid="3175395">Italy</option>
								<option value="CI" data-geoid="2287781">Ivory Coast</option>
								<option value="JM" data-geoid="3489940">Jamaica</option>
								<option value="JP" data-geoid="1861060">Japan</option>
								<option value="JE" data-geoid="3042142">Jersey</option>
								<option value="JO" data-geoid="248816">Jordan</option>
								<option value="KZ" data-geoid="1522867">Kazakhstan</option>
								<option value="KE" data-geoid="192950">Kenya</option>
								<option value="KI" data-geoid="4030945">Kiribati</option>
								<option value="XK" data-geoid="831053">Kosovo</option>
								<option value="KW" data-geoid="285570">Kuwait</option>
								<option value="KG" data-geoid="1527747">Kyrgyzstan</option>
								<option value="LA" data-geoid="1655842">Laos</option>
								<option value="LV" data-geoid="458258">Latvia</option>
								<option value="LB" data-geoid="272103">Lebanon</option>
								<option value="LS" data-geoid="932692">Lesotho</option>
								<option value="LR" data-geoid="2275384">Liberia</option>
								<option value="LY" data-geoid="2215636">Libya</option>
								<option value="LI" data-geoid="3042058">Liechtenstein</option>
								<option value="LT" data-geoid="597427">Lithuania</option>
								<option value="LU" data-geoid="2960313">Luxembourg</option>
								<option value="MO" data-geoid="1821275">Macao</option>
								<option value="MK" data-geoid="718075">Macedonia</option>
								<option value="MG" data-geoid="1062947">Madagascar</option>
								<option value="MW" data-geoid="927384">Malawi</option>
								<option value="MY" data-geoid="1733045">Malaysia</option>
								<option value="MV" data-geoid="1282028">Maldives</option>
								<option value="ML" data-geoid="2453866">Mali</option>
								<option value="MT" data-geoid="2562770">Malta</option>
								<option value="MH" data-geoid="2080185">Marshall Islands</option>
								<option value="MQ" data-geoid="3570311">Martinique</option>
								<option value="MR" data-geoid="2378080">Mauritania</option>
								<option value="MU" data-geoid="934292">Mauritius</option>
								<option value="YT" data-geoid="1024031">Mayotte</option>
								<option value="MX" data-geoid="3996063">Mexico</option>
								<option value="FM" data-geoid="2081918">Micronesia</option>
								<option value="MD" data-geoid="617790">Moldova</option>
								<option value="MC" data-geoid="2993457">Monaco</option>
								<option value="MN" data-geoid="2029969">Mongolia</option>
								<option value="ME" data-geoid="3194884">Montenegro</option>
								<option value="MS" data-geoid="3578097">Montserrat</option>
								<option value="MA" data-geoid="2542007">Morocco</option>
								<option value="MZ" data-geoid="1036973">Mozambique</option>
								<option value="MM" data-geoid="1327865">Myanmar [Burma]</option>
								<option value="NA" data-geoid="3355338">Namibia</option>
								<option value="NR" data-geoid="2110425">Nauru</option>
								<option value="NP" data-geoid="1282988">Nepal</option>
								<option value="NL" data-geoid="2750405">Netherlands</option>
								<option value="NC" data-geoid="2139685">New Caledonia</option>
								<option value="NZ" data-geoid="2186224">New Zealand</option>
								<option value="NI" data-geoid="3617476">Nicaragua</option>
								<option value="NE" data-geoid="2440476">Niger</option>
								<option value="NG" data-geoid="2328926">Nigeria</option>
								<option value="NU" data-geoid="4036232">Niue</option>
								<option value="NF" data-geoid="2155115">Norfolk Island</option>
								<option value="KP" data-geoid="1873107">North Korea</option>
								<option value="MP" data-geoid="4041468">Northern Mariana Islands</option>
								<option value="NO" data-geoid="3144096">Norway</option>
								<option value="OM" data-geoid="286963">Oman</option>
								<option value="PK" data-geoid="1168579">Pakistan</option>
								<option value="PW" data-geoid="1559582">Palau</option>
								<option value="PS" data-geoid="6254930">Palestine</option>
								<option value="PA" data-geoid="3703430">Panama</option>
								<option value="PG" data-geoid="2088628">Papua New Guinea</option>
								<option value="PY" data-geoid="3437598">Paraguay</option>
								<option value="PE" data-geoid="3932488">Peru</option>
								<option value="PH" data-geoid="1694008">Philippines</option>
								<option value="PN" data-geoid="4030699">Pitcairn Islands</option>
								<option value="PL" data-geoid="798544">Poland</option>
								<option value="PT" data-geoid="2264397">Portugal</option>
								<option value="PR" data-geoid="4566966">Puerto Rico</option>
								<option value="QA" data-geoid="289688">Qatar</option>
								<option value="CG" data-geoid="2260494">Republic of the Congo</option>
								<option value="RO" data-geoid="798549">Romania</option>
								<option value="RU" data-geoid="2017370">Russia</option>
								<option value="RW" data-geoid="49518">Rwanda</option>
								<option value="RE" data-geoid="935317">Réunion</option>
								<option value="BL" data-geoid="3578476">Saint Barthélemy</option>
								<option value="SH" data-geoid="3370751">Saint Helena</option>
								<option value="KN" data-geoid="3575174">Saint Kitts and Nevis</option>
								<option value="LC" data-geoid="3576468">Saint Lucia</option>
								<option value="MF" data-geoid="3578421">Saint Martin</option>
								<option value="PM" data-geoid="3424932">Saint Pierre and Miquelon</option>
								<option value="VC" data-geoid="3577815">Saint Vincent and the Grenadines</option>
								<option value="WS" data-geoid="4034894">Samoa</option>
								<option value="SM" data-geoid="3168068">San Marino</option>
								<option value="SA" data-geoid="102358">Saudi Arabia</option>
								<option value="SN" data-geoid="2245662">Senegal</option>
								<option value="RS" data-geoid="6290252">Serbia</option>
								<option value="SC" data-geoid="241170">Seychelles</option>
								<option value="SL" data-geoid="2403846">Sierra Leone</option>
								<option value="SG" data-geoid="1880251">Singapore</option>
								<option value="SX" data-geoid="7609695">Sint Maarten</option>
								<option value="SK" data-geoid="3057568">Slovakia</option>
								<option value="SI" data-geoid="3190538">Slovenia</option>
								<option value="SB" data-geoid="2103350">Solomon Islands</option>
								<option value="SO" data-geoid="51537">Somalia</option>
								<option value="ZA" data-geoid="953987">South Africa</option>
								<option value="GS" data-geoid="3474415">South Georgia and the South Sandwich Islands</option>
								<option value="KR" data-geoid="1835841">South Korea</option>
								<option value="SS" data-geoid="7909807">South Sudan</option>
								<option value="ES" data-geoid="2510769">Spain</option>
								<option value="LK" data-geoid="1227603">Sri Lanka</option>
								<option value="SD" data-geoid="366755">Sudan</option>
								<option value="SR" data-geoid="3382998">Suriname</option>
								<option value="SJ" data-geoid="607072">Svalbard and Jan Mayen</option>
								<option value="SZ" data-geoid="934841">Swaziland</option>
								<option value="SE" data-geoid="2661886">Sweden</option>
								<option value="CH" data-geoid="2658434">Switzerland</option>
								<option value="SY" data-geoid="163843">Syria</option>
								<option value="ST" data-geoid="2410758">São Tomé and Príncipe</option>
								<option value="TW" data-geoid="1668284">Taiwan</option>
								<option value="TJ" data-geoid="1220409">Tajikistan</option>
								<option value="TZ" data-geoid="149590">Tanzania</option>
								<option value="TH" data-geoid="1605651">Thailand</option>
								<option value="TG" data-geoid="2363686">Togo</option>
								<option value="TK" data-geoid="4031074">Tokelau</option>
								<option value="TO" data-geoid="4032283">Tonga</option>
								<option value="TT" data-geoid="3573591">Trinidad and Tobago</option>
								<option value="TN" data-geoid="2464461">Tunisia</option>
								<option value="TR" data-geoid="298795">Turkey</option>
								<option value="TM" data-geoid="1218197">Turkmenistan</option>
								<option value="TC" data-geoid="3576916">Turks and Caicos Islands</option>
								<option value="TV" data-geoid="2110297">Tuvalu</option>
								<option value="UM" data-geoid="5854968">U.S. Minor Outlying Islands</option>
								<option value="VI" data-geoid="4796775">U.S. Virgin Islands</option>
								<option value="UG" data-geoid="226074">Uganda</option>
								<option value="UA" data-geoid="690791">Ukraine</option>
								<option value="AE" data-geoid="290557">United Arab Emirates</option>
								<option value="GB" data-geoid="2635167">United Kingdom</option>
								<option value="US" data-geoid="6252001">United States</option>
								<option value="UY" data-geoid="3439705">Uruguay</option>
								<option value="UZ" data-geoid="1512440">Uzbekistan</option>
								<option value="VU" data-geoid="2134431">Vanuatu</option>
								<option value="VA" data-geoid="3164670">Vatican City</option>
								<option value="VE" data-geoid="3625428">Venezuela</option>
								<option value="VN" data-geoid="1562822">Vietnam</option>
								<option value="WF" data-geoid="4034749">Wallis and Futuna</option>
								<option value="EH" data-geoid="2461445">Western Sahara</option>
								<option value="YE" data-geoid="69543">Yemen</option>
								<option value="ZM" data-geoid="895949">Zambia</option>
								<option value="ZW" data-geoid="878675">Zimbabwe</option>
								<option value="AX" data-geoid="661882">Åland</option>
							</select>
						</div>
					</div>
					<div class="col-sm-3">
						<p class="form-label">
							Zip code
						</p>
						<div class="form-group">
							<input type="text" class="form-control">
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-6">
						<p class="form-label">
							State
						</p>
						<div class="form-group">
							<input type="text" class="form-control">
						</div>
					</div>
					<div class="col-sm-6">
						<p class="form-label">
							City
						</p>
						<div class="form-group">
							<input type="text" class="form-control">
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-6">
						<p class="form-label">
							Address
						</p>
						<div class="form-group">
							<input type="text" class="form-control">
						</div>
					</div>
				</div>			
			</div>		
		</div>
		<div class="col-md-4 is-relative" >
			<div class="checkout__body" data-affix>
				<h3 class="checkout__body__title">
					Order overview
				</h3>
				<div class="row">
					<div class="col-sm-7">
						<p class="text-page">Subtotal (include VAT):</p>
					</div>
					<div class="col-sm-5 sm-right">
						<p class="text-page">$ 88.00</p>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-7">
						<p class="text-page">Shipping:</p>
					</div>
					<div class="col-sm-5 sm-right">
						<p class="text-page">$ 15.98</p>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-7">
						<p class="text-page">Total Savings:</p>
					</div>
					<div class="col-sm-5 sm-right">
						<p class="text-page">$ 00.00</p>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-7">
						<p class="text-page">VAT 0%:</p>
					</div>
					<div class="col-sm-5 sm-right">
						<p class="text-page">$ 00.00</p>
					</div>
				</div>
				<div class="row m-t20">
					<div class="col-sm-7">					
						<p class="text-bold text-mutted">Total price:</p>
					</div>
					<div class="col-sm-5 sm-right">
						<p class="text-bold text-mutted">$103.98</p>
					</div>
				</div>
				<div class="row m-t20">
					<div class="col-xs-12">
						<p class="text-lighten m-b0">
							Have a promo code?
						</p>
						<a href="" class="link__page-16">
							Click here to enter it
						</a>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12">
						<div class="b-Top p-t20 m-t20">
							<label class="cb-checkbox cb-gray">
								<input type="checkbox">
								I agree to JUSTSHIRT <a href="">Privacy Policy</a>
							</label>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12">
						<button class="btn__accept">
							Pay Now
						</button>
					</div>
				</div>
			</div>	
		</div>
	</div>
	</div>
</div>