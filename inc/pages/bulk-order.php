<div class="row m-t30 m-b100">
    <div class="col-lg-9 col-md-8">
    	<div class="post">
			<h3>
				Text STATIC Block 
			</h3>
			<p class="post__content__text">
				hgoregihoiregiore
			</p>
			<p class="post__content__text">
				Lorem ipsum dolor sit amet
			</p>
			<p class="post__content__text">
				Dolore ex, voluptatibus esse amet
			</p>
			<div class="page-content__row m-t40">
				<h3>
					Contact Form for large quantities / wholesale prices
				</h3>
				<div class="clearfix">
					<div class="quantity">
						<div class="form-group">
							<p class="pull-left m-r10 p-t5"><strong>Qty</strong></p>
				            <div class="quantity__block input-group">
				                <button class="quantity__decrement input-group-btn">-</button>
				                <input class="quantity__control form-control" type="text" value="8">
				                <button class="quantity__increment input-group-btn">+</button>
				            </div>
						</div>
			        </div>
				</div>
				<div class="row">
					<div class="col-sm-6">
						<p>
							<strong>Product</strong>
						</p>
						<div class="form-group">
							<input type="text" class="form-control">
						</div>
					</div>
					<div class="col-sm-6">
						<p >
							<strong>Prin Type</strong>
						</p>
						<div class="form-group">
							<input type="text" class="form-control">
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-6">
						<div class="form-group">
							<p>
								<strong>Prin Area</strong>
							</p>
							<div class="form-group">
								<input type="text" class="form-control">
							</div>
						</div>
					</div>
					<div class="col-sm-6">
						<div class="form-group">
							<p>
								<strong>Delivery Time</strong>
							</p>
							<div class="form-group">
								<input type="text" class="form-control">
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-6">
						<div class="form-group">
							<p>
								<strong>Name</strong>
							</p>
							<div class="form-group">
								<input type="text" class="form-control">
							</div>
						</div>
					</div>
					<div class="col-sm-6">
						<div class="form-group">
							<p>
								<strong>Company</strong>
							</p>
							<div class="form-group">
								<input type="text" class="form-control">
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-6">
						<div class="form-group">
							<p>
								<strong>Address</strong>
							</p>
							<div class="form-group">
								<input type="text" class="form-control">
							</div>
						</div>
					</div>
					<div class="col-sm-6">
						<div class="form-group">
							<p>
								<strong>Tel. Number</strong>
							</p>
							<div class="form-group">
								<input type="text" class="form-control">
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-6">
						<div class="form-group">
							<p>
								<strong>E-mail</strong>
							</p>
							<div class="form-group">
								<input type="text" class="form-control">
							</div>
						</div>
					</div>
					<div class="col-sm-6">
						<div class="form-group">
							<p>
								<strong>Upload Design</strong>
							</p>
							<div class="form-group">
								<button class="btn__default btn__small"> Upload your own
									<input type="file" multiple="">
								</button>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12">
						<div class="form-group">
							<p>
								<strong>Comment</strong>
							</p>
							<textarea rows="5" class="form-control"></textarea>
						</div>
					</div>
				</div>
                <div class="row">
                    <div class="col-xs-12 text-right">
                        <input type="submit" class="btn__default" value="Send">
                    </div>
                </div>
			</div>
    	</div>
     </div>
    <div class="col-lg-3 col-md-4 sm-hidden">
        <div class="post__banner">
            <img src="images/blog/post_banner.jpg" alt=""/>

            <div class="post__banner__text-box">
                <h4 class="post__banner__text">Video Tutorial: Applying Vector and Effects</h4>
                <a class="btn-post-reverse" href="">Read more</a>
            </div>
        </div>


        <div class="month-posts">    
            <div class="month-posts__slider">
                <div data-date="May 2015">
                    <a href="" class="month-posts__post">
                        <div class="month-posts__post__img-box">
                            <img src="images/blog/mini-post-1.jpg" alt=""/>
                        </div>
                        <div class="month-posts__post__content">
                            <span class="month-posts__post__title">Trending Colors – The Best-Selling Tones6</span>
                            <span class="month-posts__post__date">September, 2015</span>
                        </div>
                    </a>
                    <a href="" class="month-posts__post">
                        <div class="month-posts__post__img-box">
                            <img src="images/blog/mini-post-2.jpg" alt=""/>
                        </div>
                        <div class="month-posts__post__content">
                            <span class="month-posts__post__title">Now that’s Fresh!</span>
                            <span class="month-posts__post__date">September, 2015</span>
                        </div>
                    </a>
                    

                    <div class="month-posts__link-wrap">
                        <a class="month-posts__link" href="">More posts for this month</a>
                    </div>
                </div>
                <div data-date="June 2015">
                    <a href="" class="month-posts__post">
                        <div class="month-posts__post__img-box">
                            <img src="images/blog/mini-post-1.jpg" alt=""/>
                        </div>
                        <div class="month-posts__post__content">
                            <span class="month-posts__post__title">Trending Colors – The Best-Selling Tones6</span>
                            <span class="month-posts__post__date">September, 2015</span>
                        </div>
                    </a>
                    <a href="" class="month-posts__post">
                        <div class="month-posts__post__img-box">
                            <img src="images/blog/mini-post-2.jpg" alt=""/>
                        </div>
                        <div class="month-posts__post__content">
                            <span class="month-posts__post__title">Now that’s Fresh!</span>
                            <span class="month-posts__post__date">September, 2015</span>
                        </div>
                    </a>
                    <a href="" class="month-posts__post">
                        <div class="month-posts__post__img-box">
                            <img src="images/blog/mini-post-3.jpg" alt=""/>
                        </div>
                        <div class="month-posts__post__content">
                            <span class="month-posts__post__title">Trending Colors – The Best-Selling Tones6</span>
                            <span class="month-posts__post__date">September, 2015</span>
                        </div>
                    </a>
                    <a href="" class="month-posts__post">
                        <div class="month-posts__post__img-box">
                            <img src="images/blog/mini-post-4.jpg" alt=""/>
                        </div>
                        <div class="month-posts__post__content">
                            <span class="month-posts__post__title">Now that’s Fresh!</span>
                            <span class="month-posts__post__date">September, 2015</span>
                        </div>
                    </a>
                    <a href="" class="month-posts__post">
                        <div class="month-posts__post__img-box">
                            <img src="images/blog/mini-post-5.jpg" alt=""/>
                        </div>
                        <div class="month-posts__post__content">
                            <span class="month-posts__post__title">Now that’s Fresh!</span>
                            <span class="month-posts__post__date">September, 2015</span>
                        </div>
                    </a>

                    <div class="month-posts__link-wrap">
                        <a class="month-posts__link" href="">More posts for this month</a>
                    </div>
                </div>
                <div data-date="July 2015">
                    <a href="" class="month-posts__post">
                        <div class="month-posts__post__img-box">
                            <img src="images/blog/mini-post-1.jpg" alt=""/>
                        </div>
                        <div class="month-posts__post__content">
                            <span class="month-posts__post__title">Trending Colors – The Best-Selling Tones6</span>
                            <span class="month-posts__post__date">September, 2015</span>
                        </div>
                    </a>
                    <a href="" class="month-posts__post">
                        <div class="month-posts__post__img-box">
                            <img src="images/blog/mini-post-2.jpg" alt=""/>
                        </div>
                        <div class="month-posts__post__content">
                            <span class="month-posts__post__title">Now that’s Fresh!</span>
                            <span class="month-posts__post__date">September, 2015</span>
                        </div>
                    </a>
                    <a href="" class="month-posts__post">
                        <div class="month-posts__post__img-box">
                            <img src="images/blog/mini-post-3.jpg" alt=""/>
                        </div>
                        <div class="month-posts__post__content">
                            <span class="month-posts__post__title">Trending Colors – The Best-Selling Tones6</span>
                            <span class="month-posts__post__date">September, 2015</span>
                        </div>
                    </a>
                    <a href="" class="month-posts__post">
                        <div class="month-posts__post__img-box">
                            <img src="images/blog/mini-post-4.jpg" alt=""/>
                        </div>
                        <div class="month-posts__post__content">
                            <span class="month-posts__post__title">Now that’s Fresh!</span>
                            <span class="month-posts__post__date">September, 2015</span>
                        </div>
                    </a>
                    <a href="" class="month-posts__post">
                        <div class="month-posts__post__img-box">
                            <img src="images/blog/mini-post-5.jpg" alt=""/>
                        </div>
                        <div class="month-posts__post__content">
                            <span class="month-posts__post__title">Now that’s Fresh!</span>
                            <span class="month-posts__post__date">September, 2015</span>
                        </div>
                    </a>

                    <div class="month-posts__link-wrap">
                        <a class="month-posts__link" href="">More posts for this month</a>
                    </div>
                </div>
                <div data-date="September 2015">
                    <a href="" class="month-posts__post">
                        <div class="month-posts__post__img-box">
                            <img src="images/blog/mini-post-1.jpg" alt=""/>
                        </div>
                        <div class="month-posts__post__content">
                            <span class="month-posts__post__title">Trending Colors – The Best-Selling Tones6</span>
                            <span class="month-posts__post__date">September, 2015</span>
                        </div>
                    </a>
                    <a href="" class="month-posts__post">
                        <div class="month-posts__post__img-box">
                            <img src="images/blog/mini-post-2.jpg" alt=""/>
                        </div>
                        <div class="month-posts__post__content">
                            <span class="month-posts__post__title">Now that’s Fresh!</span>
                            <span class="month-posts__post__date">September, 2015</span>
                        </div>
                    </a>
                    <a href="" class="month-posts__post">
                        <div class="month-posts__post__img-box">
                            <img src="images/blog/mini-post-3.jpg" alt=""/>
                        </div>
                        <div class="month-posts__post__content">
                            <span class="month-posts__post__title">Trending Colors – The Best-Selling Tones6</span>
                            <span class="month-posts__post__date">September, 2015</span>
                        </div>
                    </a>
                    <a href="" class="month-posts__post">
                        <div class="month-posts__post__img-box">
                            <img src="images/blog/mini-post-4.jpg" alt=""/>
                        </div>
                        <div class="month-posts__post__content">
                            <span class="month-posts__post__title">Now that’s Fresh!</span>
                            <span class="month-posts__post__date">September, 2015</span>
                        </div>
                    </a>
                    <a href="" class="month-posts__post">
                        <div class="month-posts__post__img-box">
                            <img src="images/blog/mini-post-5.jpg" alt=""/>
                        </div>
                        <div class="month-posts__post__content">
                            <span class="month-posts__post__title">Now that’s Fresh!</span>
                            <span class="month-posts__post__date">September, 2015</span>
                        </div>
                    </a>

                    <div class="month-posts__link-wrap">
                        <a class="month-posts__link" href="">More posts for this month</a>
                    </div>
                </div>
                <div data-date="October 2015">
                    <a href="" class="month-posts__post">
                        <div class="month-posts__post__img-box">
                            <img src="images/blog/mini-post-1.jpg" alt=""/>
                        </div>
                        <div class="month-posts__post__content">
                            <span class="month-posts__post__title">Trending Colors – The Best-Selling Tones6</span>
                            <span class="month-posts__post__date">September, 2015</span>
                        </div>
                    </a>
                    <a href="" class="month-posts__post">
                        <div class="month-posts__post__img-box">
                            <img src="images/blog/mini-post-2.jpg" alt=""/>
                        </div>
                        <div class="month-posts__post__content">
                            <span class="month-posts__post__title">Now that’s Fresh!</span>
                            <span class="month-posts__post__date">September, 2015</span>
                        </div>
                    </a>
                    <a href="" class="month-posts__post">
                        <div class="month-posts__post__img-box">
                            <img src="images/blog/mini-post-3.jpg" alt=""/>
                        </div>
                        <div class="month-posts__post__content">
                            <span class="month-posts__post__title">Trending Colors – The Best-Selling Tones6</span>
                            <span class="month-posts__post__date">September, 2015</span>
                        </div>
                    </a>
                    <a href="" class="month-posts__post">
                        <div class="month-posts__post__img-box">
                            <img src="images/blog/mini-post-4.jpg" alt=""/>
                        </div>
                        <div class="month-posts__post__content">
                            <span class="month-posts__post__title">Now that’s Fresh!</span>
                            <span class="month-posts__post__date">September, 2015</span>
                        </div>
                    </a>
                    <a href="" class="month-posts__post">
                        <div class="month-posts__post__img-box">
                            <img src="images/blog/mini-post-5.jpg" alt=""/>
                        </div>
                        <div class="month-posts__post__content">
                            <span class="month-posts__post__title">Now that’s Fresh!</span>
                            <span class="month-posts__post__date">September, 2015</span>
                        </div>
                    </a>

                    <div class="month-posts__link-wrap">
                        <a class="month-posts__link" href="">More posts for this month</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>