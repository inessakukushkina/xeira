<ul class="breadcrumbs">
	<li class="breadcrumbs__item">
		<a href="" class="breadcrumbs__item__link">
			My account
		</a>
	</li>
	<li class="breadcrumbs__item">
		<a href="" class="breadcrumbs__item__link">
			My campaigns
		</a>
	</li>
</ul>
<div class="row">
	<div class="col-sm-3">
		<div class="sidebar">			
			<h2 class="sidebar_title">
				My Account
			</h2>
			<ul class="sidebar__list">
				<li class="sidebar__list__item">
					<a href="" class="sidebar__list__item__link active">
						My Designs
					</a>
				</li>
				<li class="sidebar__list__item">
					<a href="" class="sidebar__list__item__link">
						My Purchases
					</a>
				</li>
				<li class="sidebar__list__item">
					<a href="" class="sidebar__list__item__link">
						My Sold Items
					</a>
				</li>
				<li class="sidebar__list__item">
					<a href="" class="sidebar__list__item__link">
						My Campaigns
					</a>
				</li>
				<li class="sidebar__list__item">
					<a href="" class="sidebar__list__item__link">
						Wishlist
					</a>
				</li>
				<li class="sidebar__list__item">
					<a href="" class="sidebar__list__item__link">
						Reviews
					</a>
				</li>
				<li class="sidebar__list__item">
					<a href="" class="sidebar__list__item__link">
						Invite & earn
					</a>
				</li>
				<li class="sidebar__list__item">
					<a href="" class="sidebar__list__item__link">
						Settings
					</a>
				</li>
			</ul>
		</div>
	</div>
	<div class="col-sm-9">
		<h2 class="head__title">
			My Campaigns
		</h2>
		<div class="row m-t20">
			<div class="col-sm-4">
				<div class="form-group form-search">
					<input type="text" class="form-control" placeholder="Search entire store here...">
				</div>
			</div>
			<div class="col-sm-2 sm-right">
				<p class="text-page p-t5 m-b0">Sort By:</p>				
			</div>
			<div class="col-sm-4">
				<div class="form-group">
					<select class="form-control">
						<option value="">All categories</option>
						<option value="">Pig</option>
						<option value="">Raccoon</option>
					</select>
				</div>
			</div>
		</div>
		<table class="order__table responsive__table m-t5 m-b40">
			<thead>
				<tr>
					<th>Сampagin name</th>
					<th>Estimated profit</th>
					<th>Items</th>
					<th>Sold</th>
					<th>Date end</th>
					<th>Status</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td data-th="Сampagin name:">
						<a href="" class="order__link">
							Some campagin name
						</a>
					</td>
					<td data-th="Estimated profit:">
						$ 833.00
					</td>
					<td data-th="Items:">
						234
					</td>
					<td data-th="Sold:">
						234
					</td>
					<td data-th="Date end:">
						06/14/2015
					</td>
					<td data-th="Status:">
						<span class="order__pending">
							<i class="m-r5 icon-pending"></i>
							Pending
						</span>
					</td>
				</tr>
				<tr>
					<td data-th="Сampagin name:">
						<a href="" class="order__link">
							Some campagin name
						</a>
					</td>
					<td data-th="Estimated profit:">
						$ 833.00
					</td>
					<td data-th="Items:">
						234
					</td>
					<td data-th="Sold:">
						234
					</td>
					<td data-th="Date end:">
						06/14/2015
					</td>
					<td data-th="Status:">
						<span class="order__completed">
							<i class="m-r5 icon-complete"></i>
							Complete
						</span>
					</td>
				</tr>
				<tr>
					<td data-th="Сampagin name:">
						<a href="" class="order__link">
							Some campagin name
						</a>
					</td>
					<td data-th="Estimated profit:">
						$ 833.00
					</td>
					<td data-th="Items:">
						234
					</td>
					<td data-th="Sold:">
						234
					</td>
					<td data-th="Date end:">
						06/14/2015
					</td>
					<td data-th="Status:">
						<span class="order__completed">
							<i class="m-r5 icon-complete"></i>
							Complete
						</span>
					</td>
				</tr>
			</tbody>
		</table>
	</div>
</div>