<ul class="breadcrumbs">
	<li class="breadcrumbs__item">
		<a href="" class="breadcrumbs__item__link">
			My account
		</a>
	</li>
	<li class="breadcrumbs__item">
		<a href="" class="breadcrumbs__item__link">
			Invite & Earn
		</a>
	</li>
</ul>
<div class="row">
	<div class="col-sm-3">
		<div class="sidebar">			
			<h2 class="sidebar_title">
				My Account
			</h2>
			<ul class="sidebar__list">
				<li class="sidebar__list__item">
					<a href="" class="sidebar__list__item__link active">
						My Designs
					</a>
				</li>
				<li class="sidebar__list__item">
					<a href="" class="sidebar__list__item__link">
						My Purchases
					</a>
				</li>
				<li class="sidebar__list__item">
					<a href="" class="sidebar__list__item__link">
						My Sold Items
					</a>
				</li>
				<li class="sidebar__list__item">
					<a href="" class="sidebar__list__item__link">
						My Campaigns
					</a>
				</li>
				<li class="sidebar__list__item">
					<a href="" class="sidebar__list__item__link">
						Wishlist
					</a>
				</li>
				<li class="sidebar__list__item">
					<a href="" class="sidebar__list__item__link">
						Reviews
					</a>
				</li>
				<li class="sidebar__list__item">
					<a href="" class="sidebar__list__item__link">
						Invite & earn
					</a>
				</li>
				<li class="sidebar__list__item">
					<a href="" class="sidebar__list__item__link">
						Settings
					</a>
				</li>
			</ul>
		</div>
	</div>
	<div class="col-sm-9">
		<h2 class="head__title">
			Upload Designs
		</h2>
		<ul class="download__list clearfix">
			<li class="download__list__item active" data-list>
				<i class="fa fa-check-circle"></i> Upload files
			</li>
			<li class="download__list__item" data-list>
				<i class="fa fa-check-circle"></i> Confirm copyrights
			</li>
			<li class="download__list__item" data-list>
				<i class="fa fa-check-circle"></i> Describe design
			</li>
			<li class="download__list__item" data-list>
				<i class="fa fa-check-circle"></i> Publish design
			</li>
		</ul>
		<form action="/" id="dropFiles" class="dropzone dz-clickable">
			<div class="dropzone__content dz-message">
				<h3 class="dropzone__content__title">
					Drag files here
				</h3>
				<div class="dropzone__content__or">
					<span class="dropzone__content__or__text">or</span>
				</div>
				<span class="btn__accept">
					Browse files
				</span>
				<h3 class="dropzone__content__list__title">
					Supported formats
				</h3>
				<ul class="dropzone__content__list">
					<li class="dropzone__content__list__item">
						JPG
					</li>
					<li class="dropzone__content__list__item">
						AI
					</li>
					<li class="dropzone__content__list__item">
						GIF
					</li>
					<li class="dropzone__content__list__item">
						SDR
					</li>
					<li class="dropzone__content__list__item">
						EPS
					</li>
					<li class="dropzone__content__list__item">
						PNG
					</li>
					<li class="dropzone__content__list__item">
						SVG
					</li>
				</ul>
				<p class="dropzone__content__text">
					Image size 40 to 4000 px, file size 10 MB max.
				</p>
				<p class="dropzone__content__text">
					Vector file 3 colors max.
				</p>
			</div>
		</form>
		<div class="row confirm__upload block__upload is-hidden" data-upload>
			<div class="col-md-6">
				<div class="upload__gallery">
					<img src="" alt="" class="upload__gallery__picture">
				</div>
			</div>
			<div class="col-md-6">
				<div class="upload__content">
					<h3 class="upload__title">
						Before you continue
					</h3>
					<p class="upload__text">
						Please confirm that this is your design and that it does not infringe any rights (particularly copyrights, personal rights or trademarks). 
						We audit every design for legal compliance and will reject any infringing designs.
					</p>
					<label id="" class="cb-checkbox w-100 m-t20">
						<input type="checkbox">
						I confirm that I am allowed to use this design
					</label>
					<div class="clearfix m-t40">
						<button class="btn__primary m-r20" data-back>
							Back
						</button>
						<button class="btn__default m-t10" data-forward>
							Continue
						</button>
					</div>
				</div>
			</div>
		</div>
		<div class="row block__upload is-hidden" data-upload>
			<div class="col-md-6">
				<div class="upload__gallery">
					<img src="" alt="" class="upload__gallery__picture">
				</div>
			</div>
			<div class="col-md-6">
				<div class="upload__content">
					<h3 class="upload__title">
						Before you continue
					</h3>
					<div class="row">
						<div class="col-xs-12">
							<p class="form-label">
								First name
							</p>
							<div class="form-group">
								<input type="text" class="form-control">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12">
							<p class="form-label">
								Choose at least 3 tags for your design
							</p>
							<div class="form-group">
								<input type="text" class="form-control tags">
							</div>
							<div class="form-group">
								<select class="chosen-select">
									<option value="">Animal & Nature</option>
									<option value="">Building</option>
									<option value="">Zoo</option>
								</select>
							</div>
						</div>
					</div>
					<label class="cb-checkbox w-100 m-t20">
						<input type="checkbox">
						I confirm that I am allowed to use this design
					</label>
					<div class="m-t40">
						<button class="btn__primary m-r20" data-back>
							Back
						</button>
						<button class="btn__default m-t10" data-forward>
							Continue
						</button>
					</div>
				</div>				
			</div>
		</div>
		<div class="row block__upload is-hidden" data-upload>
			<div class="col-md-6">
				<div class="upload__gallery">
					<img src="" alt="" class="upload__gallery__picture">
				</div>
			</div>
			<div class="col-md-6">
				<div class="upload__content">
					<h3 class="upload__title">
						Description
					</h3>
					<div class="row">
						<div class="col-xs-6">
							<p class="form-label">
								Design ID:
							</p>							
						</div>
						<div class="col-xs-6 text-right">
							<p class="text-page">
								1004345391
							</p>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-6">
							<p class="form-label">
								Design name:
							</p>							
						</div>
						<div class="col-xs-6 text-right">
							<p class="text-page">
								Monkey
							</p>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-6">
							<p class="form-label">
								Design type:
							</p>							
						</div>
						<div class="col-xs-6 text-right">
							<p class="text-page">
								Pixel graphic (PNG)
							</p>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-6">
							<p class="form-label">
								Scaling:
							</p>							
						</div>
						<div class="col-xs-6 text-right">
							<p class="text-page">
								Scalable (min: 50%)
							</p>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-6">
							<p class="form-label">
								Size:
							</p>							
						</div>
						<div class="col-xs-6 text-right">
							<p class="text-page">
								1024 px x 768 px
							</p>
							<p class="text-page">
								8.5 inch x 6.4 inch
							</p>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12">
							<p class="form-label">
								Price:
							</p>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-6">
							<div class="row">
								<div class="col-xs-8">
									<div class="form-group">
										<input type="text" class="form-control">
									</div>							
								</div>
								<div class="col-xs-4">
									<p class="text-page p-t5">
										$
									</p>
								</div>
							</div>
						</div>
						<div class="col-xs-6 text-right">
							<p class="pink_text p-t5">
								You will receive $800.00
							</p>
						</div>
					</div>
					
					<div class="row">
						<div class="col-xs-12">
							<div class="m-t40">
								<button class="btn__primary m-r20" data-back>
									Back
								</button>
								<button class="btn__default m-t10">
									Publish Design
								</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>